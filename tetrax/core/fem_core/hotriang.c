#include "hotriang.h"
#include "utils.h"
#include "matutils.h"
#include <stdlib.h>
/*----------------------------------------------------------------------*/
/*
   This is the 3D Green function derivative dG/dnv, where nv is the
   surface normal vector
   The Green function is Green(r,r') = 1/(r-r')
   dG/dnv = (r-r')nv/|r-r'|^3
*/
double dGreendnv(double *r, double *rp, double *nv)
{
    double dr[3], absdr, small, dg3 = 0.0, d4PI = (1.0/(4*PI));

    small = 1.e-15;

    dr[0] = r[0] - rp[0];
    dr[1] = r[1] - rp[1];
    dr[2] = r[2] - rp[2];

    absdr = vectabs(dr);
    if(absdr > small) dg3 = 1.0/(absdr*absdr*absdr);
    return(dot_prod(dr,nv)*dg3*d4PI);
}
/* End double dGreendnv */
/*----------------------------------------------------------------------*/
double GreenTriangInt(double *x0, double *x1, double *nv)
{
  double *dv,green,small,t,g,g_3=0.0;

  dv = (double *) malloc((size_t) 3*sizeof(double));
  small = 1.e-15;
  dv[0] = x0[0] - x1[0];
  dv[1] = x0[1] - x1[1];
  dv[2] = x0[2] - x1[2];
  g = vectabs(dv);
  if(g > small) g_3 = 1.0/pow(g,3);
  t = dot_prod(dv,nv);
  green = t*g_3;
  free(dv);
  return(green);
}
/* End double GreenTriangInt */
/*----------------------------------------------------------------------*/
void Triangulate(int *ijk, int **ijk_n, double *x, double **w,
		 int *nc, int *npn, int *lpn)
/*
c     A triangular element 1-2-3 is subdivided into four elements:
c      
c
c                1
c              /   \
c             /  I  \
c            /       \
c           6---------5
c          / \  IV   / \
c         /II \     /III\
c        /     \   /     \
c       2------- 4 ------ 3
c
c
c first, it should be checked whether the new nodes have already been 
c created.      
c
c--------      split edge 2-3 ? -> new node number 
*/
{
  int *c, flag=0;
  int i,j,n1,n2,nnp,n_c;

  c = (int *) malloc((size_t) 6*sizeof(int));
  nnp = *npn;
  n_c = *nc;

  c[0] = ijk[0];
  c[1] = ijk[1];
  c[2] = ijk[2];
  flag = 0;
  for(i=0;i<nnp;i++) {
    /*
      Loop over all pending nodes.
      This loop is to check whether the node which is about to be inserted 
      has already been created in a neighboring element.
    */
    n1 = lpn[3*i];
    n2 = lpn[3*i+1];
    if((n1 == ijk[1] && n2 == ijk[2]) || (n1 == ijk[2] && n2 == ijk[1])) {
      c[3] = lpn[3*i+2];
      nnp--;
      for(j=0;j<3;j++) lpn[3*i+j] = lpn[3*nnp+j];
      flag = 1;
      break;
    }
  }
  if(flag == 0) {
    n_c++;
    c[3] = n_c;
    lpn[3*nnp] = ijk[1];
    lpn[3*nnp+1] = ijk[2];
    lpn[3*nnp+2] = n_c;
    nnp++;
  }

  flag = 0;
  for(i=0;i<nnp;i++) {
    n1 = lpn[3*i];
    n2 = lpn[3*i+1];
    if((n1 == ijk[0] && n2 == ijk[2]) || (n1 == ijk[2] && n2 == ijk[0])) {
      c[4] = lpn[3*i+2];
      nnp--;
      for(j=0;j<3;j++) lpn[3*i+j] = lpn[3*nnp+j];
      flag = 1;
      break;
    }
  }

  if(flag == 0) {
    n_c++;
    c[4] = n_c;
    lpn[3*nnp] = ijk[0];
    lpn[3*nnp+1] = ijk[2];
    lpn[3*nnp+2] = n_c;
    nnp++;
  }

  flag = 0;
  for(i=0;i<nnp;i++) {
    n1 = lpn[3*i];
    n2 = lpn[3*i+1];
    if((n1 == ijk[0] && n2 == ijk[1]) || (n1 == ijk[1] && n2 == ijk[0])) {
      c[5] = lpn[3*i+2];
      nnp--;
      for(j=0;j<3;j++) lpn[3*i+j] = lpn[3*nnp+j];
      flag = 1;
      break;
    }
  }
  if(flag == 0) {
    n_c++;
    c[5] = n_c;
    lpn[3*nnp] = ijk[0];
    lpn[3*nnp+1] = ijk[1];
    lpn[3*nnp+2] = n_c;
    nnp++;
  }

  *nc = n_c;
  *npn = nnp;

  for(i=0;i<3;i++) {
    x[3*c[3]+i] = 0.5*(x[3*c[1]+i]+x[3*c[2]+i]);
    w[i][c[3]] = 0.5*(w[i][c[1]]+w[i][c[2]]);
    x[3*c[4]+i] = 0.5*(x[3*c[0]+i]+x[3*c[2]+i]);
    w[i][c[4]] = 0.5*(w[i][c[0]]+w[i][c[2]]);
    x[3*c[5]+i] = 0.5*(x[3*c[0]+i]+x[3*c[1]+i]);
    w[i][c[5]] = 0.5*(w[i][c[0]]+w[i][c[1]]);
  }

  /* Element I */ 
  ijk_n[0][0] = c[0];
  ijk_n[1][0] = c[5];
  ijk_n[2][0] = c[4];
  /* Element 2 */ 
  ijk_n[0][1] = c[1];
  ijk_n[1][1] = c[3];
  ijk_n[2][1] = c[5];
  /* Element 3 */ 
  ijk_n[0][2] = c[2];
  ijk_n[1][2] = c[4];
  ijk_n[2][2] = c[3];
  /* Element 4 */ 
  ijk_n[0][3] = c[3];
  ijk_n[1][3] = c[4];
  ijk_n[2][3] = c[5];

  free(c);
}
/*----------------------------------------------------------------------*/
void HOBoundaryElm3D(double *x0, double *x1, double *x2, double *x3,
		   double *nv, double A, double *wg, int o_p)
{
  /*
    input:
    int o_p  -  is the requested order of triangulation
    triangulation order, o_p = 0 -> 1 triangle, o_p = 1 -> 4 triangles ...
    xyz[el1] -  coordinates of the observation point
    xyz[el2,3,4] -  coordinates of the original corners of triangle
    A - area of initial element
    nv - normal vector of the original triangle
    
    output:
    double *wg - global weights to perform a high order integration 
  */
  int nel_f, nx_f;
  double *x, **w, An;
  int **ijk, **ijk_n, *ijk_tt, *lpn, npn;
  int i,j,k, it, oc, nc, nel_c, nel_n;

  /* int nel_f - expected number of elements */
  nel_f = pow(2,2*o_p);
  /* int nx_f - expected number of nodes */
  nx_f = (int) (pow(2,o_p)+1)*(pow(2,o_p)+2)/2;
  nx_f += 1;

  x = (double *) malloc((size_t) 3*nx_f*sizeof(double));
    ijk_tt = (int *) malloc((size_t) 3*sizeof(int));
  ijk = IMatrix(3,nel_f);
  ijk_n = IMatrix(3,4);
  w = DMatrix(3,nx_f);

  /* define initial triangle */
  for(i=0;i<3;i++) {
      x[i] = x1[i];
      x[3+i] = x2[i];
      x[6+i] = x3[i];
  }
  /* initialize weights */
  for(i=0;i<3;i++) {
    for(j=0;j<3;j++) w[i][j] = 0.0;
    w[i][i] = 1.0; 
  }
  
  oc = 0;
  nc = 2;
  nel_c = 0;
  nel_n = 0;

  lpn = (int *) malloc((size_t) 3*nel_f*sizeof(int));
  npn = 0;
  IniIVector(lpn,3*nel_f,0);

  for(i=0;i<3;i++) ijk[i][0] = i;
  do {
    for(i=0;i<=nel_n;i++) {
      for(it=0;it<3;it++) ijk_tt[it] = ijk[it][i];
      Triangulate(ijk_tt,ijk_n,x,w,&nc,&npn,lpn);
      for(j=0;j<3;j++) {
	    ijk[j][i] = ijk_n[j][0];
	    ijk[j][nel_c+1] = ijk_n[j][1];
        ijk[j][nel_c+2] = ijk_n[j][2];
    	ijk[j][nel_c+3] = ijk_n[j][3];
      }
      nel_c += 3;
    }
    nel_n = nel_c;
    oc++;
  } while(oc < o_p);

  IniDVector(wg,3,0.0);
  An = A/(3.0*(double)(nel_n+1));

  for(i=0;i<=nel_n;i++) 
    for(j=0;j<3;j++) 
      for(k=0;k<3;k++) 
	    wg[j] += w[j][ijk[k][i]]*dGreendnv(x0,x+3*ijk[k][i],nv);

  for(j=0;j<3;j++) wg[j] *= An;

  free(x);
  FreeIMatrix(ijk_n);
  FreeIMatrix(ijk);
  free(ijk_tt);
  FreeDMatrix(w);
  free(lpn);
}
/*----------------------------------------------------------------------*/
