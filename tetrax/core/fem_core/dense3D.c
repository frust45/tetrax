/* 
   Functions to calculate the dense matrix for 3D FEM/BEM at k=0
   Written by Attila Kákay in Dresden (Duerestrasse 71), 08.12.2020
*/
#include "akmag.h"
#include "dense3D.h"
#include "utils.h"
#include "matutils.h"
#include "bnd2d.h"
#include "hotriang.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define DSIGN(X,Y) ((Y) < 0 ? -fabs(X) : fabs(X))

/*----------------------------------------------------------------------*/
int Lindholm3D(double *x0, double *x1, double *x2, double *x3, double *nv,
                double A, double *wg, int *failed)
{
/*
   see D. A. Lindholm, IEEE Trans. MAG. 20, No.5, September 1984, 2025-2032
*/
    double tol;
    int i,j;
    int changed;
    int vlength = 3*4;
    double x[12],xi[12], rho[12];
    double rho0[4], s[4], eta[12],eta0[4], chi0;
    double gamma[3][3], P[3];
    double Omega, Omega_tmp;

    *failed = 0;
    tol = 1.e-7;

    for(i=0;i<3;i++) {
        x[i] = x1[i];
        x[3+i] = x2[i];
        x[6+i] = x3[i];
        x[9+i] = x1[i];
    }
    /* define rho[3][4] as rho[3*4] */
    for(i=0;i<4;i++) vectdiff(x+3*i,x0,rho+3*i);
    /* define xi[3][4] as xi[3*4] */
    for(i=0;i<3;i++) vectdiff(rho+3*(i+1),rho+3*i,xi+3*i);
    /* expand to cyclic permutation, NB: xi is not normalized yet!*/
    for(i=0;i<3;i++) xi[i+9] = xi[i];
    /* define length of edges */
    for(i=0;i<4;i++) s[i] = vectabs(xi+3*i);

    /* normalize xi[3][4] as xi[3*4] */
    for(i=0;i<4;i++)
        for(j=0;j<3;j++)
            xi[j+3*i] /= s[i];

    /* define eta[3][4] as eta[3*4] */
    for(i=0;i<4;i++) cross_prod(nv,xi+3*i,eta+3*i);
    chi0 = dot_prod(nv,rho+6);

    if(fabs(chi0) < tol) {
        for(j=0;j<3;j++) wg[j] = 0.0;
        return(1);
    }

    /* define gamma[3][3] as gamma[3*3] */
    for(i=0;i<3;i++)
        for(j=0;j<3;j++) gamma[i][j] = dot_prod(xi+3*(i+1),xi+3*j);

    for(i=0;i<4;i++) {
        eta0[i] = dot_prod(eta+3*i,rho+3*i);
        rho0[i] = vectabs(rho+3*i);
    }

    for(i=0;i<3;i++)
        P[i] = log((rho0[i]+rho0[i+1]+s[i])/(rho0[i]+rho0[i+1]-s[i]));

    Omega_tmp = SolidAngle(x0,x1,x2,x3);
    Omega = fabs(Omega_tmp)*DSIGN(-1.0,chi0);
    /*Omega = SolidAngleDelta(rho0,rho);
    Omega = fabs(Omega)*DSIGN(-1.0,chi0);
    */

    changed = 0;
    do {
        for(i=0;i<3;i++) {
            wg[i] = eta0[i+1]*Omega-
                    chi0*(gamma[i][0]*P[0] + gamma[i][1]*P[1] + gamma[i][2]*P[2]);
            wg[i] *= s[i+1]/(2.0*A);
        }
        if((wg[0]*wg[1] < 0.0) || (wg[0]*wg[2] < 0.0) || (wg[1]*wg[2] < 0.0)) {
            if(changed) {
                /*	if(vlevel > 8) printf("Lindholm: I resign.\n");*/
                for(i=0;i<3;i++) wg[i] = 0.0;
                *failed = 1;
                return(2);
            } else {
                Omega *= -1.0;
                changed = 1;
            }
        } else changed=0;
    } while(changed);
    return(99);
}
/* END Lindholm3D */
/*----------------------------------------------------------------------*/
//void ComputeDenseMatrix3D(AKElm *elm, AKElm belm, OperatorMat *OpMat)
void ComputeDenseMatrix3D(int nx, int *bijk, int bnx, int bnelx,
                          int *bnlist, double *xyz, double *nv,
                          double *sang, int op, double *D3D)
{
    int i, bel;
    int cbn, bn1, bn2, bn3, lst1, lst2, lst3;
    int *n2bn, failed, lind=0, opl=0;
    double A, nv_norm[3], wg[3];
/*
    nx = elm->nx;
    bnelx = elm->bnelx;
    bnx = elm->bnx;
    bnxb = belm.nx;
    dim = elm->dim;
    if(dim == 3) op=2;
*/
    if (op > 3) opl = 3;
    else opl = op;
/*
    if(bnx != bnxb) {
        printf("Something is seriously wrong with the boundary elements.\n");
        exit(0);
    }
*/
    n2bn = (int *) malloc(nx*sizeof(int));
    if(!n2bn) memerror("densematrix.c (n2bn[nx])");
    IniIVector(n2bn,nx,0);
    IniDVector(D3D, bnx*bnx, 0.0);
/* The solid angles will contribute to the diagonals */
#pragma omp parallel for
    for(i=0; i<bnx; i++) {
         D3D[i*(bnx+1)] = sang[i];
         n2bn[bnlist[i]] = i;
    }

    /* The magic should happen here */
//#pragma parallel for private (cbn,bel)
    for(i=0; i<bnx; i++) {
        cbn = bnlist[i];
        for (bel = 0; bel<bnelx; bel++) {
            bn1 = bijk[3*bel];
            bn2 = bijk[3*bel+1];
            bn3 = bijk[3*bel+2];
            lst1 = n2bn[bn1];
            lst2 = n2bn[bn2];
            lst3 = n2bn[bn3];
            A = vectabs(nv + 3*bel);
            nv_norm[0] = nv[3 * bel    ] / A;
            nv_norm[1] = nv[3 * bel + 1] / A;
            nv_norm[2] = nv[3 * bel + 2] / A;
            wg[0] = 0.0;
            wg[1] = 0.0;
            wg[2] = 0.0;
//            int Lindholm3D(double *x0, double *x1, double *x2, double *x3,
//            double *nv, double A, double *wg, int *failed)
            lind = Lindholm3D(xyz+3*cbn, xyz+3*bn1, xyz+3*bn2, xyz+3*bn3,
                              nv_norm,A,wg,&failed);
//            IntGreenNormal2D(elm->xyz+3*cbn, elm->xyz+3*bn1, elm->xyz+3*bn2,nv_norm,A,wg);
            if(failed) {
                printf("Lindholm failed!\n");
                fflush(stdout);
                HOBoundaryElm3D(xyz+3*cbn, xyz+3*bn1, xyz+3*bn2, xyz+3*bn3,
                                nv_norm,A,wg,opl);
                failed = 0;
            }
            D3D[i * bnx + lst1] += wg[0];
            D3D[i * bnx + lst2] += wg[1];
            D3D[i * bnx + lst3] += wg[2];
//            printf("%lf\t%lf\t%lf\n",wg[0],wg[1],wg[2]);
        }
    }
    free(n2bn);
    printf("Dense matrix computation done!");
    fflush(stdout);
}
/* END void ComputeDenseMatrix */
/*----------------------------------------------------------------------*/
