//
// Created by Attila Kakay on 25.01.21.
//

#ifndef TETRAX_MATUTILS_H
#define TETRAX_MATUTILS_H

#include "akmag.h"

void IniIVector(int *vector, int size, int value);
/*----------------------------------------------------------------------*/
void IniDVector(double *vector, int size, double value);
/*----------------------------------------------------------------------*/
void sprstp (double sa[], int ija[], double sat[], int ijat[]);
/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
void RISSprSAXS(RISSprMat spm, AKVec vec, AKVec *out, int sign);
/*----------------------------------------------------------------------*/
void dsprsax(double *sa, int *ija, double *x, double *b,int n);
/*----------------------------------------------------------------------*/
void dsprsaxadd(double *sa, int *ija, double *x, double *b,int n);
/*----------------------------------------------------------------------*/
void dsprstx(double *sa, int *ija, double *x, double *b,int n);
/*----------------------------------------------------------------------*/


#endif //TETRAX_MATUTILS_H
