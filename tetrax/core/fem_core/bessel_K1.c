//
// Created by Attila Kákay on 14.04.21.
//

#include <math.h>
#include "bessel_K1.h"

/*----------------------------------------------------------------------*/
double BESSI1(double x)
{
    //  Bessel Function of 1st kind of first order - BESSEL I1
    double y,p1,p2,p3,p4,p5,p6,p7,q1,q2,q3,q4,q5,q6,q7,q8,q9,ax,bx;
    p1=0.5; p2=0.87890594; p3=0.51498869; p4=0.15084934;
    p5=0.2658733e-1; p6=0.301532e-2; p7=0.32411e-3;
    q1=0.39894228; q2=-0.3988024e-1; q3=-0.362018e-2;
    q4=0.163801e-2; q5=-0.1031555e-1; q6=0.2282967e-1;
    q7=-0.2895312e-1; q8=0.1787654e-1; q9=-0.420059e-2;
    if (fabs(x) < 3.75) {
        y=(x/3.75)*(x/3.75);
        return(x*(p1+y*(p2+y*(p3+y*(p4+y*(p5+y*(p6+y*p7)))))));
    }
    else {
        ax=fabs(x);
        y=3.75/ax;
        bx=exp(ax)/sqrt(ax);
        ax=q1+y*(q2+y*(q3+y*(q4+y*(q5+y*(q6+y*(q7+y*(q8+y*q9)))))));
        return (ax*bx);
    }
}
/* END double BESSI1(double x) */
/*----------------------------------------------------------------------*/
double BESSK1(double x)
{
    //  Bessel Function of 2nd kind of first order - BESSEL K1
    double y, out;

    if (x == 0) return (1e32);
    if (x <= 2.0) {
        y = x * x / 4.0;
        out = (log(x / 2.0) * BESSI1(x));
        out += (1.0 / x) * (1.0 + y * (0.15443144 + y * (-0.67278579 +
                y * (-0.18156897 + y * (-0.1919402e-1 + y * (-0.110404e-2 +
                y * (-0.4686e-4)))))));
        return (out);
    } else {
        y = 2.0 / x;
        out = (exp(-x) / sqrt(x)) * (1.25331414 + y * (0.23498619 +
                y * (-0.3655620e-1 + y * (0.1504268e-1 + y * (-0.780353e-2 +
                y * (0.325614e-2 +   y * (-0.68245e-3)))))));
    }
    return out;
}
/* END double BESSK1(double x) */
/*----------------------------------------------------------------------*/