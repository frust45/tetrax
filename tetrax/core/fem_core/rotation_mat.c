//
// Created by Attila Kákay on 29.04.21.
//
#include <math.h>
#include <stdio.h>
#include "rotation_mat.h"

/*----------------------------------------------------------------------*/
/**
 * Constructs a SparseMat from the given parameters. This SparseMat rotates a
 * vector of magnetizations from the global xyz-coordinate system to the local
 * 123-coordinate system. This does not represent a 3x3-matrix, but a
 * 3Nx3N-matrix, where N is the number of magnetization vectors. It is assumed
 * that all the x-, y- and z-coordinates are each grouped together.
 *
 * @param m0 The magnetization vectors used for the rotation matrix. m0[0] are
 * all the x-components, m0[1] all the y-components, m0[2] all the z-components.
 * @param m0_len The number of magnetization vectors (NOT the number of doubles
 * in m0)
 */
CSRSprMat rotation_matrix(double *m0[3], int nx) {
    RISSprMat result;
    CSRSprMat result_csr;
    // In a worst case scenario, the SparseMat might hold 9 doubles for every
    // vector entry, + 1 for bookkeeping
    N_RISSprMat(&result, 9*nx + 1);
    // ija[0] = length of the diagonal of the resulting matrix + 1
    result.ija[0] = 3*nx + 1;
    // k is the current index in ija
    int k = 3*nx;
    // i selects the magnetization vector, cm selects the coordinate (x/y/z)
    // it is important that the iteration over i happens "inside" the
    // iteration over cm, because that is how the data is stored in memory
    for (int cm = 0; cm < 3; ++cm) {
        for (int i = 0; i < nx; ++i) {
            // building the diagonal here...
            double mag_vec[3] = { m0[0][i], m0[1][i], m0[2][i] };
            result.sa[i + cm*nx] =
                    rotation_matrix_component(mag_vec, cm, cm);
            // ...and off-diagonal elements here
            // while cm goes over rows (1/2/3), cn goes over columns (x/y/z)
            for (int cn = 0; cn < 3; ++cn) {
                double entry = rotation_matrix_component(mag_vec, cm, cn);
                if (entry != 0 && cm != cn) {
                    ++k;
                    result.sa[k] = entry;
                    // ija[k] is the column
                    result.ija[k] = i + cn*nx;
                }
            }
            // save the index of the next off-diagonal element
            result.ija[i + cm*nx + 1] = k + 1;
        }
    }
    ris2csrDirect(&result,&result_csr);
    F_RISSprMat(&result);
    return result_csr;
}
/*----------------------------------------------------------------------*/
/**
 * Returns one component of the rotation matrix R(m), which rotates from the
 * global xyz-coordinate system to the local 123-coordinate system. This
 * function does the same as rotation_matrix_component_nonorm, it just norms the
 * e1/e2/e3 vectors afterwards.
 *
 * The local coordinate system is given by
 * e1 = -m cross (ez cross m)
 * e2 = ez cross m
 * e3 = m
 *
 * Note to self: The rotation matrix generation might be sped up a lot by
 * storing intermediate results. Might.
 *
 * @param idx_m The m-index ("up/down" / "1/2/3") of the matrix component
 *     returned. Goes from 0 to 2.
 * @param idx_n The n-index ("left/right" / "x/y/z") of the matrix component
 *     returned. Goes from 0 to 2.
 */
double rotation_matrix_component(double m[3], int idx_m, int idx_n) {
    double abs_sq = 0;
    for (int n = 0; n < 3; ++n) {
        double rmc = rotation_matrix_component_nonorm(m, idx_m, n);
        abs_sq += rmc*rmc;
    }
    return rotation_matrix_component_nonorm(m, idx_m, idx_n) / sqrt(abs_sq);
}
/*----------------------------------------------------------------------*/
/**
 * Returns one not-normed component of the rotation matrix R(m), which rotates
 * from the global xyz-coordinate system to the local 123-coordinate system. Not
 * normed means that the vectors e1, e2 and e3 do not have unit length here.
 *
 * The local coordinate system is given by
 * e1 = -m cross (ez cross m)
 * e2 = ez cross m
 * e3 = m
 *
 * @param idx_m The m-index ("up/down" / "1/2/3") of the matrix component
 *     returned. Goes from 0 to 2.
 * @param idx_n The n-index ("left/right" / "x/y/z") of the matrix component
 *     returned. Goes from 0 to 2.
 */
double rotation_matrix_component_nonorm(double m[3], int idx_m, int idx_n) {
    assert(0 <= idx_m && idx_m < 3);
    assert(0 <= idx_n && idx_n < 3);
    // rotation_matrix(m, idx_m, idx_n) =
    //     (idx_m-th basis vector of local base)
    //       [scalar product]
    //     (idx_n-th basis vector of global base)
    //
    // because the global base is simply [{1 0 0} {0 1 0} {0 0 1}], computing a
    // scalar product is not needed
    // individual components of the local base vectors are computed instead
    double ez[3] = {0, 0, 1};

    double ez_cross_m[3];
    cross_product(ez, m, ez_cross_m);
    double abs_sq = 0;
    for (int i = 0; i < 3; ++i) {
        abs_sq += ez_cross_m[i] * ez_cross_m[i];
    }
    // if abs_sq == 0, then m is parallel to ez
    // set e1 = ex, e2 = ey, e3 = ez
    // so the rotation matrix becomes the identity operator
    if (abs_sq == 0) {
//        printf("Warning: magnetization in ez-direction\n");
        return idx_m == idx_n ? m[2] : 0;
    }

    switch (idx_m) {
        case 0: {
            // e1 = -m cross (ez cross m)
            return -cross_product_component(m, ez_cross_m, idx_n);
        };
        case 1:
            return cross_product_component(ez, m, idx_n);
        case 2:
            return m[idx_n];
    }
    assert(false);
}
/*----------------------------------------------------------------------*/
/**
 * Computes the cross product of a and b, and stores the result in result.
 */
void cross_product(double a[3], double b[3], double result[3]) {
    for (int idx_m = 0; idx_m < 3; ++idx_m) {
        result[idx_m] = cross_product_component(a, b, idx_m);
    }
}
/*----------------------------------------------------------------------*/
/**
 * Returns the idx_m-th component of the cross product (a cross b).
 */
double cross_product_component(double a[3], double b[3], int idx_m) {
    double result = 0;
    for (int idx_n = 0; idx_n < 3; ++idx_n) {
        result += cross_product_operator(a, idx_m, idx_n) * b[idx_n];
    }
    return result;
}
/*----------------------------------------------------------------------*/
/**
 * Returns one component of the operator "mu_x(a)" which is defined by
 *
 *     mu_x(a) applied to b = cross product of a and b
 *
 * So, to get the idx_m-th component of the vector c = (a cross b), one would
 * need to sum over cross_product_operator(a, idx_m, idx_n) for idx_n from 0 to
 * 2 (inclusive).
 *
 * @param idx_m The m-index ("up/down") of the operator component returned.
 *     Goes from 0 to 2.
 * @param idx_n The n-index ("left/right") of the operator component returned.
 *     Goes from 0 to 2.
 */
double cross_product_operator(double a[3], int idx_m, int idx_n) {
    assert(0 <= idx_m && idx_m < 3);
    assert(0 <= idx_n && idx_n < 3);
    // epsilon(i,j,k) = levi-civita-symbol
    // the cross product operator component =
    // for idx_a in 0..3:
    //     sum epsilon(idx_m, idx_a, idx_n)*a[idx_a]
    //
    // it is known that epsilon(idx_m, idx_a, idx_n) assumes a nonzero value at
    // most once
    if (idx_m == idx_n) return 0;
    // notation for possible configurations: (idx_m, idx_a, idx_n)
    switch (idx_m) {
        case 0:
            // (0, idx_a, 1) => idx_a = 2, epsilon = -1
            if (idx_n == 1) return -a[2];
                // (0, idx_a, 2) => idx_a = 1, epsilon = 1
            else return a[1];
            break;
        case 1:
            // (1, idx_a, 0) => idx_a = 2, epsilon = 1
            if (idx_n == 0) return a[2];
                // (1, idx_a, 2) => idx_a = 0, epsilon = -1
            else return -a[0];
            break;
        case 2:
            // (2, idx_a, 0) => idx_a = 1, epsilon = -1
            if (idx_n == 0) return -a[1];
                // (2, idx_a, 1) => idx_a = 0, epsilon = 1
            else return a[0];
    };
    assert(false); // we should never end up here
}
/*----------------------------------------------------------------------*/
