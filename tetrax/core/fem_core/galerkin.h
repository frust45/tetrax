//
// Created by Attila Kákay on 15.07.20.
//

#ifndef TETRAX_GALERKIN_H
#define TETRAX_GALERKIN_H

#include "akmag.h"

void ShapeFGrad(AKElm *elm);
void Shape2DFGrad(AKElm *elm);
void Shape3DFGrad(AKElm *elm);
void CalcNodesWignerSeitzVolume(AKElm *elm);
void GalerkinInteractions(AKElm *elm, OperatorMat *OpMat);
void ExchangeLaplacian(AKElm *elm, OperatorMat *OpMat);

#endif //TETRAX_GALERKIN_H
