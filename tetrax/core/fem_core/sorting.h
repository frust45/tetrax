#ifndef SORT_H
#define SORT_H

#define SWAP(a,b) itemp=(a);(a)=(b);(b)=itemp;
#define MM 7
#define NSTACK 50
#define M_D 7

/*----------------------------------------------------------------------*/
void nrerror(char error_text[]);
/*----------------------------------------------------------------------*/
void dindexx(unsigned long n, long long *arr, int *indx);
/*----------------------------------------------------------------------*/
void iindexx(unsigned long n, int *arr, int *indx);
/*----------------------------------------------------------------------*/
void sort(unsigned long n, int arr[]);
/*----------------------------------------------------------------------*/
#endif
