import numpy as np
from scipy.sparse import csr_matrix, lil_matrix, bmat


def get_matrices1D(xyz, ijk, bijk, Aex, Msat, scale):
    """Creating the sparse matrices based on the weak formulation.

    Parameters
    ----------
    xyz : coordinates of the nodes (np.array, shape = (NumONod, 3))
    ijk : finite elements arranged in matrix form (np.array, shape = (NumOElem,3))
            ijk[num,:] = [P1, P2 region]
                P1/P2 index of left/right point associated with element
                (indexing starting with 0)

    NumOElm : total number of finite elements
    NumONode : total number of magnetic nodes

    Returns
    -------
    vol_n : volume of the node (for 1D elements the length)
    
    """
    nx = xyz.shape[0]
    VolElem, VolNod, dNa = CalcdNa(xyz, ijk)
    belm, cnt = np.unique(np.array([ijk[:, 0], ijk[:, 1]]).flatten(), return_counts=True)
    #    cnt = bijk.shape[0]
    boundary_nodes = belm[cnt == 1]
    #boundary_nodes = bijk.flatten()
    xc = CalcLaPl(VolNod, VolElem, ijk, boundary_nodes, dNa, Aex, dim=3)
    div_x = csr_matrix((nx, nx))  # empty matrix
    div_y = CalcDiv_y(VolNod, VolElem, dNa, ijk)  # Minus hinzugefuegt
    div_z = csr_matrix((nx, nx))  # empty matrix
    poiss = -CalcPoiss(VolNod, VolElem, ijk, boundary_nodes, dNa)  # dim =1, DirichletBC = Flase
    lap = -CalcPoiss(VolNod, VolElem, ijk, boundary_nodes, dNa, DirichletBC=True)  # dim = 1
    grad_x = csr_matrix((nx, nx))  # empty matrix
    grad_y = -CalcGrad_y(VolNod, VolElem, dNa, ijk)
    grad_z = csr_matrix((nx, nx))  # empty matrix
    sort_indices = np.argsort(xyz[boundary_nodes, 1])
    # print(sort_indices)
    nv = 1.0 * np.ones_like(boundary_nodes)
    nv[sort_indices[::2]] *= -1.
    #    dA = None
    pang = None
    #belm = ijk[sort_indices,:]
    #print(belm)
#    IntExcMat = CalcInterlayerExchangeMatrix(boundary_nodes, VolNod, sort_indices)
    return boundary_nodes, nv, VolNod, pang, belm, xc, poiss, lap, div_x, div_y, div_z, grad_x, grad_y, grad_z\
#        , \ grad_z, grad_z, grad_z


def ExchangeOperator1D(xyz, ijk, bijk, Aex, Msat, scale):
    VolElem, VolNod, dNa = CalcdNa(xyz, ijk)
    xc = CalcLaPl(VolNod, VolElem, ijk, bijk, dNa, Aex, dim=3)
    return xc

def CalcVol(xyz, ijk):
    """Computes the volumes of the elements and nodes.

    Parameters
    ----------

    xyz : coordinates of the nodes (np.array, shape = (NumOfNod, 3))
    ijk : finite elements arranged in matrix form (np.array, shape = (NumOfElem,3))
            ijk[num,:] = [P1, P2 region]
                P1/P2 index of left/right point associated with element
                (indexing starting with 0)

    Returns
    -------

    VolElem : Volume/length of the line-elements (np.array, shape = (NumOElem))
    VolNod : Volume/length associated with the nods (np.array, shape = (NumONod))

    """
    VolElem = np.zeros(ijk.shape[0])
    P1 = ijk[:, 0].astype(int)
    P2 = ijk[:, 1].astype(int)
    VolElem = np.sqrt(np.sum((xyz[P1, :] - xyz[P2, :]) ** 2, axis=1))

    VolNod = np.zeros(xyz.shape[0])
    for cnt in range(len(VolElem)):
        VolNod[ijk[cnt, 0]] += VolElem[cnt] / 2
        VolNod[ijk[cnt, 1]] += VolElem[cnt] / 2

    return VolElem, VolNod


def CalcdNa(xyz, ijk):
    """
    dNa - derivative of the shape-function defined on the elements
            (np.array, shape = (2,NumOElem))
            dNa[0,num]  = derivative of the shapefunction in the
                        left point of the num element
            dNa[1, num] = derivative of the shapefunction in the
                        right point of the element
    """

    VolElem, VolNod = CalcVol(xyz, ijk)
    dNa = np.array([-1 / VolElem, 1 / VolElem])
    return VolElem, VolNod, dNa


def CalcLaPl(VolNod, VolElem, ijk, BoundaryNodes, dNa, Aex, DirichletBC=False, dim=1):
    """
    Calculates the 2nd derivative in the y-direction as a sparse matrix in
    csr-format according to FEM, thus containing Neumann-boundary conditions.
    If DirichletBC = True, then the matrix is changed, so that it contains a 1
    at Lapl[bn,bn] as only entry in the row bn (for bn in BoundaryNodes),
    meaining the matrix can be used to solve an equation with Dirichlet-
    boundary-conditions.
    """

    if isinstance(Aex, float) or isinstance(Aex, int):
        Aex_ = np.full_like(VolNod,Aex)
    else:
        if Aex.shape[0] != VolNod.shape[0]:
            print("Something wrong with the length of the exchange constant scalar field.")
            exit()
        else:
            Aex_ = Aex

    n = VolNod.size
    LaPL = lil_matrix((dim * n, dim * n), dtype=float)

    for cnt in range(dim):

        for nod in range(n):
            if np.any(BoundaryNodes == nod) and DirichletBC:

                LaPL[nod + cnt * n, nod + cnt * n] = 1

            else:

                LElem = np.where(ijk[:, 1] == nod)[0]
                RElem = np.where(ijk[:, 0] == nod)[0]

                L = 0
                M = 0
                R = 0
                for ind in LElem:
                    #                   L = -dNa[0,ind]*dNa[1,ind]
                    #                   M += -dNa[0,ind]**2
                    inx0 = ijk[ind,0]
                    inx1 = ijk[ind,1]
                    L = -(Aex_[inx0] * VolNod[inx0] + Aex_[inx1] * VolNod[inx1])/(VolNod[inx0] + VolNod[inx1])\
                        * VolElem[ind] * dNa[0, ind] * dNa[1, ind] / VolNod[nod]
                    M += -Aex_[nod] * VolElem[ind] * dNa[1, ind] ** 2 / VolNod[nod]
                for ind in RElem:
                    #                   R = -dNa[0,ind]*dNa[1,ind]
                    #                   M += -dNa[1,ind]**2
                    inx0 = ijk[ind,0]
                    inx1 = ijk[ind,1]
                    R = -(Aex_[inx0] * VolNod[inx0] + Aex_[inx1] * VolNod[inx1])/(VolNod[inx0] + VolNod[inx1])\
                        * VolElem[ind] * dNa[0, ind] * dNa[1, ind] / VolNod[nod]
                    M += -Aex_[nod] * VolElem[ind] * dNa[0, ind] ** 2 / VolNod[nod]

                if L != 0:
                    LaPL[nod + cnt * n, int(ijk[LElem[0], 0] + cnt * n)] = L
                LaPL[nod + cnt * n, nod + cnt * n] = M
                if R != 0:
                    LaPL[nod + cnt * n, int(ijk[RElem[0], 1] + cnt * n)] = R

    return LaPL.tocsr()


def CalcPoiss(VolNod, VolElem, ijk, BoundaryNodes, dNa, DirichletBC=False, dim=1):
    """
    Calculates the 2nd derivative in the y-direction as a sparse matrix in
    csr-format according to FEM, thus containing Neumann-boundary conditions.
    If DirichletBC = True, then the matrix is changed, so that it contains a 1
    at Lapl[bn,bn] as only entry in the row bn (for bn in BoundaryNodes),
    meaining the matrix can be used to solve an equation with Dirichlet-
    boundary-conditions.
    """

    n = VolNod.size
    LaPL = lil_matrix((dim * n, dim * n), dtype=float)

    for cnt in range(dim):

        for nod in range(n):
            if np.any(BoundaryNodes == nod) and DirichletBC:

                LaPL[nod + cnt * n, nod + cnt * n] = 1

            else:

                LElem = np.where(ijk[:, 1] == nod)[0]
                RElem = np.where(ijk[:, 0] == nod)[0]

                L = 0
                M = 0
                R = 0
                for ind in LElem:
                    #                   L = -dNa[0,ind]*dNa[1,ind]
                    #                   M += -dNa[0,ind]**2
                    L = -VolElem[ind] * dNa[0, ind] * dNa[1, ind]
                    M += -VolElem[ind] * dNa[1, ind] ** 2
                for ind in RElem:
                    #                   R = -dNa[0,ind]*dNa[1,ind]
                    #                   M += -dNa[1,ind]**2
                    R = -VolElem[ind] * dNa[0, ind] * dNa[1, ind]
                    M += -VolElem[ind] * dNa[0, ind] ** 2

                if L != 0 and not (np.any(BoundaryNodes == ijk[LElem[0], 0]) and DirichletBC):
                    LaPL[nod + cnt * n, int(ijk[LElem[0], 0] + cnt * n)] = L

                LaPL[nod + cnt * n, nod + cnt * n] = M

                if R != 0 and not (np.any(BoundaryNodes == ijk[RElem[0], 1]) and DirichletBC):
                    LaPL[nod + cnt * n, int(ijk[RElem[0], 1] + cnt * n)] = R

    #                if L != 0:
    #                    LaPL[nod + cnt * n, ijk[LElem[0], 0] + cnt * n] = L
    #                LaPL[nod + cnt * n, nod + cnt * n] = M
    #                if R != 0:
    #                    LaPL[nod + cnt * n, ijk[RElem[0], 1] + cnt * n] = R

    return LaPL.tocsr()


def CalcDiv_y(VolNod, VolElem, dNa, ijk):
    """
    Calculates the transposed matrix of the 1st derivative in y-direction as
    a sparse matrix in csr-format according to FEM and Attila (without the
    Volume associated with the nodes).
    """

    n = VolNod.size
    Div_y = lil_matrix((n, n), dtype=float)

    for nod in range(n):
        LElem = np.where(ijk[:, 1] == nod)[0]
        RElem = np.where(ijk[:, 0] == nod)[0]

        L = 0
        M = 0
        R = 0
        for ind in LElem:
            L = dNa[0, ind] * VolElem[ind] / 2
            M += dNa[1, ind] * VolElem[ind] / 2

        for ind in RElem:
            R = dNa[1, ind] * VolElem[ind] / 2
            M += dNa[0, ind] * VolElem[ind] / 2

        if L != 0:
            Div_y[nod, ijk[LElem[0], 0]] = L
        Div_y[nod, nod] = M
        if R != 0:
            Div_y[nod, ijk[RElem[0], 1]] = R

    return Div_y.tocsr().T


def CalcGrad_y(VolNod, VolElem, dNa, ijk):
    """
    Calculates the 1st derivative in the y-direction as a sparse matrix in
    csr-format according to FEM, includes the volume associated to the nodes.
    """

    n = VolNod.size
    Grad_y = lil_matrix((n, n), dtype=float)

    for nod in range(n):
        LElem = np.where(ijk[:, 1] == nod)[0]
        RElem = np.where(ijk[:, 0] == nod)[0]

        L = 0
        M = 0
        R = 0
        for ind in LElem:
            L = dNa[0, ind] * VolElem[ind] / (2 * VolNod[nod])
            M += dNa[1, ind] * VolElem[ind] / (2 * VolNod[nod])
        for ind in RElem:
            R = dNa[1, ind] * VolElem[ind] / (2 * VolNod[nod])
            M += dNa[0, ind] * VolElem[ind] / (2 * VolNod[nod])

        if L != 0:
            Grad_y[nod, ijk[LElem[0], 0]] = L
        Grad_y[nod, nod] = M
        if R != 0:
            Grad_y[nod, ijk[RElem[0], 1]] = R

    return Grad_y.tocsr()


def ComputeDenseMatrix_1D(dense, belm, BoundaryNodes, xyz, nv, pang, bndint_order, k):
    """
    Calculates the dense Matrix needed for the boundary conditions of psi2 in
    the calculation of the magnetostatic potential.
    """
    y = xyz[:, 1]
    bn = len(BoundaryNodes)
    dense = np.reshape(dense,(bn,bn))

    for i in range(bn):
        for j in range(bn):
            if i != j:
                kT = abs(k) * abs(y[BoundaryNodes[i]] - y[BoundaryNodes[j]])
                dense[i, j] = 1 / 2 * np.exp(-kT) * nv[j] * np.sign(y[BoundaryNodes[i]] - y[BoundaryNodes[j]])
            else:
                dense[i, j] = -1 / 2
    dense = np.reshape(dense, (bn * bn))
    return 0

