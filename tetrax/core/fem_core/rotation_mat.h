//
// Created by Attila Kákay on 29.04.21.
//

#ifndef TETRAX_ROTATION_MAT_H
#define TETRAX_ROTATION_MAT_H

#include <assert.h>
#include <stdbool.h>
#include "akmag.h"

/*----------------------------------------------------------------------*/
void cross_product(double[3], double[3], double[3]);
/*----------------------------------------------------------------------*/
double rotation_matrix_component_nonorm(double[3], int, int);
/*----------------------------------------------------------------------*/
double rotation_matrix_component(double[3], int, int);
/*----------------------------------------------------------------------*/
CSRSprMat rotation_matrix(double *m0[3], int nx);
/*----------------------------------------------------------------------*/
double cross_product_component(double[3], double[3], int);
/*----------------------------------------------------------------------*/
double cross_product_operator(double[3], int, int);
/*----------------------------------------------------------------------*/


#endif //TETRAX_ROTATION_MAT_H
