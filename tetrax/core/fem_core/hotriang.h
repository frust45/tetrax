//
// Created by Attila Kákay on 22.07.21.
//

#ifndef TETRAX_HOTRIANG_H
#define TETRAX_HOTRIANG_H

/*----------------------------------------------------------------------*/
double GreenTriangInt(double *x0, double *x1, double *nv);
/*----------------------------------------------------------------------*/
void Triangulate(int *ijk, int **ijk_n, double *x, double **w,
                 int *nc, int *npn, int *lpn);
/*----------------------------------------------------------------------*/
void HOBoundaryElm3D(double *x0, double *x1, double *x2, double *x3,
                     double *nv, double A, double *wg, int o_p);
/*----------------------------------------------------------------------*/

#endif //TETRAX_HOTRIANG_H
