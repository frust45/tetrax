//
// Created by Attila Kákay on 12/15/21.
//

#ifndef TETRAX_TETRAXPY_H
#define TETRAX_TETRAXPY_H

#include "akmag.h"

int OperatorsFromMesh2D(double *xyz, int *ijk, int nx, int nelx, int dim, double *Aex, double *Msat,
                      double dtet, int *bnlist, int *bnx, double *nv, double *area,
                      CSRSprMat *xc, CSRSprMat *poiss, CSRSprMat *lap,
                      CSRSprMat *div_x, CSRSprMat *div_y,
                      CSRSprMat *grad_x, CSRSprMat *grad_y,
                      double *pang, int *belm);

int OperatorsFromMesh3D(double *xyz, int *ijk, int nx, int nelx, int dim, double *Aex, double *Msat,
                      double dtet, int *bnlist, int *bnx, double *nv, double *area,
                      CSRSprMat *xc, CSRSprMat *poiss, CSRSprMat *lap,
                      CSRSprMat *div_x, CSRSprMat *div_y, CSRSprMat *div_z,
                      CSRSprMat *grad_x, CSRSprMat *grad_y, CSRSprMat *grad_z,
//                      CSRSprMat *bdmi_x, CSRSprMat *bdmi_y, CSRSprMat *bdmi_z,
                      double *pang, int *belm);

int ExchangeOperatorFromMesh2D(double *xyz, int *ijk, int nx, int nelx, int dim, double *Aex, double *Msat,
                      double dtet, CSRSprMat *xc);

int ExchangeOperatorFromMesh3D(double *xyz, int *ijk, int nx, int nelx, int dim, double *Aex, double *Msat,
                      double dtet, CSRSprMat *xc);
#endif //TETRAX_TETRAXPY_H
