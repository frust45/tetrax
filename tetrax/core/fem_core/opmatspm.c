//
// Created by Attila Kákay on 10.12.20.
//

#include "opmatspm.h"
#include "matutils.h"
#include <stdio.h>
#include <stdlib.h>
//#include "malloc.h"
/*----------------------------------------------------------------------*/
int ContainNodes(int n1, int n2, AKElm *elm, int el, int *i1, int *i2)
{
  int i, eld;
  eld = elm->eld;

  *i1 = 0;
  *i2 = 0;
  for(i=0;i<eld;i++) {
    if(elm->ijk[i][el] == n1) *i1=i+1;
    if(elm->ijk[i][el] == n2) *i2=i+1;
  }
  if((*i1)*(*i2) > 0) {
    *i1 -= 1;
    *i2 -= 1;
    return(1);
  }
  return(0);
}
/*----------------------------------------------------------------------*/
int CalcLofSpMatExch(AKElm *elm)
{
  int spl,i,j,l,el,el_no, numadjn, both,mindex;
  int nx, i1, i2, eld, dim, ii;
  double A, Js1, aij, dtet, small;
  spl = 1;
  nx = elm->nx;
  dtet = elm->dtet;
  dim = elm->dim;
  eld = elm->eld;
  small = elm->small;
  for(i=0;i<nx;i++) {
    numadjn = elm->pnx_list[i];
    for(j=0;j<numadjn;j++) {
      l = elm->nx_list[elm->pnx_list[nx+i]+j];
      if(i != l) {
	    aij = 0.0;
	    for(el=0;el<elm->num_el[l];el++) {
	        el_no = elm->nel_list[elm->pnel_list[l]+el];
	        mindex = elm->ijk[eld][el_no]-1;
	        both = ContainNodes(i,l,elm,el_no,&i1,&i2);
	            if(both) {
                    A = (elm->params[mindex].A[i] * elm->vol_n[i] + elm->params[mindex].A[l] * elm->vol_n[l])/
                        ((elm->vol_n[i] + elm->vol_n[l]));

	                if (eld == 4)
		                aij -= A*elm->vol_el[el_no]/elm->vol_n[l]*
		                (elm->dNa[dim*eld*el_no+3*i1]*elm->dNa[dim*eld*el_no+3*i2]+
		                elm->dNa[dim*eld*el_no+3*i1+1]*elm->dNa[dim*eld*el_no+3*i2+1]+
		                elm->dNa[dim*eld*el_no+3*i1+2]*elm->dNa[dim*eld*el_no+3*i2+2]);
	                if (eld == 3)
                        aij -= A*elm->vol_el[el_no]/elm->vol_n[l]*
		                (elm->dNa[dim*eld*el_no+dim*i1]*elm->dNa[dim*eld*el_no+dim*i2]+
		                elm->dNa[dim*eld*el_no+dim*i1+1]*elm->dNa[dim*eld*el_no+dim*i2+1]);
        	    }
	    }
	    if(fabs(aij) > small) spl++;
      }
    }
  }
  return(spl);
}
/* End of CalcLofSpMatExch()  */
/*----------------------------------------------------------------------*/
void CalcSpMatExch(AKElm *elm, RISSprMat *spm, RISSprMat *spmt)
{
  int spl,i,j,l,el,el_no, numadjn, both, mindex;
  int nx, i1, i2, eld, node, dim, ii;
  double A, Js1, aij, dtet, small;
  spl = 1;
  nx = elm->nx;
  dtet = elm->dtet;
  dim = elm->dim;
  eld = elm->eld;
  small = elm->small;
   /* First diagonal elements */
  for(j=0;j<nx;j++) {
    spm->sa[j] = 0.0;
    for(el=0;el<elm->num_el[j];el++) {
      el_no = elm->nel_list[elm->pnel_list[j]+el];
      mindex = elm->ijk[eld][el_no]-1;
      for(node=0;node<eld;node++) {
	      if(elm->ijk[node][el_no] == j) {
            A = elm->params[mindex].A[j];
            if (eld == 4)
                spm->sa[j] -= A*elm->vol_el[el_no] / elm->vol_n[j] *
		        (elm->dNa[dim * eld * el_no + 3 * node] * elm->dNa[dim * eld * el_no + 3 * node] +
		        elm->dNa[dim * eld * el_no + 3 * node + 1] * elm->dNa[dim * eld * el_no + 3 * node + 1] +
		        elm->dNa[dim * eld * el_no + 3 * node + 2] * elm->dNa[dim * eld * el_no + 3 * node + 2]);
            if (eld == 3)
                spm->sa[j] -= A*elm->vol_el[el_no] / elm->vol_n[j] *
		        (elm->dNa[dim * eld * el_no + dim * node] * elm->dNa[dim * eld * el_no + dim * node] +
		        elm->dNa[dim * eld * el_no + dim * node + 1] * elm->dNa[dim * eld * el_no + dim * node + 1]);
          }
	  }
    }
  }
  /* Now the off-diagonal elements */
  spm->ija[0] = nx+1;
  spm->inx = nx;
  spl = nx;

  for(i=0;i<nx;i++) {
    numadjn = elm->pnx_list[i];
    for(j=0;j<numadjn;j++) {
      l = elm->nx_list[elm->pnx_list[nx+i]+j];
      if(i != l) {
	    aij = 0.0;
	    for(el=0;el<elm->num_el[l];el++) {
	        el_no = elm->nel_list[elm->pnel_list[l]+el];
	        mindex = elm->ijk[eld][el_no]-1;
            both = ContainNodes(i,l,elm,el_no,&i1,&i2);
            if(both) {
                A = (elm->params[mindex].A[i] * elm->vol_n[i] + elm->params[mindex].A[l] * elm->vol_n[l])/
                    ((elm->vol_n[i] + elm->vol_n[l]));
	            if (eld == 4)
		                aij -= A*elm->vol_el[el_no]/elm->vol_n[l]*
		                (elm->dNa[dim*eld*el_no+dim*i1]*elm->dNa[dim*eld*el_no+dim*i2]+
		                elm->dNa[dim*eld*el_no+dim*i1+1]*elm->dNa[dim*eld*el_no+dim*i2+1]+
		                elm->dNa[dim*eld*el_no+dim*i1+2]*elm->dNa[dim*eld*el_no+dim*i2+2]);
	            if (eld == 3)
                        aij -= A*elm->vol_el[el_no]/elm->vol_n[l]*
		                (elm->dNa[dim*eld*el_no+dim*i1]*elm->dNa[dim*eld*el_no+dim*i2]+
		                elm->dNa[dim*eld*el_no+dim*i1+1]*elm->dNa[dim*eld*el_no+dim*i2+1]);
	        }
	    }
	    if(fabs(aij) > small) {
	        spl++;
	        spm->sa[spl] = aij;
	        spm->ija[spl] = l;
	    }

      }
    }
    spm->ija[i+1] = spl+1;
  }
  sprstp(spm->sa,spm->ija,spmt->sa,spmt->ija);
}
/* End of CalcSpMatExch()  */
/*----------------------------------------------------------------------*/

// Calculation of the divergence Sparse matrix

int CalcLofSpMatDiv(AKElm *elm, int comp)
{
  int nx, dim, eld, spl;
  int i,j,l,el,el_no,node, numadjn, both,mindex;
  double aij, dd, dtet, Bs, small;

  spl  = 1;
  nx = elm->nx;
  dim = elm->dim;
  eld = elm->eld;
  small = elm->small;
  dtet = elm->dtet;
  dd = 1.0/eld;
//  dd = 1.0/(eld*D);

  for(i=0;i<nx;i++) {
    numadjn = elm->pnx_list[i];
    for(j=0;j<numadjn;j++) {
      l = elm->nx_list[elm->pnx_list[nx+i]+j];
      if(i != l) {
	       aij = 0.0;
	       for(el=0;el<elm->num_el[l];el++) {
	          el_no = elm->nel_list[elm->pnel_list[l]+el];
	          mindex = elm->ijk[eld][el_no]-1;
//	          Bs = elm->params[mindex].Bs;
	          for(node=0;node<eld;node++)
	             if(elm->ijk[node][el_no] == i)
                     aij +=  dd*elm->vol_el[el_no]*elm->dNa[dim*eld*el_no+dim*node+comp];
//  	             aij +=  dd*elm->vol_el[el_no]*elm->dNa[dim*eld*el_no+dim*node+comp]*Bs*dmu0;
	       }
	       if(fabs(aij) > small) spl++;
      }
    }
  }

  return(spl);
}
/* End of CalcLofSpMatDiv()  */
/*----------------------------------------------------------------------*/
void CalcSpMatDiv(AKElm *elm, RISSprMat *spm, int comp)
{
  int nx, dim, eld, spl, SPLMAX;
  int i,j,k, l, el, el_no, node, numadjn, both, mindex;
  double Ms, aij, dd, small, dtet, Bs, dmu0 = 1.0/(mu0);

  nx = elm->nx;
  dim = elm->dim;
  eld = elm->eld;
  small = elm->small;
  dtet = elm->dtet;
  dd = 1.0/eld;
//  dd = 1.0/(eld*D);

  SPLMAX = spm->ilx;

  // First diagonal elements
  for(j=0;j<nx;j++) {
    spm->sa[j] = 0.0;
    for(el=0;el<elm->num_el[j];el++) {
      el_no = elm->nel_list[elm->pnel_list[j]+el];
      mindex = elm->ijk[eld][el_no]-1;
//      Bs = elm->params[mindex].Bs;
      for(node=0;node<eld;node++) {
	       if(elm->ijk[node][el_no] == j)
              spm->sa[j] += dd*elm->vol_el[el_no]*elm->dNa[dim*eld*el_no+dim*node+comp];
//   	       spm->sa[j] += dd*elm->vol_el[el_no]*elm->dNa[dim*eld*el_no+dim*node+comp]*Bs*dmu0;
      }
    }
  }

  // Now the off-diagonal elements
  spm->ija[0] = nx+1;
  k = nx;
  for(i=0;i<nx;i++) {
    numadjn = elm->pnx_list[i];
    for(j=0;j<numadjn;j++) {
      l = elm->nx_list[elm->pnx_list[nx+i]+j];
      if(i != l) {
	       aij = 0.0;
	       for(el=0;el<elm->num_el[l];el++) {
	          el_no = elm->nel_list[elm->pnel_list[l]+el];
	          mindex = elm->ijk[eld][el_no]-1;
//	          Bs = elm->params[mindex].Bs;
	          for(node=0;node<eld;node++)
	             if(elm->ijk[node][el_no] == i)
                   aij += dd*elm->vol_el[el_no]*elm->dNa[dim*eld*el_no+dim*node+comp];
//  	             aij += dd*elm->vol_el[el_no]*elm->dNa[dim*eld*el_no+dim*node+comp]*Bs*dmu0;
	       }
	       if(fabs(aij) > small) {
	          k++;
	          if(k > SPLMAX)
	             printf("Something is wrong in the Load SP Matrix creation!\n");
	          spm->sa[k] = aij;
	          spm->ija[k] = l;
	        }
      }
    }
    spm->ija[i+1] = k+1;
  }

}
/* End of CalcSpMatLoad()  */
/*----------------------------------------------------------------------*/
int CalcLofSpMatPoisson(AKElm *elm)
{
  int nx, spl,i,j,l,el,el_no, numadjn, both;
  int i1, i2, dim, eld;
  double aij, small;

  nx = elm->nx;
  dim = elm->dim;
  eld = elm-> eld;
  small = elm->small;

  spl = 1;

  for(i=0;i<nx;i++) {
    numadjn = elm->pnx_list[i];
    for(j=0;j<numadjn;j++) {
      l = elm->nx_list[elm->pnx_list[nx+i]+j];
      if(i != l) {
	       aij = 0.0;
	        for(el=0;el<elm->num_el[l];el++) {
	           el_no = elm->nel_list[elm->pnel_list[l]+el];
	           both = ContainNodes(i,l,elm,el_no,&i1,&i2);
	           if(both) {
               if(eld == 4)
                  aij +=  elm->vol_el[el_no]*
	                 (elm->dNa[dim*eld*el_no+dim*i1]*elm->dNa[dim*eld*el_no+dim*i2]+
	                  elm->dNa[dim*eld*el_no+dim*i1+1]*elm->dNa[dim*eld*el_no+dim*i2+1]+
	                  elm->dNa[dim*eld*el_no+dim*i1+2]*elm->dNa[dim*eld*el_no+dim*i2+2]);
               if(eld == 3)
                   aij +=  elm->vol_el[el_no]*
                  (elm->dNa[dim*eld*el_no+dim*i1]*elm->dNa[dim*eld*el_no+dim*i2]+
                   elm->dNa[dim*eld*el_no+dim*i1+1]*elm->dNa[dim*eld*el_no+dim*i2+1]);

	           }
	           if(fabs(aij) > small) spl++;
          }
      }
    }
  }
  return(spl);
}
/* End of CalcLofSpMatPoisson()  */
/*----------------------------------------------------------------------*/
void CalcSpMatPoisson(AKElm *elm, RISSprMat *spm)
{
  int nx, i,j,k,l,el,el_no, node,numadjn, both;
  int i1, i2, dim, eld, SPLMAX;
  double aij, small;

  nx = elm->nx;
  dim = elm->dim;
  eld = elm-> eld;
  small = elm->small;
  SPLMAX = spm->ilx;

//  First diagonal elements
  for(j=0;j<nx;j++) {
    spm->sa[j] = 0.0;
    for(el=0;el<elm->num_el[j];el++) {
      el_no = elm->nel_list[elm->pnel_list[j]+el];
      for(node=0;node<eld;node++) {
	       if(elm->ijk[node][el_no] == j) {
            if(eld == 4)
	            spm->sa[j] += elm->vol_el[el_no]*
	               (elm->dNa[dim*eld*el_no+dim*node]*elm->dNa[dim*eld*el_no+dim*node]+
	                elm->dNa[dim*eld*el_no+dim*node+1]*elm->dNa[dim*eld*el_no+dim*node+1]+
	                elm->dNa[dim*eld*el_no+dim*node+2]*elm->dNa[dim*eld*el_no+dim*node+2]);
            if (eld == 3)
              spm->sa[j] += elm->vol_el[el_no]*
                  (elm->dNa[dim*eld*el_no+dim*node]*elm->dNa[dim*eld*el_no+dim*node]+
                  elm->dNa[dim*eld*el_no+dim*node+1]*elm->dNa[dim*eld*el_no+dim*node+1]);
          }

      }
    }
  }
//   Now the off-diagonal elements
  spm->ija[0] = nx+1;
  k = nx;
  for(i=0;i<nx;i++) {
    numadjn = elm->pnx_list[i];
    for(j=0;j<numadjn;j++) {
      l = elm->nx_list[elm->pnx_list[nx+i]+j];
      if(i != l) {
	       aij = 0.0;
	       for(el=0;el<elm->num_el[l];el++) {
	          el_no = elm->nel_list[elm->pnel_list[l]+el];
	          both = ContainNodes(i,l,elm,el_no,&i1,&i2);
	          if(both) {
              if(eld == 4)
                 aij +=  elm->vol_el[el_no]*
                  (elm->dNa[dim*eld*el_no+dim*i1]*elm->dNa[dim*eld*el_no+dim*i2]+
                   elm->dNa[dim*eld*el_no+dim*i1+1]*elm->dNa[dim*eld*el_no+dim*i2+1]+
                   elm->dNa[dim*eld*el_no+dim*i1+2]*elm->dNa[dim*eld*el_no+dim*i2+2]);
              if(eld == 3)
               aij +=  elm->vol_el[el_no]*
                 (elm->dNa[dim*eld*el_no+dim*i1]*elm->dNa[dim*eld*el_no+dim*i2]+
                  elm->dNa[dim*eld*el_no+dim*i1+1]*elm->dNa[dim*eld*el_no+dim*i2+1]);

            }

          }
	        if(fabs(aij) > small) {
	           k++;
	           if(k > SPLMAX)
	              printf("There was some problem with the Poisson sparse matrix\nlength determination!!!\n");
	            spm->sa[k] = aij;
	            spm->ija[k] = l;
	       }
      }
    }
    spm->ija[i+1] = k+1;
  }

}
/* End of CalcSpMatPoisson()  */
/*----------------------------------------------------------------------*/
int CalcLofSpMatLaplace(AKElm *elm)
{
  int nx, dim, eld, bnx;
  int spl,i,j,l,el,el_no, numadjn, both;
  int cbn, i1, i2;
  double aij, small;

  nx = elm->nx;
  dim = elm->dim;
  eld = elm->eld;
  bnx = elm->bnx;
  small = elm->small;
  spl = 1;

  elm->atbnd = (int *) malloc(nx*sizeof(int));
  IniIVector(elm->atbnd, nx, 0);

  for(i=0; i<bnx; i++) {
      cbn = elm->bnlist[i];
      elm->atbnd[cbn] = 1;
  }
  for(i=0;i<nx;i++) {
    if(!elm->atbnd[i]) {
      numadjn = elm->pnx_list[i];
      for(j=0;j<numadjn;j++) {
	       l = elm->nx_list[elm->pnx_list[nx+i]+j];
	       if(!elm->atbnd[l]) {
	          if(i != l) {
	             aij = 0.0;
	             for(el=0;el<elm->num_el[l];el++) {
	                el_no = elm->nel_list[elm->pnel_list[l]+el];
	                both = ContainNodes(i,l,elm,el_no,&i1,&i2);
	                if(both) {
                      if(eld == 4)
                        aij +=  elm->vol_el[el_no]*
                      		(elm->dNa[dim*eld*el_no+dim*i1]*elm->dNa[dim*eld*el_no+dim*i2]+
                      		 elm->dNa[dim*eld*el_no+dim*i1+1]*elm->dNa[dim*eld*el_no+dim*i2+1]+
                      		 elm->dNa[dim*eld*el_no+dim*i1+2]*elm->dNa[dim*eld*el_no+dim*i2+2]);
                      if(eld == 3)
                             aij +=  elm->vol_el[el_no]*
                           		(elm->dNa[dim*eld*el_no+dim*i1]*elm->dNa[dim*eld*el_no+dim*i2]+
                           		 elm->dNa[dim*eld*el_no+dim*i1+1]*elm->dNa[dim*eld*el_no+dim*i2+1]);
                    }
                }
              }
	            if(fabs(aij) > small) spl++;
	         }
	     }
    }
  }
  return(spl);
}
/*----------------------------------------------------------------------*/
void CalcSpMatLaplace(AKElm *elm, RISSprMat *spm, RISSprMat *spmp)
{
  int nx, dim, eld, bnx;
  int i,j,k,l,el,el_no, numadjn, both, SPLMAX;
  int i1, i2;
  double aij, small;

  nx = elm->nx;
  dim = elm->dim;
  eld = elm->eld;
  bnx = elm->bnx;
  SPLMAX = spm->ilx;
  small = elm->small;

//#pragma parallel for
  for(i=0; i<nx; i++) spm->sa[i] = spmp->sa[i];

//#pragma parallel for
  for(i=0; i<bnx; i++) spm->sa[elm->bnlist[i]] = 1.0;

  // Now the off-diagonal elements
  spm->ija[0] = nx+1;
  k = nx;
  for(i=0;i<nx;i++) {
    if(!elm->atbnd[i]) {
      numadjn = elm->pnx_list[i];
      for(j=0;j<numadjn;j++) {
	       l = elm->nx_list[elm->pnx_list[nx+i]+j];
	       if(!elm->atbnd[l]) {
	          if(i != l) {
	             aij = 0.0;
	             for(el=0;el<elm->num_el[l];el++) {
	                el_no = elm->nel_list[elm->pnel_list[l]+el];
	                both = ContainNodes(i,l,elm,el_no,&i1,&i2);
                  if(both) {
                      if(eld == 4)
                        aij +=  elm->vol_el[el_no]*
                      		(elm->dNa[dim*eld*el_no+dim*i1]*elm->dNa[dim*eld*el_no+dim*i2]+
                      		 elm->dNa[dim*eld*el_no+dim*i1+1]*elm->dNa[dim*eld*el_no+dim*i2+1]+
                      		 elm->dNa[dim*eld*el_no+dim*i1+2]*elm->dNa[dim*eld*el_no+dim*i2+2]);
                      if(eld == 3)
                             aij +=  elm->vol_el[el_no]*
                           		(elm->dNa[dim*eld*el_no+dim*i1]*elm->dNa[dim*eld*el_no+dim*i2]+
                           		 elm->dNa[dim*eld*el_no+dim*i1+1]*elm->dNa[dim*eld*el_no+dim*i2+1]);
                  }
               }
	             if(fabs(aij) > small) {
	                k++;
	                if(k > SPLMAX)
	                    printf("There was some problem with the Laplace sparse matrix\nlength determination!!!\n");
	                spm->sa[k] = aij;
	                spm->ija[k] = l;
	             }
	          }
	       }
      }
    }
    spm->ija[i+1] = k+1;
  }
}
/* End of CalcSpMatLaplace()  */
/*----------------------------------------------------------------------*/
int CalcLofSpMatGrad(AKElm *elm, int comp)
{

  int nx, dim, eld, spl;
  int i, j, l, el, el_no, node, numadjn, both;
  double aij, dd, d_mu0=1.0/mu0, small;

  nx = elm->nx;
  dim = elm->dim;
  eld = elm->eld;
  small = elm->small;
  dd = 1.0/eld;

  spl = 1;

  for(i=0; i<nx; i++) {
    numadjn = elm->pnx_list[i];
    for(j=0; j<numadjn; j++) {
      l = elm->nx_list[elm->pnx_list[nx+i]+j];
      if(i != l) {
	       aij = 0.0;
	       for(el=0; el<elm->num_el[l]; el++) {
	          el_no = elm->nel_list[elm->pnel_list[l]+el];
	          for(node=0; node<eld; node++)
	             if(elm->ijk[node][el_no] == i)
                    aij +=  dd*elm->vol_el[el_no]/elm->vol_n[l]*(-elm->dNa[dim*eld*el_no+dim*node+comp]);
	       }
	       if(fabs(aij) > small) spl++;
      }
    }
  }
  return(spl);
}
/* End of CalcLofSpMatGrad()  */
/*----------------------------------------------------------------------*/
void CalcSpMatGrad(AKElm *elm, RISSprMat *spm, int comp)
{

  int nx, dim, eld, spl;
  int i, j, l, el, el_no, node, numadjn, both, SPLMAX;
  double aij, dd, d_mu0=1.0/mu0, small;
  RISSprMat spmt;

  N_RISSprMat(&spmt,spm->ilx);

  nx = elm->nx;
  dim = elm->dim;
  eld = elm->eld;
  small = elm->small;
  SPLMAX = spm->ilx;
  dd = 1.0/eld;

  // First diagonal elements
  for(j=0; j<nx; j++) {
    spmt.sa[j] = 0.0;
    for(el=0; el<elm->num_el[j]; el++) {
      el_no = elm->nel_list[elm->pnel_list[j]+el];
      for(node=0; node<eld; node++) {
	       if(elm->ijk[node][el_no] == j)
	         spmt.sa[j] += dd*elm->vol_el[el_no]/elm->vol_n[j]*(-elm->dNa[dim*eld*el_no+dim*node+comp]);
      }
    }
  }

  // Now the off-diagonal elements
  spmt.ija[0] = nx+1;
  spl = nx;
  for(i=0; i<nx; i++) {
    numadjn = elm->pnx_list[i];
    for(j=0; j<numadjn; j++) {
      l = elm->nx_list[elm->pnx_list[nx+i]+j];
      if(i != l) {
	       aij = 0.0;
	       for(el=0; el<elm->num_el[l]; el++) {
	          el_no = elm->nel_list[elm->pnel_list[l]+el];
	          for(node=0; node<eld; node++)
	             if(elm->ijk[node][el_no] == i)
                    aij +=  dd*elm->vol_el[el_no]/elm->vol_n[l]*(-elm->dNa[dim*eld*el_no+dim*node+comp]);
	       }
	       if(fabs(aij) > small) {
	          spl++;
	          if(spl > SPLMAX)
	             printf("Something is wrong in the Hdem SP Matrix creation!\n");
	          spmt.sa[spl] = aij;
	          spmt.ija[spl] = l;
	       }
      }
    }
    spmt.ija[i+1] = spl+1;
  }
  sprstp(spmt.sa,spmt.ija,spm->sa,spm->ija);
  F_RISSprMat(&spmt);
}
/* End of CalcSpMatGrad()  */
/*----------------------------------------------------------------------*/
int CalcLofSpMatDMIbulk(AKElm *elm, int comp)
{

  int nx, dim, eld, spl;
  int i, j, l, el, el_no, node, numadjn, both, i1, i2;
  double aij, dd, d_mu0=1.0/mu0, small;

  nx = elm->nx;
  dim = elm->dim;
  eld = elm->eld;
  small = elm->small;
  dd = 1.0/eld;

  spl = 1;

  for(i=0; i<nx; i++) {
    numadjn = elm->pnx_list[i];
    for(j=0; j<numadjn; j++) {
      l = elm->nx_list[elm->pnx_list[nx+i]+j];
      if(i != l) {
	       aij = 0.0;
	       for(el=0; el<elm->num_el[l]; el++) {
	          el_no = elm->nel_list[elm->pnel_list[l]+el];
//	          for(node=0; node<eld; node++)
//              both = ContainNodes(i,l,elm,el_no,&i1,&i2);
//              if(both)
//                if(eld == 4)
//                     aij -=  dd*elm->vol_el[el_no]/elm->vol_n[l]*elm->dNa[dim*eld*el_no+dim*i1+comp];
 	          for(node=0; node<eld; node++)
	             if(elm->ijk[node][el_no] == i && elm->ijk_is_boundary[el_no])
                    aij -=  dd*elm->vol_el[el_no]/elm->vol_n[l]*elm->dNa[dim*eld*el_no+dim*node+comp];
//	             if(elm->ijk[node][el_no] == i)
//                    aij +=  dd*elm->vol_el[el_no]/elm->vol_n[l]*(-elm->dNa[dim*eld*el_no+dim*node+comp]);
	       }
	       if(fabs(aij) > small) spl++;
      }
    }
  }
  return(spl);
}
/* End of CalcLofSpMatDMIbul()  */
/*----------------------------------------------------------------------*/
void CalcSpMatDMIbulk(AKElm *elm, RISSprMat *spm, int comp)
{

  int nx, dim, eld, spl;
  int i, j, l, el, el_no, node, numadjn, both, SPLMAX, i1, i2;
  double aij, dd, d_mu0=1.0/mu0, small;

  nx = elm->nx;
  dim = elm->dim;
  eld = elm->eld;
  small = elm->small;
  SPLMAX = spm->ilx;
  dd = 1.0/eld;

  // First diagonal elements
  for(j=0; j<nx; j++) {
    spm->sa[j] = 0.0;
    for(el=0; el<elm->num_el[j]; el++) {
      el_no = elm->nel_list[elm->pnel_list[j]+el];
      for(node=0; node<eld; node++) {
	       if(elm->ijk[node][el_no] == j && elm->ijk_is_boundary[el_no])
	         spm->sa[j] += dd*elm->vol_el[el_no]/elm->vol_n[j]*(-elm->dNa[dim*eld*el_no+dim*node+comp]);
      }
    }
  }

  // Now the off-diagonal elements
  spm->ija[0] = nx+1;
  spl = nx;
  for(i=0; i<nx; i++) {
    numadjn = elm->pnx_list[i];
    for(j=0; j<numadjn; j++) {
      l = elm->nx_list[elm->pnx_list[nx+i]+j];
      if(i != l) {
	       aij = 0.0;
	       for(el=0; el<elm->num_el[l]; el++) {
	          el_no = elm->nel_list[elm->pnel_list[l]+el];
//              both = ContainNodes(i,l,elm,el_no,&i1,&i2);
//              if(both)
//                if(eld == 4)
 	          for(node=0; node<eld; node++)
	             if(elm->ijk[node][el_no] == i && elm->ijk_is_boundary[el_no])
                    aij -=  dd*elm->vol_el[el_no]/elm->vol_n[l]*elm->dNa[dim*eld*el_no+dim*node+comp];

//	          for(node=0; node<eld; node++)
//	             if(elm->ijk[node][el_no] == i)
//                    aij +=  dd*elm->vol_el[el_no]/elm->vol_n[l]*(-elm->dNa[dim*eld*el_no+dim*node+comp]);
	       }
	       if(fabs(aij) > small) {
	          spl++;
	          if(spl > SPLMAX)
	             printf("Something is wrong in the Hdem SP Matrix creation!\n");
	          spm->sa[spl] = aij;
	          spm->ija[spl] = l;
	       }
      }
    }
    spm->ija[i+1] = spl+1;
  }
}
/* End of CalcSpMatDMIbulk()  */
/*----------------------------------------------------------------------*/
/**
 * Construct the following matrix:
 *
 * ( 0 grad_z -grad_y )
 * ( -grad_z 0 grad_x )
 * ( grad_y -grad_x 0 )
 */
RISSprMat rotation_operator(RISSprMat phix, RISSprMat phiy, RISSprMat phiz) {
    int n = phix.ija[0] - 1;
    int l_x = phix.ija[n];  // length of phix.sa / phix.ija
    int l_y = phiy.ija[n];
    int l_z = phiz.ija[n];
    RISSprMat result;
    N_RISSprMat(&result, 2*l_x + 2*l_y + 2*l_z + 1);
    result.ija[0] = 3*n + 1;
    for (int i = 0; i < 3*n; ++i) {
        result.sa[i] = 0;
    }
    int k = 3*n;
    for (int r_b = 0; r_b < n; ++r_b) {
        // row-block-index b
        for (int r_c = 0; r_c < 3; ++r_c) {
            // row-coordinate-index r_c
            for (int c_b = 0; c_b < n; ++c_b) {
                // column-block-index c_b
                for (int c_c = 0; c_c < 3; ++c_c) {
                    double col = 3*c_b + c_c;
                    double p;
                    // column-coordinate-index c_c
                    if (r_c == 0 && c_c == 0) {  // x x
                        continue;  // diagonal is zero
                    } else if (r_c == 0 && c_c == 1) {  // x y
                        p = sparsemat_at(phiz, r_b, c_b);
                    } else if (r_c == 0 && c_c == 2) {  // x z
                        p = - sparsemat_at(phiy, r_b, c_b);
                    } else if (r_c == 1 && c_c == 0) {  // y x
                        p = - sparsemat_at(phiz, r_b, c_b);
                    } else if (r_c == 1 && c_c == 1) {  // y y
                        continue;
                    } else if (r_c == 1 && c_c == 2) {  // y z
                        p = sparsemat_at(phix, r_b, c_b);
                    } else if (r_c == 2 && c_c == 0) {  // z x
                        p = sparsemat_at(phiy, r_b, c_b);
                    } else if (r_c == 2 && c_c == 1) {  // z y
                        p = - sparsemat_at(phix, r_b, c_b);
                    } else if (r_c == 2 && c_c == 2) {  // z z
                        continue;
                    }
                    if (p != 0) {
                        result.sa[k] = p;
                        result.ija[k] = col;
                    }
                }
            }
            result.ija[r_b*3 + r_c +1] = k + 1;
        }
    }
    return result;
}
/*----------------------------------------------------------------------*/
/**
 * Return x[i, j].
 */
double sparsemat_at(RISSprMat x, int i, int j) {
    if (i == j) {
        return x.sa[i];
    }
    int k = x.ija[i];
    while (x.ija[k] < j) {
        ++k;
    }
    if (x.ija[k] == j) {
        return x.sa[k];
    } else {
        return 0.;
    }
}
/*----------------------------------------------------------------------*/

