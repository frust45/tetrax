//
// Created by Attila Kákay on 14.04.21.
//

#ifndef TETRAX_DENSE2D_ONTHEFLY_H
#define TETRAX_DENSE2D_ONTHEFLY_H

/*----------------------------------------------------------------------*/
void ComputeDenseMatrix_kdep_py(int nx, int *bijk, int bnx, int bnelx,
                                int *bnlist, double *xyz, double *nv,
                                double *pang, int op, double k,
                                double *D2Dk);
/*----------------------------------------------------------------------*/
double dGreendnv2Dkdep_py(double *r, double *rp, double *nv, double k);
/*----------------------------------------------------------------------*/
double dGreendnv2D_py(double *r, double *rp, double *nv);
/*----------------------------------------------------------------------*/
void HOBoundaryElm2D_py(double *x0, double *x1, double *x2,
                   double *nv, double A, double *wg, int op, double kwave);
/*----------------------------------------------------------------------*/
void LinSplit_py(int *ijk, int **ijk_n, double *xyz, double **w, int *nc);
/*----------------------------------------------------------------------*/
double dot_prod2D_py(double *a, double *b);
/*----------------------------------------------------------------------*/
double vectabs2D_py(double *r);
/*----------------------------------------------------------------------*/
//int **IMatrix(int ni, int nj);
/*----------------------------------------------------------------------*/
//void FreeIMatrix(int **p);
/*----------------------------------------------------------------------*/
//double **DMatrix(int ni, int nj);
/*----------------------------------------------------------------------*/
//void FreeDMatrix(double **p);
/*----------------------------------------------------------------------*/
#endif //TETRAX_DENSE2D_ONTHEFLY_H
