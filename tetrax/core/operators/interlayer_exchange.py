import logging

from scipy.constants import mu_0
from scipy.sparse import csr_matrix, diags, lil_matrix
from scipy.sparse.linalg import spsolve, LinearOperator, lgmres, bicg

from ...helpers.math import *


class InterlayerExchangeOperator(LinearOperator):

    def __init__(self, sample):
        self.sparse_mat = lil_matrix((3 * sample.nx, 3 * sample.nx))
        self.Msat = sample.Msat

        if (sample._dim == 1) and (sample.nb > 2):
            sample.J1 = [sample.J1] if not isinstance(sample.J1,list) else sample.J1
            sort_indices = np.argsort(sample.xyz[sample.boundary_nodes, 1])
            sparse_block = lil_matrix((sample.nx, sample.nx))
            spacer_index = 0
            for node, next in zip(sample.boundary_nodes[sort_indices[1::2]], sample.boundary_nodes[sort_indices[2::2]]):
                sparse_block[node, next] = sparse_block[next, node] = sample.J1[spacer_index] * 2 / (sample.dA[node] + sample.dA[next])
                spacer_index += 1
            #diagonal = np.zeros(sample.nx)
            #diagonal[sample.boundary_nodes] = -np.ones(sample.nb)/sample.dA[sample.boundary_nodes]

            #sparse_block -= dia_matrix((diagonal, 0), shape=sparse_block.shape)
            self.sparse_mat = bmat([[sparse_block, None, None],
                                    [None, sparse_block, None],
                                    [None, None, sparse_block]])
            self.sparse_mat *= -1 / (mu_0 * sample.scale)

        self.shape = self.sparse_mat.shape
        self.sparse_mat.tocsr()
        self.dtype = np.complex128

    def _matvec(self, vec):
        return self.sparse_mat.dot(vec) / np.tile(self.Msat ** 2, 3)
