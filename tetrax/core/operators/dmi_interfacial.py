import logging

from scipy.constants import mu_0
from scipy.sparse import csr_matrix, diags, lil_matrix
from scipy.sparse.linalg import spsolve, LinearOperator, lgmres, bicg

from ...helpers.math import *

def _construct_diff_and_pi_matrices(sample):
    grad_x = sample.grad_x
    grad_y = sample.grad_y
    nx = sample.nx

    # for now symmetry breaking direction is in y.
    d_vec = np.repeat(np.array([0, 1, 0]), nx, axis=0).flatten()

    # construct Pi matrix (for k-dep part)
    d_vec_x = d_vec[:nx]
    d_vec_x_diag = dia_matrix((d_vec_x, 0), shape=(nx, nx))

    d_vec_y = d_vec[nx:-nx]
    d_vec_y_diag = dia_matrix((d_vec_y, 0), shape=(nx, nx))
    zeros_block = csr_matrix((nx, nx))

    Pi_mat = bmat([[zeros_block, zeros_block, 1j * d_vec_x_diag],
                        [zeros_block, zeros_block, 1j * d_vec_y_diag],
                        [-1j * d_vec_x_diag, -1j * d_vec_y_diag, zeros_block]])

    diff_mat = bmat([[d_vec_x_diag.dot(grad_x), d_vec_x_diag.dot(grad_y), zeros_block],
                          [d_vec_y_diag.dot(grad_x), d_vec_y_diag.dot(grad_y), zeros_block],
                          [zeros_block, zeros_block, zeros_block]]) - \
                    bmat([[grad_x.dot(d_vec_x_diag), grad_x.dot(d_vec_y_diag), zeros_block],
                          [grad_y.dot(d_vec_x_diag), grad_y.dot(d_vec_y_diag), zeros_block],
                          [zeros_block, zeros_block, zeros_block]])

    return diff_mat, Pi_mat

class InterfacialDMIOperator(LinearOperator):

    def __init__(self, sample):
        self.fac = 2 * sample.Didmi / (mu_0 * sample.scale)
        self.Msat = sample.Msat
        self.diff_mat, self.Pi_mat = _construct_diff_and_pi_matrices(sample)
        self.update(sample)

    def update(self,sample):
        self.fac = 2 * sample.Didmi / (mu_0 * sample.scale)
        self.Msat = sample.Msat
        self.k = 0
        self.set_k(self.k)
#        self.sparse_mat = self.fac * (self.diff_mat + self.k * self.Pi_mat)
        self.shape = self.sparse_mat.shape

    def set_k(self, k):
        self.k = k
        self.sparse_mat = self.fac * (self.diff_mat + self.k * self.Pi_mat)

    def _matvec(self, x):
        return self.sparse_mat.dot(x)/np.tile(self.Msat ** 2, 3)


class InterfacialDMIOperatorAFM(LinearOperator):

    def __init__(self, sample):
#        self.fac = 2 * sample.Didmi * (sample.Msat ** 2 * mu_0 * sample.scale)
#        self.k = 0
        self.diff_matFM, self.Pi_matFM = _construct_diff_and_pi_matrices(sample)
        self.diff_mat = bmat([[None, self.diff_matFM],[self.diff_matFM, None]])
        self.Pi_mat = bmat([[None, self.Pi_matFM], [self.Pi_matFM, None]])
#        self.sparse_mat = self.fac * (self.diff_mat + self.k * self.Pi_mat)
#        self.shape = self.sparse_mat.shape
        self.update(sample)

    def update(self,sample):
        self.fac = 2 * sample.Didmi * (sample.Msat ** 2 * mu_0 * sample.scale)
        self.k = 0
        self.sparse_mat = self.fac * (self.diff_mat + self.k * self.Pi_mat)
        self.shape = self.sparse_mat.shape


    def set_k(self, k):
        self.k = k
        self.sparse_mat = self.fac * (self.diff_mat + self.k * self.Pi_mat)

    def _matvec(self, x):
        return self.sparse_mat.dot(x)