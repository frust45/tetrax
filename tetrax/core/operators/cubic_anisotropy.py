import logging

from scipy.constants import mu_0
from scipy.sparse import csr_matrix, diags, lil_matrix
from scipy.sparse.linalg import spsolve, LinearOperator, lgmres, bicg

from ...helpers.math import *


class CubicAnisotropyLinearOperator(LinearOperator):

    def __init__(self, sample):
        self.Kc = sample.Kc1
        self.Msat = sample.Msat
        self.nx = sample.nx
        self.m0 = sample.mag
        self.fac = 2 * self.Kc / (mu_0)
        self.sample = sample
#        For general case we might need to perform the cross product of the
        #        sample.v1Kc with sample.v2Kc to get sample.v3Kc = sample.v1Kc.cross(sample.v2Kc)
        sample.v3Kc = cross_product(sample.v1Kc,sample.v2Kc)

        # homogenous anisotropy
        self.c1 = np.repeat(np.array(sample.v1Kc), self.nx, axis=0).flatten()
        self.c2 = np.repeat(np.array(sample.v2Kc), self.nx, axis=0).flatten()
        self.c3 = np.repeat(np.array(sample.v3Kc), self.nx, axis=0).flatten()
        self.C1 = flattened_mesh_vec_tensor_product(self.c1, self.c1)
        self.C2 = flattened_mesh_vec_tensor_product(self.c2, self.c2)
        self.C3 = flattened_mesh_vec_tensor_product(self.c3, self.c3)

        self.sparse_mat = None
        self.shape = (3 * self.nx, 3 * self.nx)

    def make_sparse_mat_with_current_m0(self):
        # get current m0
        self.m0 = self.sample.mag.to_flattened()

        # create bare dyads

        M0 = flattened_mesh_vec_tensor_product(self.m0, self.m0)

        # these are spatially dependent!
        A1 = np.tile((flattened_mesh_vec_scalar_product(self.m0, self.c2)) ** 2 + (
            flattened_mesh_vec_scalar_product(self.m0, self.c3)) ** 2, 3).flatten()
        A2 = np.tile((flattened_mesh_vec_scalar_product(self.m0, self.c1)) ** 2 + (
            flattened_mesh_vec_scalar_product(self.m0, self.c3)) ** 2, 3).flatten()
        A3 = np.tile((flattened_mesh_vec_scalar_product(self.m0, self.c1)) ** 2 + (
            flattened_mesh_vec_scalar_product(self.m0, self.c2)) ** 2, 3).flatten()

        # A factors are pulled into the tensor product since they are spatially dependent
        Nc1 = flattened_mesh_vec_tensor_product(A1 * self.c1, self.c1) + 2 * self.C1.dot(M0).dot(self.C2 + self.C3)
        Nc2 = flattened_mesh_vec_tensor_product(A2 * self.c2, self.c2) + 2 * self.C1.dot(M0).dot(self.C2 + self.C3)
        Nc3 = flattened_mesh_vec_tensor_product(A3 * self.c3, self.c3) + 2 * self.C1.dot(M0).dot(self.C2 + self.C3)

        self.sparse_mat = self.fac * (Nc1 + Nc2 + Nc3)

    def set_new_param(self, conf, nx):
        """
        Set new parameters for unixial anisotropy.
        """
        raise NotImplementedError

    def _matvec(self, vec):
        # print(self.sparse_mat)
        # print(vec)
#        return self.sparse_mat.dot(vec)
        return self.sparse_mat.dot(vec) / np.tile(self.Msat ** 2, 3)

    def nonlinear_field_old(self, vec):
        """Full nonlinear anistropy. Returns the cubic anisotropy field of a given vector.
        This is not a linear operator."""
        A1 = np.tile((flattened_mesh_vec_scalar_product(vec, self.c2)) ** 2 + (
            flattened_mesh_vec_scalar_product(vec, self.c3)) ** 2, 3).flatten()
        A2 = np.tile((flattened_mesh_vec_scalar_product(vec, self.c1)) ** 2 + (
            flattened_mesh_vec_scalar_product(vec, self.c3)) ** 2, 3).flatten()
        A3 = np.tile((flattened_mesh_vec_scalar_product(vec, self.c1)) ** 2 + (
            flattened_mesh_vec_scalar_product(vec, self.c2)) ** 2, 3).flatten()

        hc1 = flattened_mesh_vec_tensor_product(A1 * self.c1, self.c1).dot(vec)
        hc2 = flattened_mesh_vec_tensor_product(A2 * self.c2, self.c2).dot(vec)
        hc3 = flattened_mesh_vec_tensor_product(A3 * self.c3, self.c3).dot(vec)

        hc = -self.fac * (hc1 + hc2 + hc3)
        return hc

    def nonlinear_field(self, vec):
        """Full nonlinear anistropy. Returns the cubic anisotropy field of a given vector.
        This is not a linear operator."""
        A1 = np.tile(
            (flattened_mesh_vec_scalar_product(vec, self.c2)) ** 2 + (
                flattened_mesh_vec_scalar_product(vec, self.c3)) ** 2,
            3).flatten()
        A2 = np.tile(
            (flattened_mesh_vec_scalar_product(vec, self.c1)) ** 2 + (
                flattened_mesh_vec_scalar_product(vec, self.c3)) ** 2,
            3).flatten()
        A3 = np.tile(
            (flattened_mesh_vec_scalar_product(vec, self.c1)) ** 2 + (
                flattened_mesh_vec_scalar_product(vec, self.c2)) ** 2,
            3).flatten()

        hc1 = A1 * np.tile(flattened_mesh_vec_scalar_product(vec, self.c1), 3).flatten() * self.c1
        hc2 = A2 * np.tile(flattened_mesh_vec_scalar_product(vec, self.c2), 3).flatten() * self.c2
        hc3 = A3 * np.tile(flattened_mesh_vec_scalar_product(vec, self.c3), 3).flatten() * self.c3

        hc = -self.fac * (hc1 + hc2 + hc3) / np.tile(self.Msat ** 2,3)
        return hc

def cross_product(u, v):
    return (u[1]*v[2] - u[2]*v[1],
            u[2]*v[0] - u[0]*v[2],
            u[0]*v[1] - u[1]*v[0])