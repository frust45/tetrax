import logging

from scipy.constants import mu_0
from scipy.sparse import csr_matrix, diags, lil_matrix
from scipy.sparse.linalg import spsolve, LinearOperator, lgmres, bicg

from ...helpers.math import *
from .dipole import N_dip

class TotalDynMat(LinearOperator):

    def __init__(self, N_nodip_mat, N_dip, leftmulmat, rightmulmat):

        # initialize demag operator with all k-idependent operators and quantities
        self.div_x = N_dip.div_x
        self.div_y = N_dip.div_y
        self.div_z = N_dip.div_z
        self.poisson = N_dip.poisson
        self.boundary_nodes = N_dip.boundary_nodes
        self.laplace = N_dip.laplace
        self.grad_x = N_dip.grad_x
        self.grad_y = N_dip.grad_y
        self.grad_z = N_dip.grad_z
        self.k = N_dip.k
        self.dense = N_dip.dense
        self.ksquared_mat = N_dip.ksquared_mat
        self.nx = N_dip.nx
        self.nb = N_dip.nb
        self.shape = 2 * self.nx, 2 * self.nx
        self.dtype = np.complex128
        self.sparse_mat = N_nodip_mat
        self.leftmulmat = leftmulmat
        self.rightmulmat = rightmulmat
        self.sparse_mat_k = None
        self.a_n = N_dip.a_n
        self.bndint_order = N_dip.bndint_order
        self.dim = N_dip.dim
        self.beta = N_dip.beta
        self.beta2 = np.tile(N_dip.beta, 3)
        self.Msat = N_dip.Msat
        self.Msat_avrg = N_dip.Msat_avrg
        self.compute_dense_matrix = N_dip.compute_dense_matrix
        self.no_dip = False

        # other stuff for dense matrix computation
        self.xyz = N_dip.xyz
        self.belm = N_dip.belm
        self.nv = N_dip.nv
        self.pang = N_dip.pang
        super().__init__(self.dtype, self.shape)

    def set_k(self, k, sparse_mat_k,no_dip):
        # should read in/calculate dense matrix and not take it, will be done later
        self.no_dip = no_dip
        to_compute = True
        if self.dense is None:
            if self.dim == 3:
                to_compute = False
            else:
                self.dense = np.empty((self.nb * self.nb))
#        print("To compute: ",to_compute)
        self.k = k
        self.ksquared_mat = k ** 2 * csr_matrix(diags(self.a_n))
        diag = self.a_n.copy()
        diag[self.boundary_nodes] = np.zeros(self.nb)
        self.ksquared_matb = k ** 2 * csr_matrix(diags(diag))
        self.sparse_mat_k = sparse_mat_k

        if to_compute:
            self.dense = np.reshape(self.dense, (self.nb * self.nb))
            dense_err = self.compute_dense_matrix(self.dense,
                                                  self.belm,
                                                  self.boundary_nodes,
                                                  self.xyz, self.nv,
                                                  self.pang,
                                                  self.bndint_order,
                                                  self.k)
            self.dense = np.reshape(self.dense, (self.nb, self.nb))

#            temp_dense = self.compute_dense_matrix(self.belm, self.boundary_nodes, self.xyz, self.nv, self.pang,
#                                                    self.bndint_order,
#                                                    self.k)
#            self.dense = np.reshape(temp_dense, (self.nb, self.nb))

    def _matvec(self, vec_loc):
        if self.k == None:
            raise ValueError(
                "No wave vector (k) has been assigned to the dynamic matrix yet. Do this by calling the set_k() method.")
        else:
            vec_lab = self.rightmulmat.dot(vec_loc)
            h_lab = (self.sparse_mat_k.dot(vec_lab) / (self.beta2 * self.Msat_avrg **2)) if self.no_dip else \
                (self.sparse_mat_k.dot(vec_lab) / (self.beta2 * self.Msat_avrg **2) +
                    N_dip(vec_lab, self.k, self.ksquared_mat, self.ksquared_matb,
                        self.nx, self.div_x, self.div_y, self.div_z, self.poisson,
                        self.boundary_nodes, self.dense, self.laplace,
                        self.grad_x, self.grad_y, self.grad_z,
                        self.a_n, self.dim, self.beta, self.Msat_avrg))

            return self.leftmulmat.dot(h_lab)


class Error(Exception):
    """Base class for exceptions in this module."""
    pass


class InvertError(Error):
    """Exception raised for errors in inversion of total dynamic matrix.
    """

    def __init__(self, k, after_iter, already_sucess, message):
        self.k = k
        self.after_iter = after_iter
        self.already_sucess = already_sucess
        self.message = message


class InvTotalDynMat(LinearOperator):
    """Inverse of Dynamic Matrix"""

    def __init__(self, demag_op, k, tol=1e-4, preconditioner=None, maxiter=1000):
        self.demag_op = demag_op
        self.shape = demag_op.shape
        self.dtype = np.complex128
        min_tol = 1000 * np.sqrt(self.shape[0]) * np.finfo(self.dtype).eps
        self.tol = max(min_tol, tol)
        self.preconditioner = preconditioner
        self.maxiter = maxiter
        self.counter = 0
        self.k = k

    def _matvec(self, x):
        logging.debug("inverse of {} requested".format(x))
        # reset demag_op so that it recalculates the demag field on first use
        self.demag_op.counter = 0
        b, info = lgmres(self.demag_op, x, tol=self.tol, atol=1000 * np.finfo(self.dtype).eps,
                         M=self.preconditioner, maxiter=self.maxiter)
        if info != 0:
            raise InvertError(self.k, info, self.counter,
                              "Error inverting dynamic matrix at k = {} : lgmres did not converge (info = {}). "
                              "The inverse was already calculated successfully {} times.".format(self.k, info,
                                                                                                 self.counter))
        self.counter += 1
        # logging.info("inverted: {} times | gmres iterations: {}".format(self.counter, self.demag_op.counter))
        # print("inverse computed")
        return b


class SuperLUInv(LinearOperator):
    """SuperLU object as LinearOperator"""

    def __init__(self, superlu):
        self.shape = superlu.shape
        self.dtype = np.complex128
        self.superlu = superlu

    def _matvec(self, x):
        return self.superlu.solve(x)


def cross_operator(nx):
    """
    Return the rotated cross product operator as a crs_matrix.
    """
    col_idcs = np.empty(2 * nx, dtype=int)
    row_idcs = np.empty(2 * nx, dtype=int)
    data = np.empty(2 * nx)
    data[:nx] = -1
    row_idcs[:nx] = np.arange(nx)
    col_idcs[:nx] = nx + np.arange(nx)
    data[nx:] = +1
    row_idcs[nx:] = nx + np.arange(nx)
    col_idcs[nx:] = np.arange(nx)
    return csr_matrix((data, (row_idcs, col_idcs)), shape=(3 * nx, 3 * nx))


