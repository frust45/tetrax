import logging

from scipy.constants import mu_0
from scipy.sparse import csr_matrix, diags, lil_matrix
from scipy.sparse.linalg import spsolve, LinearOperator, lgmres, bicg

from ..fem_core.cythoncore import ComputeDenseMatrix_2D, ComputeDenseMatrix_3D
from ..fem_core.fempreproc import ComputeDenseMatrix_1D
from ...helpers.math import *



def N_dip(m, k, ksquared_mat_a_n, ksquared_mat_a_nb, nx,
          div_x, div_y, div_z, poiss, boundary_nodes,
          dense, laplace, grad_x, grad_y, grad_z, a_n, dim, beta, Msat):
    # calculate charges
    #dispatcher = []

    m_x = m[:nx]
    m_y = m[nx:2 * nx]
    m_z = m[2 * nx:]

    rhs = div_x.dot(beta * m_x) + div_y.dot(beta * m_y) + div_z.dot(beta * m_z) + 1j * k * beta * m_z * a_n

    # (nable^2 - k^2) * psi1_k = rho_k
    # op = poiss - ksquared_mat_a_n*3
    # if 0.015 < k < 0.03:
    #    print(k, det(op.todense()))
    # rho_k2 = -(poiss - ksquared_mat_a_n).dot(rho_k)
    if (dim == 1):
        psi1, _ = bicg(poiss - ksquared_mat_a_n, rhs)
    else:
        psi1 = spsolve(poiss - ksquared_mat_a_n, rhs)

    # get boundary conditions for psi2

    sigma = np.zeros(nx) + 0j
    sigma[boundary_nodes] = dense.dot(psi1[boundary_nodes])

    # get imhom laplace
    sigma2 = (-1)**dim*(poiss - ksquared_mat_a_n).dot(sigma)
#    sigma2 = (poiss - ksquared_mat_a_n).dot(sigma)
    sigma2[boundary_nodes] = sigma[boundary_nodes]

    # solve for psi2 potential
    psi2 = spsolve(laplace - ksquared_mat_a_n, (-1)**(dim+1)*sigma2)

    # add up to make full potential
    psi = psi1 + psi2

    # calculate (negative) lateral dipolar field
    Nm_x = grad_x.dot(psi)
    Nm_y = grad_y.dot(psi)
#   Nm_z = grad_z.dot(psi)

    # calculate (negative) longitudinal dipolar field
    Nm_z = grad_z.dot(psi) + 1j * k * psi
    return np.array([Nm_x, Nm_y, Nm_z]).flatten()


class DipolarOperator(LinearOperator):

    def __init__(self, sample, bndint_order=6):

        # initialize dipolar operator with all k-independent operators and quantities
        self.div_x = sample.div_x
        self.div_y = sample.div_y
        self.div_z = sample.div_z
        self.poisson = sample.poisson
        self.boundary_nodes = sample.boundary_nodes
        self.laplace = sample.laplace
        self.grad_x = sample.grad_x
        self.grad_y = sample.grad_y
        self.grad_z = sample.grad_z
        self.Msat = sample._Msat
        self.Msat_avrg = sample.Msat_avrg
        self.k = None
        self.dense = None
        self.ksquared_mat = None
        self.nx = sample.nx
        self.nb = sample.nb
        self.shape = 3 * self.nx, 3 * self.nx
        self.dtype = np.complex128
        self.a_n = sample.dA
        self.dim = sample._dim

        # other stuff for dense matrix computation
        self.xyz = sample.xyz
        self.belm = sample.belm

        self.nv = sample.nv
        self.pang = sample.pang
        self.bndint_order = bndint_order
        self.beta = sample._beta
        dispatcher = [ComputeDenseMatrix_1D,ComputeDenseMatrix_2D,ComputeDenseMatrix_3D]
        self.compute_dense_matrix = dispatcher[self.dim-1]
        self.set_k(0)

    def update(self,sample):
        self.Msat = sample._Msat
        self.Msat_avrg = sample.Msat_avrg
        self.beta = sample._beta
        self.set_k(0)

    def set_k(self, k):
        # should read in/calculate dense matrix and not take it, will be done later
        to_compute = True
        if self.dense is None:
            if self.dim == 3:
                to_compute = False
            else:
                self.dense = np.empty((self.nb * self.nb))
        #print(to_compute)
        self.k = k
        self.ksquared_mat = k ** 2 * csr_matrix(diags(self.a_n))
        diag = self.a_n.copy()
        diag[self.boundary_nodes] = np.zeros(self.nb)
        self.ksquared_matb = k ** 2 * csr_matrix(diags(diag))

        if to_compute:
            self.dense = np.reshape(self.dense, (self.nb * self.nb))
            dense_err = self.compute_dense_matrix(self.dense,
                                                  self.belm,
                                                  self.boundary_nodes,
                                                  self.xyz,
                                                  self.nv,
                                                  self.pang,
                                                  self.bndint_order,
                                                  self.k)
            self.dense = np.reshape(self.dense, (self.nb, self.nb))

    def _matvec(self, x):
        if self.k is None:
            raise ValueError(
                "No wave vector (k) has been assigned to the dynamic matrix yet. "
                "Do this by calling the set_k() method.")
        else:
            return N_dip(x, self.k, self.ksquared_mat, self.ksquared_matb, self.nx,
                         self.div_x, self.div_y, self.div_z, self.poisson, self.boundary_nodes,
                         self.dense, self.laplace, self.grad_x, self.grad_y, self.grad_z, self.a_n, self.dim, self.beta,
                         self.Msat_avrg)
