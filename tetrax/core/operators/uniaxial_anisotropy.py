import logging

from scipy.constants import mu_0
from scipy.sparse import csr_matrix, diags, lil_matrix
from scipy.sparse.linalg import spsolve, LinearOperator, lgmres, bicg

from ...helpers.math import *


def _get_uniaxial_attributes(sample):
    is_AFM = sample._magnetic_order == "AFM"

    Msat = sample.Msat
    xyz_shape = sample.xyz.shape
    Ku1_list = [sample.Ku1] if isinstance(sample.Ku1, (float, int, np.ndarray)) else sample.Ku1

    # e_u hast to be handled separately, because people might also wanna supply [0,0,1] and so on.
    e_u_list = [sample.e_u] if (isinstance(sample.e_u, np.ndarray) or np.shape(sample.e_u) == (3,)) else sample.e_u

    nx = sample.nx
    Ku1_prime_list = None
    if is_AFM:
        Ku1_prime_list = [sample.Ku1_prime] if isinstance(sample.Ku1_prime,
                                                               (float, int, np.ndarray)) else sample.Ku1_prime

    return Msat, xyz_shape, Ku1_list, Ku1_prime_list, e_u_list, nx



class UniAxialAnisotropyOperator(LinearOperator):

    def __init__(self, sample) -> None:

        self.Msat, \
        self.xyz_shape, \
        self.Ku1_list, \
        self.Ku1_prime_list, \
        self.e_u_list, \
        self.nx = _get_uniaxial_attributes(sample)

        if len(self.Ku1_list) != len(self.e_u_list):
            print(
                f"Could not initialize UniAxialAnisotropyOperator. Not the same number of Ku1 ({len(self.Ku1_list)}) and e_u ({len(self.e_u_list)}) specified.")


        else:
            self.sparse_mat = self.make_sparse_mat()
            self.shape = self.sparse_mat.shape
            self.dtype = np.complex128

    def update(self, sample):
        self.Msat, \
        self.xyz_shape, \
        self.Ku1_list, \
        self.Ku1_prime_list, \
        self.e_u_list, \
        self.nx = _get_uniaxial_attributes(sample)

        if len(self.Ku1_list) != len(self.e_u_list):
            print(
                f"Could not initialize UniAxialAnisotropyOperator. Not the same number of Ku1 ({len(self.Ku1_list)}) and e_u ({len(self.e_u_list)}) specified.")


        else:
            self.sparse_mat = self.make_sparse_mat()
            self.shape = self.sparse_mat.shape
            self.dtype = np.complex128



    def make_sparse_mat(self):
        sparse_mat = csr_matrix((3 * self.nx, 3 * self.nx))

        # iterate over all anisotropies.
        for i, Ku1 in enumerate(self.Ku1_list):

            # make a flattened mesh vector Ku1*ones(3*nx) from Ku1 if it is a single number,
            # tile (Ku1, Ku1, Ku1) to mesh vector if inhomogeneous (array).
            Ku1_array = np.tile(Ku1, 3) if isinstance(Ku1, np.ndarray) else Ku1 * np.ones(3 * self.nx)

            e_u = self.e_u_list[i]

            # check if e_u is single vector or inhomogeneous
            if np.shape(e_u) == self.xyz_shape:

                # only normalize and convert to flattened mesh vector
                e_u_normalized = normalize_single_3d_vec(e_u).T.flatten()

            elif np.shape(e_u) == (3,):

                # extent vector to mesh vector, normalize and flatten
                # TODO this could be faster by switching the order of things
                e_u_normalized = normalize_single_3d_vec(np.array([e_u for i in range(self.nx)])).T.flatten()

            sparse_mat += flattened_mesh_vec_tensor_product(-2 * Ku1_array / (mu_0) * e_u_normalized,
                                                            e_u_normalized)

        return sparse_mat

    def _matvec(self, vec):
        return self.sparse_mat.dot(vec) / np.tile(self.Msat ** 2, 3)


class UniAxialAnisotropyOperatorAFM(LinearOperator):

    def __init__(self, sample) -> None:

        self.Msat, \
        self.xyz_shape, \
        self.Ku1_list, \
        self.Ku1_prime_list, \
        self.e_u_list, \
        self.nx = _get_uniaxial_attributes(sample)

        # Validate lengths
        if len(self.Ku1_list) != len(self.e_u_list) or len(self.Ku1_prime_list) != len(self.e_u_list):
            print(
                f"Could not initialize UniAxialAnisotropyOperator. Not the same number of Ku1 ({len(self.Ku1_list)}), "
                f"Ku1_prime ({len(self.Ku1_prime_list)})  and e_u ({len(self.e_u_list)}) specified.")


        else:
            self.sparse_mat = self.make_sparse_mat()
            self.shape = self.sparse_mat.shape
            self.dtype = np.complex128

    def update(self,sample):
        self.Msat, \
        self.xyz_shape, \
        self.Ku1_list, \
        self.Ku1_prime_list, \
        self.e_u_list, \
        self.nx = _get_uniaxial_attributes(sample)

        # Validate lengths
        if len(self.Ku1_list) != len(self.e_u_list) or len(self.Ku1_prime_list) != len(self.e_u_list):
            print(
                f"Could not initialize UniAxialAnisotropyOperator. Not the same number of Ku1 ({len(self.Ku1_list)}), "
                f"Ku1_prime ({len(self.Ku1_prime_list)})  and e_u ({len(self.e_u_list)}) specified.")


        else:
            self.sparse_mat = self.make_sparse_mat()
            self.shape = self.sparse_mat.shape
            self.dtype = np.complex128

    def make_sparse_mat(self):
        sparse_mat = csr_matrix((6 * self.nx, 6 * self.nx))

        # iterate over all anisotropies.
        for i, Ku1 in enumerate(self.Ku1_list):
            Ku1_prime = self.Ku1_prime_list[i]

            # make a flattened mesh vector Ku1*ones(3*nx) from Ku1 if it is a single number,
            # tile (Ku1, Ku1, Ku1) to mesh vector if inhomogeneous (array).
            Ku1_array = np.tile(Ku1, 3) if isinstance(Ku1, np.ndarray) else Ku1 * np.ones(3 * self.nx)
            Ku1_prime_array = np.tile(Ku1_prime, 3) if isinstance(Ku1_prime, np.ndarray) else Ku1_prime * np.ones(
                3 * self.nx)

            e_u = self.e_u_list[i]

            # check if e_u is single vector or inhomogeneous
            if np.shape(e_u) == self.xyz_shape:

                # only normalize and convert to flattened mesh vector
                e_u_normalized = normalize_single_3d_vec(e_u).T.flatten()

            elif np.shape(e_u) == (3,):

                # extent vector to mesh vector, normalize and flatten
                # TODO this could be faster by switching the order of things
                e_u_normalized = normalize_single_3d_vec(np.array([e_u for i in range(self.nx)])).T.flatten()

            block_Ku1 = flattened_mesh_vec_tensor_product(-2 * Ku1_array / (mu_0 * self.Msat ** 2) * e_u_normalized,
                                                          e_u_normalized)
            block_Ku1_prime = flattened_mesh_vec_tensor_product(
                -2 * Ku1_prime_array / (mu_0 * self.Msat ** 2) * e_u_normalized,
                e_u_normalized)
            sparse_mat += bmat([[block_Ku1, block_Ku1_prime], [block_Ku1_prime, block_Ku1]])

        return sparse_mat

    def _matvec(self, vec):
        return self.sparse_mat.dot(vec)
