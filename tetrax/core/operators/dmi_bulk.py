import logging

from scipy.constants import mu_0
from scipy.sparse import csr_matrix, diags, lil_matrix
from scipy.sparse.linalg import spsolve, LinearOperator, lgmres, bicg

from ...helpers.math import *


def _construct_rot_and_sigma_matrices(sample):
    nx = sample.nx
    grad_x = sample.grad_x
    grad_y = sample.grad_y
    grad_z = sample.grad_z

    zeros_block = csr_matrix((nx, nx))

    sigma_mat = bmat([[zeros_block, diags(-1j * np.ones(nx)), zeros_block],
                           [diags(1j * np.ones(nx)), zeros_block, zeros_block],
                           [zeros_block, zeros_block, zeros_block]],
                          format="csr")
    rot = bmat([[None, -grad_z, grad_y],
                [grad_z, None, -grad_x],
                [-grad_y, grad_x, None]],
                format="csr")

    return sigma_mat, rot


class BulkDMIOperator(LinearOperator):

    def __init__(self, sample):
#        self.grad_x = sample.grad_x
#        self.grad_y = sample.grad_y
        self.nx = sample.nx
#        self.Msat = sample.Msat
#        self.fac = 2 * sample.Dbulk / (mu_0 * sample.scale)
#        self.k = 0

        # TODO make this more efficient without dense matrices.
        # construct Sigma matrix (for k-dep part)
        self.Sigma, self.rot = _construct_rot_and_sigma_matrices(sample)

 #       self.sparse_mat = self.fac * (self.rot + self.k * self.Sigma)
 #       self.shape = self.sparse_mat.shape
        self.update(sample)

    def update(self,sample):
#        self.fac = 2 * sample.Dbulk / (sample.Msat ** 2 * mu_0 * sample.scale)
        self.Msat = sample.Msat
        self.fac = 2 * sample.Dbulk / (mu_0 * sample.scale)
        self.k = 0
        self.sparse_mat = self.fac * (self.rot + self.k * self.Sigma)
        self.shape = self.sparse_mat.shape

    def set_k(self, k):
        self.k = k
        self.sparse_mat = self.fac * (self.rot + self.k * self.Sigma)

    def _matvec(self, vec):
#        return self.sparse_mat.dot(x)
        return self.sparse_mat.dot(vec) / np.tile(self.Msat ** 2,3)

class BulkDMIOperatorAFM(LinearOperator):

    def __init__(self, sample):
#        self.grad_x = sample.grad_x
#        self.grad_y = sample.grad_y
        self.nx = sample.nx
#        self.fac = 2 * sample.Dbulk / (sample.Msat ** 2 * mu_0 * sample.scale)
#        self.k = 0

        # TODO make this more efficient without dense matrices.
        # construct Sigma matrix (for k-dep part)

        self.Sigma, self.rot = _construct_rot_and_sigma_matrices(sample)

#        N12 = self.fac * (self.rot + self.k * self.Sigma)

#        self.sparse_mat = bmat([[None, N12],[N12,None]])
#        self.shape = self.sparse_mat.shape
        self.update(sample)

    def update(self, sample):
        self.fac = 2 * sample.Dbulk / (sample.Msat ** 2 * mu_0 * sample.scale)
        self.k = 0
        N12 = self.fac * (self.rot + self.k * self.Sigma)
        self.sparse_mat = bmat([[None, N12],[N12,None]])
        self.shape = self.sparse_mat.shape

    def set_k(self, k):
        self.k = k
        N12 = self.fac * (self.rot + self.k * self.Sigma)
        self.sparse_mat = bmat([[None, N12], [N12, None]])

    def _matvec(self, x):
        return self.sparse_mat.dot(x)
