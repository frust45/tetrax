import logging

from scipy.constants import mu_0
from scipy.sparse import csr_matrix, diags, lil_matrix
from scipy.sparse.linalg import spsolve, LinearOperator, lgmres, bicg

from ...helpers.math import *


class UniformDMIOperatorAFM(LinearOperator):

    def __init__(self, sample) -> None:

        self.Msat = sample.Msat
        self.xyz_shape = sample.xyz.shape
        self.e_u_list = [sample.e_u] if isinstance(sample.e_u, np.ndarray) else sample.e_u
        self.nx = sample.nx
        self.Duni = sample.Duni

        self.sparse_mat = self.make_sparse_mat()
        self.shape = self.sparse_mat.shape
        self.dtype = np.complex128

    def make_sparse_mat(self):
        sparse_mat = csr_matrix((6 * self.nx, 6 * self.nx))

        # iterate over all anisotropies.
        for i, e_u in enumerate(self.e_u_list):

            # check if e_u is single vector or inhomogeneous
            if np.shape(e_u) == self.xyz_shape:

                # only normalize and convert to flattened mesh vector
                e_u_normalized = normalize_single_3d_vec(e_u).T.flatten()

            elif np.shape(e_u) == (3,):

                # extent vector to mesh vector, normalize and flatten
                # TODO this could be faster by switching the order of things
                e_u_normalized = normalize_single_3d_vec(np.array([e_u for i in range(self.nx)])).T.flatten()


            # includes all constants
            d_vector = self.Duni /  (mu_0) * FlattenedMeshVector(e_u_normalized)


            N12 = bmat([[None, diags(-d_vector.z), diags(d_vector.y)],
                        [diags(d_vector.z), None, diags(-d_vector.x)],
                        [diags(-d_vector.y), diags(d_vector.x), None]])

            add_mat = bmat([[None, -N12], [N12, None]])

            sparse_mat += add_mat

        return sparse_mat

    def _matvec(self, vec):
#        return self.sparse_mat.dot(vec)
        return self.sparse_mat.dot(vec) / np.tile(self.Msat ** 2,3)
