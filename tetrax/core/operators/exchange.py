import logging

from scipy.constants import mu_0
from scipy.sparse import csr_matrix, diags, lil_matrix
from scipy.sparse.linalg import spsolve, LinearOperator, lgmres, bicg
from ..fem_core.fempreproc import CalcLaPl, CalcdNa, ExchangeOperator1D
from ..fem_core.cythoncore import ExchangeOperator2D, ExchangeOperator3D
from ...helpers.math import *


class ExchangeOperator(LinearOperator):

    def __init__(self, sample):
        dispatcher=[ExchangeOperator1D,ExchangeOperator2D,ExchangeOperator3D]
        cell_types = ["vertex", "line", "triangle", "tetra"]
        self.cell_types = cell_types[sample._dim]
        self.bnd_cell_types = cell_types[sample._dim-1]
        self.Laplacian=dispatcher[sample._dim-1]
        sparse_mat_k0 = -self.Laplacian(sample.xyz,
                                   sample.mesh.get_cells_type(self.cell_types),
                                   sample.mesh.get_cells_type(self.bnd_cell_types),
                                   sample.Aex,
                                   sample.Msat,
                                   sample.scale)
        self.shape = sparse_mat_k0.shape
        self.nx = sample.nx
        self.beta = np.tile(sample._beta, 3)
        self.sparse_mat_k0 = sparse_mat_k0
        self.dtype = np.complex128
        self.Msat = sample.Msat
        self.Aex = sample.Aex
#        self.lex = np.sqrt(2 * sample.Aex / (mu_0 * sample.Msat ** 2)) / sample.scale
        self.factor = np.sqrt(2 / mu_0 ) / sample.scale
        self.k = 0
        self.set_k(0)
#        self.update(sample)

    def update(self,sample):
        self.Msat = sample.Msat
        if not np.array_equal(self.Aex, sample.Aex):
            self.Aex = sample.Aex
            self.sparse_mat_k0 = -self.Laplacian(sample.xyz,
                                       sample.mesh.get_cells_type(self.cell_types),
                                       sample.mesh.get_cells_type(self.bnd_cell_types),
                                       sample.Aex,
                                       sample.Msat,
                                       sample.scale)
            self.shape = self.sparse_mat_k0.shape
        self.k = 0
        self.set_k(0)

    def set_k(self, k):
        self.k = k
        self.sparse_mat = self.factor ** 2 * self.sparse_mat_k0 + \
                          self.factor ** 2 * self.k ** 2 * \
                          dia_matrix((np.tile(self.Aex, 3), 0), shape=self.shape)

    def _matvec(self, vec):
        return self.sparse_mat.dot(vec) / np.tile(self.Msat ** 2,3)


class ExchangeOperatorAFM(LinearOperator):

    def __init__(self, sample):
        dispatcher=[ExchangeOperator1D,ExchangeOperator2D,ExchangeOperator3D]
        cell_types = ["vertex", "line", "triangle", "tetra"]
        self.cell_types = cell_types[sample._dim]
        self.bnd_cell_types = cell_types[sample._dim-1]
        self.Laplacian=dispatcher[sample._dim-1]
        self.dtype = np.complex128
        self.Aex = sample.Aex
        self.lex = np.sqrt(2 * sample.Aex / (mu_0 * sample.Msat ** 2)) / sample.scale
        self.lex_prime = np.sqrt(2 * sample.Aex_prime / (mu_0 * sample.Msat ** 2)) / sample.scale
        self.Lambda_exc = - sample.Aex_uni / (mu_0 * sample.Msat ** 2)
        self.k = 0
        self.nx = sample.nx
        self.shape = (6 * self.nx, 6 * self.nx)
        self.identity_6nx = dia_matrix(
            (np.ones(6 * self.nx), 0), shape=self.shape)
        self.identity_3nx = dia_matrix(
            (np.ones(3 * self.nx), 0), shape=(3 * self.nx, 3 * self.nx))
        self.pauli_6nx = bmat([[None, self.identity_3nx], [self.identity_3nx, None]])
        self.update(sample)

    def update(self,sample):
        Aex = sample.Aex * np.ones(self.nx)
        Msat = sample.Msat * np.ones(self.nx)
        self.xc = -self.Laplacian(sample.xyz,
                                sample.mesh.get_cells_type(self.cell_types),
                                sample.mesh.get_cells_type(self.bnd_cell_types),
                                Aex,
                                Msat,
                                sample.scale) / sample.Aex
        self.lex = np.sqrt(2 * sample.Aex / (mu_0 * sample.Msat ** 2)) / sample.scale
        self.lex_prime = np.sqrt(2 * sample.Aex_prime / (mu_0 * sample.Msat ** 2)) / sample.scale
        self.Lambda_exc = - sample.Aex_uni / (mu_0 * sample.Msat ** 2)
        self.k = 0
        self.sparse_mat_k0 = bmat([[self.lex ** 2 * self.xc, self.lex_prime ** 2 * self.xc],
                                   [self.lex_prime ** 2 * self.xc, self.lex ** 2 * self.xc]]) + \
                             self.Lambda_exc * self.pauli_6nx
        self.set_k(0)

    def set_k(self, k):
        self.k = k
        self.sparse_mat = self.sparse_mat_k0 + self.k ** 2 * (
                self.lex ** 2 * self.identity_6nx + self.lex_prime ** 2 * self.pauli_6nx)

    def _matvec(self, vec):
        return self.sparse_mat.dot(vec)