import logging

from scipy.constants import mu_0
from scipy.sparse import csr_matrix, diags, lil_matrix
from scipy.sparse.linalg import spsolve, LinearOperator, lgmres, bicg

from ...helpers.math import *


class NotImplementedOperatorFM(LinearOperator):

    def __init__(self, sample):
        self.nx = sample.nx
        self.sparse_mat = csr_matrix((3 * self.nx, 3 * self.nx))
        self.shape = (3 * self.nx, 3 * self.nx)

    def set_k(self, k):
        pass

    def make_sparse_mat_with_current_m0(self):
        pass

    def nonlinear_field(self, x):
        return 0 * x

    def _matvec(self, x):
        return self.sparse_mat.dot(x)


class NotImplementedOperatorAFM(LinearOperator):

    def __init__(self, sample):
        self.nx = sample.nx
        self.sparse_mat = csr_matrix((6 * self.nx, 6 * self.nx))
        self.shape = (6 * self.nx, 6 * self.nx)

    def set_k(self, k):
        pass

    def make_sparse_mat_with_current_m0(self):
        pass

    def nonlinear_field(self, x):
        return 0 * x

    def _matvec(self, x):
        return self.sparse_mat.dot(x)

