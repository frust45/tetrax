from abc import ABC, abstractmethod

import k3d
import meshio
import numpy as np
from scipy.constants import mu_0

from .fem_core.cythoncore import get_matrices2D, get_matrices3D
from .fem_core.fempreproc import get_matrices1D
from .operators import ExchangeOperator, ExchangeOperatorAFM, DipolarOperator, UniAxialAnisotropyOperator, UniAxialAnisotropyOperatorAFM, BulkDMIOperator, \
    InterfacialDMIOperator, CubicAnisotropyLinearOperator, InterlayerExchangeOperator, NotImplementedOperatorFM, NotImplementedOperatorAFM, BulkDMIOperatorAFM, UniformDMIOperatorAFM, InterfacialDMIOperatorAFM
from ..core.mesh import *
from ..experiments.eigen import calculate_normal_modes, not_implemented_eigensolver
from ..helpers.io import check_mesh_shape
from ..helpers.io import get_mesh_dimension, write_field_to_file
from ..helpers.math import normalize_single_3d_vec, sample_average, interpolate_along_curve


qty_color_cycle = [0x0072B2, 0xE34234, 0x009E73, 0xE69F00, 0x56B4E9]

class AbstractSample(ABC):
    """Base class for the concrete sample classes

    - :class:`_ConfinedSampleFM`
    - :class:`_ConfinedSampleAFM`
    - :class:`_WaveguideSampleFM`
    - :class:`_WaveguideSampleAFM`
    - :class:`_LayerSampleFM`
    - :class:`_LayerSampleAFM`

    which are created by multiple-inheritance from this base class together with mixin classes for magnetic order (FM
    or AFM) and geometry type (confined, waveguide, layer). The available attributes for material parameters depend on
    the magnetic order (see User Guide for details).

    Attributes
    ----------
    name : str
        Name of the sample.
    nx : int
        Total number of nodes in the mesh of the sample.
    nb : int
        Number of boundary nodes in the mesh of the sample.
    mesh : meshio.Mesh
        Mesh of the sample.
    xyz : MeshVector
        Coordinates on the mesh of the sample.
    scale : float
        Scale of the sample (default is `1e-9`, such that all spatial dimensions are given in nanometer).


    """

    def __init__(self, name, geometry, magnetic_order, dim):
        self.name = name
        self._dim = dim
        self._geometry = geometry
        self._magnetic_order = magnetic_order
        self._no_dip = False

        # geometric parameters
        self.geom = None
        self.mesh = None
        self.nx = None
        self.nb = None
        self.scale = 1e-9
        self.xyz = None

        self._get_eigensolver()
        self._initialize_material_params()

    @abstractmethod
    def _get_eigensolver(self):
        """This method should be implemented by the FerromagneticMixin or
        AntiferromagneticMixin class of the concrete sample."""

    @abstractmethod
    def _initialize_material_params(self):
        """This method should be implemented by the FerromagneticMixin or
        AntiferromagneticMixin class of the concrete sample."""

    @abstractmethod
    def _get_magnetic_tensors(self):
        """This method should be implemented by the FerromagneticMixin or
        AntiferromagneticMixin class of the concrete sample."""

    @abstractmethod
    def show(self, show_node_labels=False, show_mag=True, comp="vec", scale=5, show_scaled_mag=True, show_grid=False,
             show_extrusion=True):
        """Show the sample. Displays the mesh and, if available, the magnetization(s).

        Parameters
        ----------
        show_node_labels : bool
            If true, shows the label associated to each node.
        show_mag : bool
            Show the magnetization (or magnetizations, in the case of antiferromagnetic samples).
        comp : {"vec", "x", "y", "z"}
           Determines which component will be plotted.
        scale : float
            Determines the scale of the vector glyphs used to visualize the magnetization (default is 5).
        show_scaled_mag : bool
            Scale magnetization with spatially dependent saturation magnetization (default is ``True``).
        show_grid : bool
            Show background grid (default is ``False``).
        show_extrusion : bool
            Show full 3D mesh by extruding 1D or 2D mesh (default is ``True``).

        Returns
        -------
        plot : k3d_plot_object
        """

    def plot(self, fields, comp="vec", scale=5, labels=None):
        """
        Plot a vector or scalar field which is defined on each node of the mesh. Multiple fields can be plotted at the same time.

        Parameters
        ----------
        fields : numpy.array or list(numpy.array)
            Scalar field of shape `(nx,)`, vector field of shape `(nx,3)` or list containing any choice of
            the aforementioned.
        comp : {"vec", "x", "y", "z"}
            If `field` is a vector field, this parameter determines which component will be plotted.
        scale : float
            Determines the scale of the vector glyphs used to visualize a vectorfield (default is 5).
        labels : str or list(str)
            Label(s) of the vectorfield(s) to be plotted. If ``labels`` is a list, it needs to have the same length as
            ``fields`` (default is ``None``).
        """
        cmap = k3d.colormaps.matplotlib_color_maps.Viridis

        if self.mesh == None:
            print("This sample does not have a mesh yet.")
        else:

            # convert input to list if it is not already
            if isinstance(fields, np.ndarray):
                fields = [fields]

            if isinstance(labels, str):
                labels = [labels]

            # do some tests on the input
            if labels is not None:
                if len(labels) != len(fields):
                    raise ValueError(
                        f"You supplied labels for the fields you want to plot, but the number of labels"
                        f" ({len(labels)}) does not match the number of fields ({len(fields)}).")

            # plot the mesh itself
            plot = k3d.plot(lighting=0)
            plot = _add_sample_mesh(plot, self)

            for i, field in enumerate(fields):

                if not isinstance(field, np.ndarray):
                    raise ValueError(
                        f"You are trying to plot something ({field}) which is not a numpy array or a sublcass thereof.")

                if field.shape in [(self.nx, 3), (self.nx,)]:

                    label = labels[i] if labels is not None else None

                    color = qty_color_cycle[i % len(qty_color_cycle)]
                    colors = np.repeat(np.array([(np.uint32(color), np.uint32(color))]), self.nx)

                    if label is not None:
                        print(label)
                        plot += k3d.text2d(label,
                                           position=[i / 2 / len(labels), 0],
                                           color=color, size=1)

                    if field.shape == (self.nx,):
                        # scalar field case
                        plot += k3d.mesh(np.float32(self.mesh.points), np.uint32(self.mesh.get_cells_type("triangle")),
                                         attribute=field,
                                         color_map=cmap, interpolation=False, side="double",
                                         flat_shading=True)

                    elif field.shape == (self.nx, 3):

                        field = MeshVector(field)

                        if comp == "vec":
                            plot += k3d.vectors(np.float32(self.mesh.points),
                                                np.float32(scale * (np.vstack([field.x, field.y, field.z])).T),
                                                head_size=2.5 * scale,
                                                line_width=0.05 * scale, colors=colors)


                        else:
                            comp_dict = {"x": field.x, "y": field.y, "z": field.z, "phi": np.arctan2(field.y, field.z),
                                         "abs": np.sqrt(field.x ** 2 + field.y ** 2 + field.z ** 2)}
                            color_dict = {"abs": k3d.colormaps.matplotlib_color_maps.Inferno,
                                          "x": k3d.colormaps.matplotlib_color_maps.RdBu,
                                          "y": k3d.colormaps.matplotlib_color_maps.RdBu,
                                          "z": k3d.colormaps.matplotlib_color_maps.RdBu,
                                          "phi": k3d.colormaps.matplotlib_color_maps.Twilight_shifted}

                            # TODO check if comp is valid key
                            plot += k3d.mesh(np.float32(self.mesh.points), np.uint32(self.mesh.get_cells_type("triangle")),
                                             attribute=comp_dict[comp],
                                             color_map=color_dict[comp], interpolation=False, side="double",
                                             flat_shading=True)



                else:
                    raise ValueError(
                        f"The vector field you are trying to plot is of shape {field.shape}, but should be either (nx,3) or (nx,).")

        #plot.display()
        return plot

    def set_geom(self, mesh):
        """Set the geometry/mesh of the sample and do neccesary preprocessing for later calculations.

        According to the dimension of the sample, the mesh is checked for compatibility. After sucessefully setting
        the mesh and extrancting necessary information, such as the number of nodes ``nx``, the
        coordinates of the mesh vertices ``xyz`` or the normal vectors ``nv``, the discretized differential operators
        (``poisson``, ``div_x``, ...) are calculated and the magnetic tensors are set up.

        Parameters
        ----------
        mesh : meshio.Mesh
        """
        input_mesh_dim = get_mesh_dimension(mesh)
        if input_mesh_dim != self._dim:
            raise ValueError(f"The dimension of the input mesh ({input_mesh_dim}) does "
                             f"not fit your chosen geometry ({self._geometry}).")

        if not check_mesh_shape(mesh, self._dim):
            raise ValueError(
                f"The input mesh is not properly shaped occording two your chosen geometry "
                f"({self._geometry}). For waveguide samples, all nodes should be in the xy "
                f"plane. For layer samples, all nodes should be on the y axis.")

        print("Setting geometry and calculating discretized differential operators on mesh.")

        dispatcher=[get_matrices1D,get_matrices2D,get_matrices3D]
        cell_types = ["vertex","line","triangle","tetra"]
        get_matrices=dispatcher[self._dim-1]

        self.mesh = mesh
        self.xyz = MeshVector(mesh.points)
        self.nx = len(self.xyz)

        self.Msat = self._Msat
        self.Aex  = self._Aex
        if self._magnetic_order == 'AFM':
            Msat = self.Msat * np.ones(self.nx)
            Aex = self.Aex * np.ones(self.nx)
        else:
            Msat = self.Msat
            Aex = self.Aex

        self.boundary_nodes, \
        self.nv, \
        self.dA, \
        self.pang, \
        self.belm, \
        self.xc, \
        self.poisson, \
        self.laplace, \
        self.div_x, \
        self.div_y, \
        self.div_z, \
        self.grad_x, \
        self.grad_y, \
        self.grad_z = get_matrices(self.xyz,
                                   self.mesh.get_cells_type(cell_types[self._dim]),
                                   self.mesh.get_cells_type(cell_types[self._dim-1]),
                                   Aex,
                                   Msat,
                                   self.scale)
        self.nb = len(self.boundary_nodes)

        # correcting wrong sign from partial integration
        self.xc *= -1
        self.poisson *= -1
        self.laplace *= -1
        self.div_x *= -1
        self.div_y *= -1
        self.div_z *= -1
        self.grad_x *= -1
        self.grad_y *= -1
        self.grad_z *= -1
        self._get_magnetic_tensors()
        print("Done.")

    def read_mesh(self, fname):
        """Read a mesh from a file using `meshio` and set it as the geometry of a sample.

        Parameters
        ----------
        fname : str
            Name of the mesh file.
        """
        try:
            mesh = meshio.read(fname)
            if mesh.points.dtype == ">f8":
                mesh.points = mesh.points.astype('<f8')

        except ReadError:
            print("Could not read this mesh file.")
        self.set_geom(mesh)

    def field_to_file(self, field, fname="field.vtk", qname=None):
        """Calls the :func:`tetrax.helpers.io.write_field_to_file` function to write scalar- or vector field to a file."""
        write_field_to_file(field, self, fname, qname)

    def average(self, field):
        """Calculate the average of a scalar of vector field within the given sample."""

        return sample_average(field, self)

    def scan_along_curve(self, field, curve, num_points=100, return_curve=False):
        """Interpolate a given scalar or vector field along a specified curve.

        Parameters
        ----------
        field : MeshScalar or MeshVector
            Field to be interpolated.
        curve : ndarray, shape (nx, 3) or tuple/list of shape (2, 3)
            The points along the interpolation curve or the start and end points of the curve
            ``((x1, y1, z1),(x2, y2, z2))``.
        num_points : int
            The number of points to interpolate along the curve. Will only be considered if start
            and end points are given (default is 100).
        return_curve : bool
            Next to the interpolated values, also return the interpolation curve.

        Returns
        -------
        interp_values : ndarray, shape (num_points, m)
            The interpolated values along the curve with ``m`` being the size of the second dimension of ``field``.

        Examples
        --------
        - :doc:`Linescan example on the magnetization-graded waveguides </examples/linescans_waveguides>`
        """
        return interpolate_along_curve(self.xyz, field, curve, num_points, return_curve, dim=self._dim)

class _FerromagnetMixin:
    def _initialize_material_params(self):

        # material parameters
        self._Msat = 796e3
        self._Aex = 13e-12
        self.gamma = 1.760859644e11
        self.alpha = 0.01
        self.Didmi = 0.0
        self.Dbulk = 0.0

        self.Ku1 = 0.0
        self.Ku2 = 0.0
        self.k_phi = 0.0
        self.k_theta = 0.0
        self.e_u = np.array([0, 0, 1])  # uniaxial anistropy direction(s)

        self.Kc1 = 0.0
        self.v1Kc = np.array([1, 0, 0])
        self.v2Kc = np.array([0, 1, 0])
        self.v3Kc = np.array([0, 0, 1])

        self.J1 = 0.0

        # mag is hidden
        self._mag = None

        # Operators
        self.N_exc  = None
        self.N_uni  = None
        self.N_dip  = None
        self.N_DMI  = None
        self.N_iDMI = None
        self.N_uDMI = None
        self.N_cub  = None
        self.N_iec  = None
        self.N_uDMI = None

    @property
    def mag(self):
        return self._mag

    @mag.setter
    def mag(self, value):
        if self.mesh == None:
            print("This sample does not have a mesh yet. You cannot set any magnetization for it.")
        else:
            if self.xyz.shape == np.shape(value):
                # TODO make unitary
                self._mag = MeshVector(normalize_single_3d_vec(value))
            elif np.shape(value) == (3,):
                self._mag = MeshVector(normalize_single_3d_vec(np.array([value for i in range(self.nx)])))
            else:
                print("You are trying to set a magnetization field which does not match the shape of the mesh. \n" + \
                      "mesh shape: {}\n".format(self.xyz.shape) + \
                      "value shape: {}\n".format(np.shape(value)))

    @property
    def Msat(self):
        return self._Msat

    @Msat.setter
    def Msat(self, value):
#        if not isinstance(value, (float, int)) and self.mesh is None:
        if self.mesh is None:
            print("This sample does not have a mesh yet. You cannot set spatially dependent saturation for it.")
            self._Msat = value
        elif isinstance(value, (float, int)):
            self._Msat = value * np.ones_like(self.xyz.x)
        else:
            if self.xyz.x.shape == np.shape(value):
                self._Msat = value
            else:
                print("You are trying to set a saturation magnetization which does not match the shape of the mesh. \n" + \
                    "number of nodes: {}\n".format(self.xyz.x.shape) + \
                    "value shape: {}\n".format(np.shape(value)))

    @property
    def Aex(self):
        return self._Aex

    @Aex.setter
    def Aex(self, value):
#        if not isinstance(value, (float, int)) and self.mesh is None:
        if self.mesh is None:
            print("This sample does not have a mesh yet. You cannot set spatially dependent exchange stiffness for it.")
            self._Aex = value
        elif isinstance(value, (float, int)):
            self._Aex = value * np.ones_like(self.xyz.x)
        else:
            if self.xyz.x.shape == np.shape(value):
                self._Aex = value
            else:
                print("You are trying to set an exchange stiffness which does not match the shape of the mesh. \n" + \
                    "number of nodes: {}\n".format(self.xyz.x.shape) + \
                    "value shape: {}\n".format(np.shape(value)))
        if self.N_exc is not None:
            self.N_exc.update(self)

    @property
    def mag_full(self):
        if self.mag is None:
            return None

        return (np.tile(self.Msat,3)*self.mag.to_flattened()).to_unflattened() if not isinstance(self.Msat, (float, int)) else self.Msat*self.mag


    @property
    def Msat_avrg(self):
        if isinstance(self._Msat, (float, int)):
            return self._Msat
        else:
            return self.average(self._Msat)

    @property
    def _beta(self):
        if isinstance(self._Msat, (float, int)):
            return 1
        else:
            return self._Msat/self.Msat_avrg

    @property
    def Bexc(self):
        if self.mesh == None:
            print("This sample does not have a mesh yet. You cannot compute fields!")
            return None
        elif self._mag == None:
            print("This sample does not have a magnetization set yet. You cannot compute fields!")
            return MeshVector(np.zeros((self.nx,3)))
        else:
            return FlattenedMeshVector(-self.N_exc.dot(self._mag.to_flattened())*np.tile(self.Msat * mu_0,3)).to_unflattened()

    @property
    def Bdip(self):
        if self.mesh == None:
            print("This sample does not have a mesh yet. You cannot compute fields!")
            return None
        elif self._mag == None:
            print("This sample does not have a magnetization set yet. You cannot compute fields!")
            return MeshVector(np.zeros((self.nx,3)))
        else:
            return FlattenedMeshVector(-self.N_dip.dot(self._mag.to_flattened())*np.tile(self.Msat_avrg * mu_0,3)).to_unflattened()

    @property
    def Buni(self):
        if self.mesh == None:
            print("This sample does not have a mesh yet. You cannot compute fields!")
            return None
        elif self._mag == None:
            print("This sample does not have a magnetization set yet. You cannot compute fields!")
            return MeshVector(np.zeros((self.nx,3)))
        else:
            return FlattenedMeshVector(-self.N_uni.dot(self._mag.to_flattened())*np.tile(self.Msat * mu_0,3)).to_unflattened()

    @property
    def Bcub(self):
        if self.mesh == None:
            print("This sample does not have a mesh yet. You cannot compute fields!")
            return None
        elif self._mag == None:
            print("This sample does not have a magnetization set yet. You cannot compute fields!")
            return MeshVector(np.zeros((self.nx,3)))
        else:
            return FlattenedMeshVector(-self.N_cub.dot(self._mag.to_flattened())*np.tile(self.Msat * mu_0,3)).to_unflattened()

    @property
    def Bidmi(self):
        if self.mesh == None:
            print("This sample does not have a mesh yet. You cannot compute fields!")
            return None
        elif self._mag == None:
            print("This sample does not have a magnetization set yet. You cannot compute fields!")
            return MeshVector(np.zeros((self.nx,3)))
        else:
            return FlattenedMeshVector(-self.N_iDMI.dot(self._mag.to_flattened())*np.tile(self.Msat * mu_0,3)).to_unflattened()

    @property
    def Bdmi(self):
        if self.mesh == None:
            print("This sample does not have a mesh yet. You cannot compute fields!")
            return None
        elif self._mag == None:
            print("This sample does not have a magnetization set yet. You cannot compute fields!")
            return MeshVector(np.zeros((self.nx,3)))
        else:
            return FlattenedMeshVector(-self.N_DMI.dot(self._mag.to_flattened())*np.tile(self.Msat * mu_0,3)).to_unflattened()


    def _get_magnetic_tensors(self):
        self.N_exc = ExchangeOperator(self)
        if self._dim == 3:
            self.N_dip = NotImplementedOperatorFM(self)
        else:
            self.N_dip = DipolarOperator(self)
        self.N_uni = UniAxialAnisotropyOperator(self)
        self.N_DMI = BulkDMIOperator(self)
        self.N_iDMI = InterfacialDMIOperator(self)
        self.N_uDMI = NotImplementedOperatorFM(self)
        self.N_cub = CubicAnisotropyLinearOperator(self)
        self.N_iec = InterlayerExchangeOperator(self)

    def _update_magnetic_tensors(self):
        self.N_exc.update(self)
        self.N_uni.update(self)
        self.N_dip.update(self)
        self.N_DMI.update(self)
        self.N_iDMI.update(self)
        self.N_uDMI = NotImplementedOperatorFM(self)
        self.N_cub = CubicAnisotropyLinearOperator(self)
        self.N_iec = InterlayerExchangeOperator(self)

    def show(self, show_node_labels=False, comp="vec", scale=5, show_mag=True, show_scaled_mag=True, show_grid=False,
             show_extrusion=True):
        if self.mesh == None:
            print("This sample does not have a mesh yet.")
        else:
            plot = k3d.plot(lighting=0)
            plot = _add_sample_mesh(plot, self)
            plot.grid_visible = show_grid

            if show_node_labels:
                text_obj = k3d.vectors(np.float32(self.mesh.points), np.float32(0 * self.mesh.points),
                                       labels=[str(i) for i in range(len(self.mesh.points))])
                plot += text_obj

            if np.all(self._mag) is not None and show_mag:

                mag_to_plot = self._mag
                if show_scaled_mag and not isinstance(self._Msat, (float, int)):
                    mag_to_plot = self.mag_full / self.Msat_avrg

                if comp == "vec":
                    m_mesh = k3d.vectors(np.float32(self.mesh.points),
                                         np.float32(scale * (np.vstack([mag_to_plot.x, mag_to_plot.y, mag_to_plot.z])).T),
                                         head_size=2.5 * scale,
                                         line_width=0.05 * scale)
                else:
                    comp_dict = {"x": self._mag.x, "y": mag_to_plot.y, "z": mag_to_plot.z,
                                 "phi": np.arctan2(mag_to_plot.y, mag_to_plot.x),
                                 "abs": np.sqrt(mag_to_plot.x ** 2 + mag_to_plot.y ** 2 + mag_to_plot.z ** 2)}
                    color_dict = {"abs": k3d.colormaps.matplotlib_color_maps.Inferno,
                                  "x": k3d.colormaps.matplotlib_color_maps.RdBu,
                                  "y": k3d.colormaps.matplotlib_color_maps.RdBu,
                                  "z": k3d.colormaps.matplotlib_color_maps.RdBu,
                                  "phi": k3d.colormaps.matplotlib_color_maps.Twilight_shifted}

                    # TODO check if comp is valid key
                    m_mesh = k3d.mesh(np.float32(self.mesh.points), np.uint32(self.mesh.get_cells_type("triangle")), attribute=comp_dict[comp],
                                      color_map=color_dict[comp], interpolation=False, side="double", flat_shading=True,
                                      name="$m_{{}}$".format(comp))
                plot += m_mesh

            if show_extrusion:
                _add_extrusion_mesh(plot, self)

            return plot

    def _repr_html_(self):
        return f"""<p><h3>Sample: {self.name}</h3></p>
               <p><table align="left">
              <tr>
                <th>Material parameter</th>
                <th>Attribute name</th>
                <th>(Average) value</th>
              </tr>
              <tr>
                <td>Saturation magnetization </td><td><tt>Msat</tt></td>
                <td>{self.average(self.Msat)} A/m</td>
              </tr>
              <tr>
                <td>Exchange stiffness constant  </td><td><tt>Aex</tt></td>
                <td>{self.average(self.Aex)} J/m</td>
              </tr>
              <tr>
                <td>Gyromagnetic ratio  </td><td><tt>gamma</tt></td>
                <td>{self.gamma} rad Hz/T</td>
              </tr>
              <tr>
                <td>Gilbert damping parameter  </td><td><tt>alpha</tt></td>
                <td>{self.alpha}</td>
              </tr>
              <tr>
                <td>Unaxial anistropy constant  </td><td><tt>Ku1</tt></td>
                <td>{self.Ku1} J/m<sup>3</sup></td>
              </tr>
              <tr>
                <td>Unaxial anistropy direction  </td><td><tt>e_u</tt></td>
                <td>{self.e_u!r}</td>
              </tr>
              <tr>
                <td>Cubic anistropy constant</td><td><tt>Kc1</tt></td>
                <td>{self.Kc1} J/m<sup>3</sup></td>
              </tr>
                <tr>
                    <td>Cubic anistropy axes</td><td><tt>v1Kc</tt><br><tt>v2Kc</tt></td>
                    <td>({self.v1Kc[0]}, {self.v1Kc[1]}, {self.v1Kc[2]})<br>({self.v2Kc[0]}, {self.v2Kc[1]}, {self.v2Kc[2]})</td>
                  </tr>
              <tr>
                <td>Bulk DMI constant  </td><td><tt>Dbulk</tt></td>
                <td>{self.Dbulk} J/m<sup>2</sup></td>
              </tr>
             <tr>
                <td>Interfacial DMI constant  </td><td><tt>Didmi</tt></td>
                <td>{self.Didmi} J/m<sup>2</sup></td>
              </tr>
              <tr>
                <td>Interlayer bilinear exchange constant  </td><td><tt>J1</tt></td>
                <td>{self.J1} J/m<sup>2</sup></td>
              </tr>
              <tr>
                 <td>&nbsp;   </td><td></td>
                <td>&nbsp;  </td>
              </tr>
              <tr>
                <th>Mesh parameter  </th><th>Attribute name</th>
                <th>Value</th>
              </tr>
              <tr>
                <td>Mesh scaling</td><td><tt>scale</tt></td>
                <td>{self.scale}</td>
              </tr>
               <tr>
                <td>Number of nodes</td><td><tt>nx</tt></td>
                <td>{self.nx}</td>
              </tr>
               <tr>
                <td>Number of boundary nodes</td><td><tt>nb</tt></td>
                <td>{self.nb}</td>
              </tr>

            </table>
            </p>"""


class _AntiferromagnetMixin:
    def _initialize_material_params(self):
        self.params = ["a", "couple", "of", "antiferromagnetic", "parameters"]

        self._mag1 = None
        self._mag2 = None

        self._Aex = 13e-12
        self.Aex_prime = 0.0
        self.Aex_uni = -1e8
        self._Msat = 769e3
        self.gamma = 1.760859644e11
        self.alpha = 0.01

        self.Didmi = 0.0
        self.Dbulk = 0.0
        self.Duni = 0.0  # constant of uniform DMI (can only be present if Ku1_prime is non-zero

        self.Ku1 = 0.0
        self.Ku1_prime = 0.0
        self.e_u = np.array([0, 0, 1])  # uniaxial anistropy direction(s)


    @property
    def mag1(self):
        return self._mag1

    @mag1.setter
    def mag1(self, value):
        if self.mesh == None:
            print("This sample does not have a mesh yet. You cannot set any magnetization for it.")
        else:
            if self.xyz.shape == np.shape(value):
                # TODO make unitary
                self._mag1 = MeshVector(normalize_single_3d_vec(value))
            elif np.shape(value) == (3,):
                self._mag1 = MeshVector(normalize_single_3d_vec(np.array([value for i in range(self.nx)])))
            else:
                print("You are trying to set a magnetization field which does not match the shape of the mesh. \n" + \
                      "mesh shape: {}\n".format(self.xyz.shape) + \
                      "value shape: {}\n".format(np.shape(value)))

    @property
    def mag2(self):
        return self._mag2

    @mag2.setter
    def mag2(self, value):
        if self.mesh == None:
            print("This sample does not have a mesh yet. You cannot set any magnetization for it.")
        else:
            if self.xyz.shape == np.shape(value):
                # TODO make unitary
                self._mag2 = MeshVector(normalize_single_3d_vec(value))
            elif np.shape(value) == (3,):
                self._mag2 = MeshVector(normalize_single_3d_vec(np.array([value for i in range(self.nx)])))
            else:
                print("You are trying to set a magnetization field which does not match the shape of the mesh. \n" + \
                      "mesh shape: {}\n".format(self.xyz.shape) + \
                      "value shape: {}\n".format(np.shape(value)))

    @property
    def Msat(self):
        return self._Msat

    @Msat.setter
    def Msat(self, value):
        if not isinstance(value, (float, int)): # and self.mesh is None:
                print("You cannot set spatially dependent saturation for AFM samples.")
        else:
            self._Msat = value

    @property
    def Msat_avrg(self):
        if isinstance(self._Msat, (float, int)):
            return self._Msat
        else:
            return self.average(self._Msat)

    @property
    def Aex(self):
        return self._Aex

    @Aex.setter
    def Aex(self, value):
        if not isinstance(value, (float, int)): # and self.mesh is None:
                print("You cannot set spatially dependent exchange stiffness for AFM samples.")
        else:
            self._Aex = value

    def _get_magnetic_tensors(self):
#        self.N_exc = NotImplementedOperatorAFM(self)
#        self.N_uni = NotImplementedOperatorAFM(self)
        self.N_dip = NotImplementedOperatorAFM(self)
        self.N_DMI = NotImplementedOperatorAFM(self)
        self.N_iDMI = NotImplementedOperatorAFM(self)
        self.N_uDMI = NotImplementedOperatorAFM(self)
        self.N_cub = NotImplementedOperatorAFM(self)
        self.N_iec = NotImplementedOperatorAFM(self)

        self.N_exc = ExchangeOperatorAFM(self)
        self.N_uni = UniAxialAnisotropyOperatorAFM(self)
#        # self.N_exc = NotImplementedOperatorAFM(self)
#        self.N_dip = NotImplementedOperatorAFM(self)
#        self.N_DMI = BulkDMIOperatorAFM(self)
#        self.N_iDMI = InterfacialDMIOperatorAFM(self)
#        self.N_uDMI = UniformDMIOperatorAFM(self)
#        self.N_cub = NotImplementedOperatorAFM(self)
#        self.N_iec = NotImplementedOperatorAFM(self)

    def _update_magnetic_tensors(self):
        self.N_exc.update(self)
        self.N_uni.update(self)

    def show(self, show_node_labels=False, comp="vec", scale=5, show_mag=True):
        if self.mesh == None:
            print("This sample does not have a mesh yet.")
        else:
            plot = k3d.plot(lighting=0)
            plt_mesh = k3d.mesh(np.float32(self.mesh.points), np.uint32(self.mesh.get_cells_type("triangle")))
            plt_mesh.wireframe = True
            plt_mesh.color = 0xaaaaaa
            plt_mesh.opacity = 1
            plot += plt_mesh

            if show_node_labels:
                text_obj = k3d.vectors(np.float32(self.mesh.points), 0 * np.float32(self.mesh.points),
                                       labels=[str(i) for i in range(len(self.mesh.points))])
                plot += text_obj

            if np.all(self._mag1) is not None and show_mag:

                if comp == "vec":
                    m1_mesh = k3d.vectors(np.float32(self.mesh.points),
                                          np.float32(scale * (np.vstack([self._mag1.x, self._mag1.y, self._mag1.z])).T),
                                          head_size=2.5 * scale,
                                          line_width=0.05 * scale)
                else:
                    comp_dict = {"x": self._mag1.x, "y": self._mag1.y, "z": self._mag1.z,
                                 "phi": np.arctan2(self._mag1.y, self._mag1.x),
                                 "abs": np.sqrt(self._mag1.x ** 2 + self._mag1.y ** 2 + self._mag1.z ** 2)}
                    color_dict = {"abs": k3d.colormaps.matplotlib_color_maps.Inferno,
                                  "x": k3d.colormaps.matplotlib_color_maps.RdBu,
                                  "y": k3d.colormaps.matplotlib_color_maps.RdBu,
                                  "z": k3d.colormaps.matplotlib_color_maps.RdBu,
                                  "phi": k3d.colormaps.matplotlib_color_maps.Twilight_shifted}

                    # TODO check if comp is valid key
                    m1_mesh = k3d.mesh(self.mesh.points, self.mesh.get_cells_type("triangle"),
                                       attribute=comp_dict[comp],
                                       color_map=color_dict[comp], interpolation=False, side="double",
                                       flat_shading=True,
                                       name="$m_{{}}$".format(comp))
                plot += m1_mesh

            if np.all(self._mag2) is not None and show_mag:
                mag2_color = 0xFF0000
                colors = np.repeat(np.array([(np.uint32(mag2_color), np.uint32(mag2_color))]), self.nx)
                if comp == "vec":
                    m2_mesh = k3d.vectors(np.float32(self.mesh.points),
                                          np.float32(scale * (np.vstack([self._mag2.x, self._mag2.y, self._mag2.z])).T),
                                          head_size=2.5 * scale,
                                          line_width=0.05 * scale, colors=colors)
                else:
                    comp_dict = {"x": self._mag2.x, "y": self._mag2.y, "z": self._mag2.z,
                                 "phi": np.arctan2(self._mag2.y, self._mag2.x),
                                 "abs": np.sqrt(self._mag2.x ** 2 + self._mag2.y ** 2 + self._mag2.z ** 2)}
                    color_dict = {"abs": k3d.colormaps.matplotlib_color_maps.Inferno,
                                  "x": k3d.colormaps.matplotlib_color_maps.RdBu,
                                  "y": k3d.colormaps.matplotlib_color_maps.RdBu,
                                  "z": k3d.colormaps.matplotlib_color_maps.RdBu,
                                  "phi": k3d.colormaps.matplotlib_color_maps.Twilight_shifted}

                    # TODO check if comp is valid key
                    m2_mesh = k3d.mesh(self.mesh.points, self.mesh.get_cells_type("triangle"),
                                       attribute=comp_dict[comp],
                                       color_map=color_dict[comp], interpolation=False, side="double",
                                       flat_shading=True,
                                       name="$m_{{}}$".format(comp))
                plot += m2_mesh
            plot.display()


class _PropagatingWaveEigensolverMixin:
    def _get_eigensolver(self):
        self.eigensolver = calculate_normal_modes


class _ConfinedWaveEigensolverMixin:
    def _get_eigensolver(self):
        self.eigensolver = calculate_normal_modes


class _ConfinedSampleFM(_ConfinedWaveEigensolverMixin, _FerromagnetMixin, AbstractSample):
    def description(self):
        print("I am a ferromagnetic confined sample.")


class _ConfinedSampleAFM(_ConfinedWaveEigensolverMixin, _AntiferromagnetMixin, AbstractSample):
    def description(self):
        print("I am a antiferromagnetic confined sample.")


class _WaveguideSampleFM(_PropagatingWaveEigensolverMixin, _FerromagnetMixin, AbstractSample):
    def description(self):
        print("I am a ferromagnetic waveguide sample.")


class _WaveguideSampleAFM(_PropagatingWaveEigensolverMixin, _AntiferromagnetMixin, AbstractSample):
    def description(self):
        print("I am a antiferromagnetic waveguide sample.")


class _LayerSampleFM(_PropagatingWaveEigensolverMixin, _FerromagnetMixin, AbstractSample):
    def description(self):
        print("I am a ferromagnetic layer sample.")


class _LayerSampleAFM(_PropagatingWaveEigensolverMixin, _AntiferromagnetMixin, AbstractSample):
    def description(self):
        print("I am a antiferromagnetic layer sample.")


def create_sample(name="my_sample", geometry="waveguide", magnetic_order="FM"):
    """Create a sample with certain geometry and magnetic order.

    Parameters
    ----------
    geometry : str, {"confined", "waveguide", "layer"}
        Geometry of the sample (default is `"waveguide"`).
    magnetic_order : str {"FM", "AFM"}, optional
        Magnetic order of the material. Can be either ferromagnetic (`"FM"`)
        or antiferromagnetic (`"AFM"`). The default is `"FM"`.


    Returns
    -------
    sample : AbstractSample

    """

    ALLOWED_GEOMETRIES = {"confined", "waveguide", "layer"}
    ALLOWED_MAGNETIC_ORDERS = {"FM", "AFM"}

    # Which sample type to choose depending on magnetic_order and geometry.

    SAMPLE_DICT = {("confined", "FM"): _ConfinedSampleFM(name, geometry, magnetic_order, 3),
                   ("confined", "AFM"): _ConfinedSampleAFM(name, geometry, magnetic_order, 3),
                   ("waveguide", "FM"): _WaveguideSampleFM(name, geometry, magnetic_order, 2),
                   ("waveguide", "AFM"): _WaveguideSampleAFM(name, geometry, magnetic_order, 2),
                   ("layer", "FM"): _LayerSampleFM(name, geometry, magnetic_order, 1),
                   ("layer", "AFM"): _LayerSampleAFM(name, geometry, magnetic_order, 1),
                   }

    # check if supplied parameters are valid.

    if geometry not in ALLOWED_GEOMETRIES:
        raise ValueError(f'Invalid geometry value "{geometry}"; should be "confined", '
                         '"waveguide" or "layer".')

    if magnetic_order not in ALLOWED_MAGNETIC_ORDERS:
        raise ValueError(f'Invalid magnetic_order value "{magnetic_order}"; should be "FM", or "AFM".')

    return SAMPLE_DICT[(geometry, magnetic_order)]


def _add_sample_mesh(plot, sample):
    """Private function to append sample mesh (connections and points for vertices) to k3d plot object."""

    cell_type = "line" if sample._geometry == "layer" else "triangle"
    plt_mesh = k3d.mesh(np.float32(sample.mesh.points), np.uint32(sample.mesh.get_cells_type(cell_type)))
    plt_mesh.wireframe = True
    plt_mesh.color = 0xaaaaaa
    plt_mesh.opacity = 1
    plot += k3d.points(positions=np.float32(sample.mesh.points),
                       point_size=0.2,
                       shader='3d',
                       color=0xaaaaaa)

    plot += plt_mesh

    return plot


def _add_extrusion_mesh(plot, sample):
    if sample._dim == 2:
        boundary_nodes = sample.boundary_nodes.tolist()
        center_points = sample.xyz[sample.boundary_nodes].copy()

        front_points = center_points.copy()
        front_points.z += 100
        connections = sample.belm.reshape((sample.nb, 2))  # sample.mesh.get_cells_type("line")

        boundary_points = k3d.mesh(np.float32(center_points), np.uint32(connections))
        front_points = k3d.mesh(np.float32(front_points), np.uint32(connections))

        # for connection in connections:
        #    pass#print(connection)

        # i = 0

        y_diff_max = np.abs(np.min(sample.xyz.y) - np.max(sample.xyz.y))
        x_diff_max = np.abs(np.min(sample.xyz.x) - np.max(sample.xyz.x))

        z_span = 3 * np.max([y_diff_max, x_diff_max])
        # print(z_span)

        number_of_facets = 3

        center_points_shifted1 = center_points.copy()
        center_points_shifted2 = center_points.copy()

        center_points_shifted1.z -= z_span / 2

        center_points_shifted2.z += z_span / 2
        all_points = np.concatenate((center_points_shifted1, center_points_shifted2))

        connections = sample.belm.reshape((sample.nb, 2))
        connections_mantel = []

        for i in range(sample.nb):
            i1 = connections[i][0]
            i2 = connections[i][1]

            j1 = boundary_nodes.index(i1)
            j2 = boundary_nodes.index(i2)

            new_connectiona = [j1, j2, j1 + sample.nb]
            new_connectionb = [j2, j2 + sample.nb, j1 + sample.nb]

            connections_mantel.append(new_connectiona)
            connections_mantel.append(new_connectionb)

        # print(i1,i2,j1,j2)

        # print(connections_mantel)

        new_faceta = k3d.mesh(np.float32(all_points), np.uint32(connections_mantel), side="both", color=0x0002, name="3D waveguide")

        new_faceta.opacity = 0.05
        plot += new_faceta
    if sample._dim == 1:
        boundary_nodes = sample.boundary_nodes.tolist()
        boundary_coords = np.sort(sample.xyz.y[boundary_nodes])

        number_of_layers = sample.nb // 2

        y_diff_max = np.abs(np.min(sample.xyz.y) - np.max(sample.xyz.y))
        x_diff_max = np.abs(np.min(sample.xyz.x) - np.max(sample.xyz.x))

        z_span = 3 * np.max([y_diff_max, x_diff_max])

        for i in range(number_of_layers):
            y1 = boundary_coords[2 * i]
            y2 = boundary_coords[2 * i + 1]

            plot += k3d.line([[0, y1, 0], [0, y2, 0]])

            pointa_minus = [-z_span / 2, y1, z_span / 2]
            pointb_minus = [z_span / 2, y1, z_span / 2]
            pointc_minus = [z_span / 2, y1, -z_span / 2]
            pointd_minus = [-z_span / 2, y1, -z_span / 2]

            # plot += k3d.points(points)

            pointa_plus = [-z_span / 2, y2, z_span / 2]
            pointb_plus = [z_span / 2, y2, z_span / 2]
            pointc_plus = [z_span / 2, y2, -z_span / 2]
            pointd_plus = [-z_span / 2, y2, -z_span / 2]

            points = [pointa_minus, pointb_minus, pointc_minus, pointd_minus, pointa_plus, pointb_plus,
                      pointc_plus, pointd_plus]

            connections = [[0, 1, 2], [2, 3, 0], [4, 5, 6], [6, 7, 4],
                           [0, 1, 4], [1, 4, 5], [2, 3, 6], [3, 6, 7],
                           [5, 6, 1], [6, 1, 2], [0, 4, 7], [0, 3, 7]]

            layer_mesh = k3d.mesh(np.float32(points), np.uint32(connections), color=0x0002, side="both",
                                  flat_shading=False, name="3D layer")
            layer_mesh.opacity = 0.05
            plot += layer_mesh

            plot += k3d.points(np.float32(sample.xyz))

    return plot