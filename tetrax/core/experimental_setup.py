import glob
import os
import time

import k3d
import numpy as np
import pygmsh

from ..core.mesh import *
from ..core.sample import _add_sample_mesh, _add_extrusion_mesh
from ..experiments.calculate_absorption import calculate_absorption
from ..experiments.relax import minimize_gibbs_free_energy
from ..helpers.io import read_mode_from_vtk, add_to_bib
from ..helpers.math import h_hom, h_CPW, h_strip

from ..experiments.calculate_absorption import calculate_linewidhts

B_ext_color = 0xED4B42


class ExperimentalSetup:
    """Experimental setup built around a ``sample`` in which external parameters are defined and numerical experiments
    can be conducted.

    Attributes
    ----------
    sample : AbstractSample
        Sample object on which numerical experiments are conducted.
    name : str
        Name of the experimental setup, used to store results (default is ``"my_experiment"``).
    Bext : MeshVector
        Vector field of the static external magnetic field (given in T). Can also be set as a triplet, `e.g.` as
        ``[0, 0, 0.5]`` for a homogeneous field of 500 mT in :math:`z` direction.
    antenna : _AbstractMicrowaveAntenna
        Microwave antenna in the experimental setup (see User Guide for details).
    """

    def __init__(self, sample, name="my_experiment"):
        self.sample = sample
        self._Bext = FlattenedMeshVector(np.zeros(3 * sample.nx)).to_unflattened()
        self.name = name
        self._antenna = None
        self._modes_available = False
        self.dispersion = None

    @property
    def Bext(self):
        return self._Bext

    @Bext.setter
    def Bext(self, value):
        if self.sample.mesh == None:
            print("This sample does not have a mesh yet. You cannot set any external field for it.")
        else:
            if np.array_equal(self.sample.xyz.shape, np.shape(value)):
                self._Bext = MeshVector(value)
            elif np.shape(value) == (3,):
                self._Bext = MeshVector([value for i in range(self.sample.nx)])
            else:
                print("You are trying to set an external field which does not match the shape of the mesh.")

    @property
    def antenna(self):
        return self.antenna

    @antenna.setter
    def antenna(self, value):
        if isinstance(value, _AbstractMicrowaveAntenna):
            self._antenna = value
        else:
            raise ValueError(f'Input {value} is not of type "MicrowaveAntenna"')

    def show(self, comp="vec", scale=5, show_antenna=True, show_grid=False, show_extrusion=True):
        """Shows the experimental setup, including external fields and microwave antennna.

        Parameters
        ----------
        comp : {"vec", "x", "y", "z"}
           If external field is nonzero, determines which component will be plotted.
        scale : float
            Determines the scale of the vector glyphs used to visualize the magnetization (default is 5).
        show_antenna : bool
            If available, show the microwave antenna (default is ``True``).
        show_grid : bool
            Show grid lines in plot (default is ``False``).
        show_extrusion : bool
            Show full 3D mesh by extruding 1D or 2D mesh (default is ``True``).
        """
        plot = k3d.plot()
        plot.grid_visible = show_grid
        plt_mesh = k3d.mesh(np.float32(self.sample.mesh.points), np.uint32(self.sample.mesh.get_cells_type("triangle")))
        plt_mesh.wireframe = True

        plt_mesh.color = 0xaaaaaa
        plt_mesh.opacity = 1
        plot += plt_mesh

        if show_extrusion:
            _add_extrusion_mesh(plot, self.sample)

        if np.any(self._Bext) != 0:

            h_abs = np.array(np.sqrt(self._Bext.x ** 2 + self._Bext.y ** 2 + self._Bext.z ** 2))
            colors = np.repeat(np.array([(np.uint32(B_ext_color), np.uint32(B_ext_color))]), self.sample.nx)
            if comp == "vec":
                m_mesh = k3d.vectors(np.float32(self.sample.mesh.points),
                                     np.float32(scale * (np.vstack([self._Bext.x, self._Bext.y, self._Bext.z])).T),
                                     head_size=2.5 * scale * np.mean(h_abs),
                                     line_width=0.05 * scale * np.mean(h_abs), colors=colors)
            else:
                comp_dict = {"x": self._Bext.x, "y": self._Bext.y, "z": self._Bext.z,
                             "phi": np.arctan2(self._Bext.y, self._Bext.x),
                             "abs": h_abs}
                color_dict = {"abs": k3d.colormaps.matplotlib_color_maps.Inferno,
                              "x": k3d.colormaps.matplotlib_color_maps.RdBu,
                              "y": k3d.colormaps.matplotlib_color_maps.RdBu,
                              "z": k3d.colormaps.matplotlib_color_maps.RdBu,
                              "phi": k3d.colormaps.matplotlib_color_maps.Twilight_shifted}

                # TODO check if comp is valid key
                m_mesh = k3d.mesh(np.float32(self.sample.mesh.points),
                                  np.uint32(self.sample.mesh.get_cells_type("triangle")), attribute=comp_dict[comp],
                                  color_map=color_dict[comp], interpolation=False, side="double", flat_shading=True,
                                  name="$m_{{}}$".format(comp))
            plot += m_mesh

        else:
            anchor = 1.5 * np.max(self.sample.xyz)
            plt_text = k3d.text2d('B_{ext} = 0',
                                  position=[0, 0],
                                  color=B_ext_color, size=1)
            plot += plt_text

        if self._antenna is not None and show_antenna:
            span = np.max(np.abs(self.sample.xyz.T[0])) * 2
            antenna_mesh = self._antenna.get_mesh(span)
            plot += k3d.mesh(np.float32(antenna_mesh.points), np.uint32(antenna_mesh.get_cells_type("triangle")),
                             color=0xFFD700, side="double")

        #plot.display()
        return plot

    def relax(self, tol=1e-12, continue_with_least_squares=False, return_last=False, save_mag=True, fname="mag.vtk",
              verbose=True):
        """Relax the magnetization ``mag`` of a sample in the experimental setup by minimizing the total magnetic energy.

        Parameters
        ----------
        tol : float, optional
            Tolerance at which the minimization is considered successful (default is ``1e-12``).
        continue_with_least_squares : bool
            If minimization with conjugate-gradient method was not successful, continue with least-squares method
            (default is ``False``). This is more stable, but slower.
        return_last : bool
            If minimization was not successful, set the last iteration step as the sample magnetization (default is ``False``).
        save_mag : bool
            Save the resulting magnetization as `vtk` file to the folder of the experimental setup (default is ``True``).
        fname : str
            Filename for magnetization file (default is ``mag.vtk``).
        verbose : bool
            Output progress of the calculation (default is ``True``).

        Returns
        -------
        success : bool
            If the relaxation was successful of not.

        Notes
        -----
        For waveguide samples, the equilibration is not totally stable yet. Please use with care and check the resulting
        equilibrium states.

        Examples
        --------
        - :doc:`Spin-wave dispersion in transversally-magnetized rectangular waveguide </examples/rectangular_waveguide_DE>`
        - :doc:`Hysteresis loop calculation </examples/hysteresis_loop>`
        - :doc:`Spatially dependent uniaxial anisotropy </examples/round_tube_spatial_dep_anis>`
        """

        add_to_bib(self.sample, "tetrax")
        
        is_AFM = self.sample._magnetic_order == "AFM"
        if not is_AFM and self.sample.mag is None:
            print("Your sample does not have a magnetization field yet.")
            return False
        elif is_AFM and (self.sample.mag1 is None or self.sample.mag2 is None):
            print("Your AFM sample does not have both magnetization fields yet.")
            return False

        else:
#            self.sample._get_magnetic_tensors()
            self.sample._update_magnetic_tensors()
            result, success = minimize_gibbs_free_energy(self.sample, self._Bext, tol_cg=tol,
                                                         return_last=return_last,
                                                         continue_with_least_squares=continue_with_least_squares,
                                                         verbose=verbose)
            if not is_AFM:
                self.sample.mag = result.to_unflattened()
            if is_AFM:
                self.sample.mag1, self.sample.mag2 = result.to_two_unflattened()
            if save_mag:
                experiment_path = f"./{self.sample.name}/{self.name}"
                if not os.path.exists(experiment_path):
                    os.makedirs(experiment_path)
                fname = f"{experiment_path}/{fname}"

                # depending on whether FM or AFM, save one or two vector fields.
                qty_to_save = self.sample.mag if not is_AFM else [self.sample.mag1, self.sample.mag2]
                names_to_save = "mag" if not is_AFM else ["mag1", "mag2"]
                self.sample.field_to_file(qty_to_save, fname=fname, qname=names_to_save)

            return success

    def eigenmodes(self, kmin=-40e6, kmax=40e6, Nk=81, num_modes=20, k=None, no_dip=False, h0_dip=False, num_cpus=1,
                   save_modes=True,
                   save_local=False, save_magphase=False, linewidths=False, perturbed_dispersion=False, save_stiffness_fields=False,
                   verbose=True, save_disp=True, fname=None):
        r"""Calculate the spin-wave eigenmodes and their frequencies/dispersion of the sample in its current state
        using a dynamic matrix approach.

        The eigensystem is calculuated by numerically solving the linearized equation of motion of magnetizion (see
        User Guide for details). For waveguides or layer samples, a finite-element propagating-wave dynamic-matrix
        approach is used [1]_. The dispersion is returned as a ``pandas.DataFrame``. Additionally, it is saved as
        ``dispersion.csv`` (or ``dispersion_perturbedmodes.csv``, if ``perturbed_dispersion=True``) in the folder
        of the experimental setup.

        Parameters
        ----------
        kmin : float
            Minimum wave vector in rad/m (default is -40e6). This argument is ignored when calculating the eigensystem for
            confined samples.
        kmax : float
            Maximum wave vector in rad/m (default is 40e6). This argument is ignored when calculating the eigensystem for
            confined samples.
        Nk : int
            Number of wave-vector values, for which the mode frequencies are calculated. This argument is ignored when calculating the eigensystem for
            confined samples (default is 81).
        num_modes : int
            Determines how many modes (for confined samples) or branches of the dispersion (for waveguides and layers
            samples) are calculated in total (default is 20).
        k : float or array_like
            By supplying an ``array_like`` of wave vectors arbitrary k vector resolution is possible for particular or interesting parts of the dispersion.
            If set to a single ``float`` value, the eigensystem is only calculated for a single wave vector (default is ``None``).
            This argument is ignored when calculating the eigensystem for confined samples.
        no_dip : bool
            Exclude dipolar interaction from calculations (default is ``False``).
        h0_dip : bool
            If ``no_dip`` is ``True``, still retain dipolar contributions in the equilibrium effective field.
        num_cpus : int
            Number of CPU cores to be used in parallel for calculations (default is 1). If set to ``-1``, all available
            cores are used.
        save_modes : bool
            Saves real and imaginary part of the mode profiles in cartesian basis as `vtk` files (default is ``True``).
            The mode profiles are saved in a ``/mode-profiles`` subdirectory in the folder of the experimental setup.
        save_local : bool
            If ``save_modes`` is also ``True``, save the components of the mode profiles in the basis locally orthogonal
            to the equilibrum magnitization (default is ``False``). The local components are saved in the same ``vtk``
            file as cartesian components.
        save_magphase : bool
            If ``save_modes`` is also ``True``, save magnetide and phase of the mode-profile components (default is ``False``).
            Saved in the same
        linewidths : bool
            Calculates the linewidths :math:`\Gamma=\alpha\epsilon\omega` of the modes (with precession ellipticities
            :math:`\epsilon`) and adds :math:`\Gamma/2\pi` in GHz to the dataframe (default is ``False``).
        perturbed_dispersion : bool
            After calculating the eigenmodes/normalmodes (possibly with ``no_dip=True``), calculate the zeroth-order
            pertubation of the dispersion including dynamic dipolar fields according to Ref [2]_. Gives approximation
            for dispersion without dipolar hybridization (default is ``False``). The results are appended to the returned
            ``dispersion`` dataframe.
        save_stiffness_fields : bool
            If `perturbed_dispersion` is also ``True``, append the unitless individual stiffness fields (components) within the
            perturbed dispersion to the ``dispersion`` dataframe (default is ``False``). Allows to numerically reverse
            engineer general spin-wave dispersions [2]_.
        verbose : bool
            Output progress of the calculation (default is ``True``).
        save_disp : bool
            Save the computed dispersion to a `csv` file in directory of the experimental setup (default is ``True``).
        fname : str
            Filename for the dispersion file (default is ``"dispersion.csv"`` or ``dispersion_perturbedmodes``, depending on ``perturbed_dispersion``).

        Returns
        -------
        dispersion : pandas.DataFrame
            Dataframe (table) containing the calculated dispersion.

        Notes
        -----
        By default, the ``dispersion`` DataFrame is structured as

        =========== ========= ========= =========
        k (rad/m)   f0 (GHz)  f1 (GHz)  ...
        =========== ========= ========= =========
        -40e6       1.426     2.352     ...
        ...         ...       ...       ...
        =========== ========= ========= =========

        In case ``linewidths=True``, the linewidths are added in units of GHz as

        =========== ========= ================= =========
        k (rad/m)   f0 (GHz)  Gamma0/2pi (GHz)  ...
        =========== ========= ================= =========
        -40e6       1.426     0.122             ...
        ...         ...       ...               ...
        =========== ========= ================= =========

        In case ``perturbed_dispersion=True``, additional columns are added as

        =========== ========= =============== =========
        k (rad/m)   f0 (GHz)  f_pert_0 (GHz)  ...
        =========== ========= =============== =========
        -40e6       1.426     1. 484          ...
        ...         ...       ...             ...
        =========== ========= =============== =========

        Additionally, if ``save_stiffnessfields=True``, the unitless stiffness fields per magnetic interaction are also appended

        =========== ========= =============== ============= =========
        k (rad/m)   f0 (GHz)  f_pert_0 (GHz)  Re(N21_exc)_0 ...
        =========== ========= =============== ============= =========
        -40e6       1.426     1. 484          0.431         ...
        ...         ...       ...             ...           ...
        =========== ========= =============== ============= =========

        Indivual columns/rows can be obtained in the usual way for `pandas.DataFrame``, for example, with

        >>> k = dispersion["k (rad/m)"]
        >>> first_row = dispersion.iloc[0]


        References
        ----------
        .. [1]  Körber, *et al.*, "Finite-element dynamic-matrix approach for
                spin-wave dispersions in magnonic waveguides with arbitrary
                cross section", `AIP Advances 11, 095006 (2021) <https://doi.org/10.1063/5.0054169>`_
        .. [2]  Körber and Kákay, "Numerical reverse engineering of general spin-wave dispersions: Bridge between
                numerics and analytics using a dynamic-matrix approach", `Phys. Rev. B 104,
                174414 (2021) <https://doi.org/10.1103/PhysRevB.104.174414>`_

        Examples
        --------
         - :doc:`Spin-wave dispersion in thin vortex-state nanotube </examples/round_tube_dispersion_vortex>`
         - :doc:`Spin-wave dispersion in transversally-magnetized rectangular waveguide </examples/rectangular_waveguide_DE>`

        """

        is_AFM = self.sample._magnetic_order == "AFM"
        if not is_AFM and self.sample.mag is None:
            print("Your sample does not have a magnetization field yet.")
            return False
        elif is_AFM and (self.sample.mag1 is None or self.sample.mag2 is None):
            print("Your AFM sample does not have both magnetization fields yet.")
            return False

        else:
#            self.sample._get_magnetic_tensors()
            self.sample._update_magnetic_tensors()
            dispersion = self.sample.eigensolver(self.sample, self.Bext, self.name, kmin, kmax, Nk, num_modes, k,
                                                 no_dip, h0_dip, num_cpus, save_modes, save_local, save_magphase,
                                                 perturbed_dispersion, save_stiffness_fields, verbose, linewidths)
            self._modes_available = save_modes
            self.dispersion = dispersion
            if save_disp:
                if fname is None:
                    fname = "dispersion.csv" if not perturbed_dispersion else "dispersion_perturbedmodes.csv"
                experiment_path = f"./{self.sample.name}/{self.name}"
                if not os.path.exists(experiment_path):
                    os.makedirs(experiment_path)
                fname = f"{experiment_path}/{fname}"
                self.dispersion.to_csv(fname)

            add_to_bib(self.sample, f"eigenmodes{self.sample._dim}d")

            if perturbed_dispersion:
                add_to_bib(self.sample, f"perturbation{self.sample._dim}d")

            if linewidths:
                add_to_bib(self.sample, "linewidth")

            return dispersion

    def linewidths(self, auto_eigenmodes=False, k=None, verbose=True, **kwargs):
        r"""Calculates the linewidths :math:`\Gamma=\alpha\epsilon\omega` of spin-wave modes (with precession
        ellipticities :math:`\epsilon`).

        Parameters
        ----------
        auto_eigenmodes : bool
            If no modes have been previously calculated, calculates them automically (default is ``False``).
        k : float
            Wave number at which the linewidths should be calculated. If `None`, the full wave-vector range available
            from the dispersion will be used (default is ``None``).
        verbose : bool
            Output progress of the calculation (default is ``True``).
        kwargs
            Optional keyword arguments to be passed to the eigensolver (e.g. ``num_cpus=-1``).

        Returns
        -------
        linewidths : pandas.DataFrame
            Dataframe (table) containing the calculated linewidths (and the dispersion).

        Notes
        -----
        The line widths are returned together with the oscillation frequencies in units of GHz.

        =========== ========= ================= =========
        k (rad/m)   f0 (GHz)  Gamma0/2pi (GHz)  ...
        =========== ========= ================= =========
        -40e6       1.426     0.122             ...
        ...         ...       ...               ...
        =========== ========= ================= =========

        References
        ----------
        .. [1] Verba, *et al.*, "Damping of linear spin-wave modes in magnetic nanostructures: Local, nonlocal,
               and coordinate-dependent damping", `Phys. Rev. B 98, 104408 (2018) <https://doi.org/10.1103/PhysRevB.98.104408>`_

        .. [2] Körber, *et al.*, "Symmetry and curvature effects on spin waves in vortex-state hexagonal nanotubes",
               `Phys. Rev. B 104, 184429 (2021) <https://doi.org/10.1103/PhysRevB.104.184429>`_

        Examples
        --------
        - :doc:`Dispersion and linewidths in thick film </examples/thick_film_dispersion_linewidth>`
        """

        if self.sample.mag is None:
            print("Your sample does not have a magnetization field yet.")
        else:
            if self._modes_available is False and auto_eigenmodes is False:
                print("You did not calculate any eigenmodes to calculate an absorption from. "
                      "Please run .eigenmodes(..., save_modes=True) in this ExperimentalSetup "
                      "first or set auto_eigenmodes=True when calling linewidths().")

            if self._modes_available is False and auto_eigenmodes is True:
                if verbose:
                    print("Calculating eigenmodes with linewidths.")
                self.dispersion = self.eigenmodes(save_modes=True, k=k, linewidths=True, **kwargs)

            if self._modes_available is True and auto_eigenmodes is False:
                if verbose:
                    print("Calculating linewidths from existing mode profiles.")

                power_map = calculate_linewidhts(self.sample,self.name,self.dispersion,k)

                # TODO make bib entry
                #add_to_bib(self.sample, f"linewidths")

                if verbose:
                    print("Done.")
                return power_map

            if self._modes_available is True and auto_eigenmodes is True:
                print("Modes are already calculated. Use auto_eigenmodes=False instead.")

            add_to_bib(self.sample, "linewidth")
            if verbose:
                print("Done.")

            return self.dispersion


    def absorption(self, auto_eigenmodes=False, fmin=0, fmax=30, Nf=200, k=None, verbose=True):
        r"""Obtain the microwave absorption of the calculated eigensystem with respect to the microwave ``antenna`` in the
        experimental setup.

        Parameters
        ----------
        auto_eigenmodes : bool
            If no eigenmodes are available in this experimental setup, calculate eigenmodes with default settings.
        fmin : float
            Minimum microwave frequency in GHz for which the absorption line is calculated (default is 0).
        fmax : float
            Maximum microwave frequency in GHz for which the absorption line is calculated (default is 30).
        Nf : int
            Number of microwave frequencies for which the absorption is calculated (default is 200).
        k : float
            If set to a value, the absorption line is calculated only for a single wave vector (default is ``None``).
            Also, the full dynamic susceptibility is saved to a dataframe. This argument is ignored in confined samples.
        verbose : bool
            Output progress of the calculation (default is ``True``).

        Returns
        -------
        susceptibility : pandas.DataFrame
            Only if ``k`` is not ``None`` or sample is a confined sample. Contains frequency-dependent real and
            imaginary part as well as magnitude as phase and magnitude of the averaged dynamic susceptiblity.
        (absorbed_power, wave_vectors, frequencies) : (numpy.ndarray, numpy.ndarray, numpy.ndarray)
            If ``k`` is ``None`` and sample is a waveguide or a layer sample. Saves the wave-vector and frequency-
            dependent microwave-power absorption (``absorbed_power``) together with the corresponding wave vectors
            and frequencies as a 2D arrays.

        Notes
        -----
        The return of a wave-vector-dependent absorption calculation can be plotted using `matplotlib` as

        >>> absorbed_power, wave_vectors, frequencies = exp.absorption(...)
        >>> import matplotlib.pyplot as plt
        >>> plt.pcolormesh(wave_vectors, frequencies, absorbed_power)

        References
        ----------
        .. [1] Verba, *et al.*, "Damping of linear spin-wave modes in magnetic nanostructures: Local, nonlocal,
               and coordinate-dependent damping", `Phys. Rev. B 98, 104408 (2018) <https://doi.org/10.1103/PhysRevB.98.104408>`_

        .. [2] Körber, *et al.*, "Symmetry and curvature effects on spin waves in vortex-state hexagonal nanotubes",
               `Phys. Rev. B 104, 184429 (2021) <https://doi.org/10.1103/PhysRevB.104.184429>`_

        Examples
        --------
        - :doc:`FMR in a rectangular waveguide </examples/FMR_rect_waveguide>`
        - :doc:`Absorption of a nanotube with antennae </examples/round_tube_absorption>`

        """
        if self.sample.mag is None:
            print("Your sample does not have a magnetization field yet.")
        else:
            if self._modes_available is False and auto_eigenmodes is False:
                print("You did not calculate any eigenmodes to calculate an absorption from. "
                      "Please run .eigenmodes(..., save_modes=True) in this ExperimentalSetup "
                      "first or set auto_eigenmodes=True when calling absorption().")

            if self._modes_available is False and auto_eigenmodes is True:
                if verbose:
                    print("Calculating eigenmodes first.")
                self.dispersion = self.eigenmodes(save_modes=True, k=k)
                if verbose:
                    print("Calculating absorption.")

                power_map = calculate_absorption(self.sample, self.name, self.dispersion, self._antenna, fmin, fmax, Nf,
                                                 k)
                add_to_bib(self.sample, f"absorption")

                if verbose:
                    print("Done.")
                return power_map
            if self._modes_available is True and auto_eigenmodes is True:
                print("Modes are already calculated. Use auto_eigenmodes=False instead.")

            if self._modes_available is True and auto_eigenmodes is False:
                if verbose:
                    print("Calculating absorption.")
                power_map = calculate_absorption(self.sample, self.name, self.dispersion, self._antenna, fmin, fmax, Nf,
                                                 k)
                add_to_bib(self.sample, f"absorption{self.sample._dim}d")

                if verbose:
                    print("Done.")
                return power_map

    def show_mode(self, k=0, N=0, scale=1, on_equilibrium=True, animated=False, periods=1, frames_per_period=20, fps=12,
                  scale_mode=1):
        r"""If spatial mode profiles are available, show a desired mode.

        The mode is visualized as vector field which can either only show the dynamic component or the full magnetization
        (mode on-top of equilibrium). Setting ``animated=True`` allows to animate the mode profiles by showing the oscillation
        over a desired number of periods (default is 1). The mode is automatically colored according to its magnitude.

        .. warning::
            At the moment, animation is implemented such that it will play until the end and cannot be stopped (except
            by interrupting the kernel). So be careful with setting a large number of oscillation periods.

        Parameters
        ----------
        k : float, optional
            Wave vector of desired mode in rad/µm (default is 0). This parameter will be ignored for confined samples.
        N : int, optional
            Mode/branch index of mode to be shown (default is 0).
        scale : float, optional
            Scaling of the mode (default is 1).
        on_equilibrium : bool, optional
            If true, show the mode on top of the equilibrium magnetization (default is True).
        animated : bool, optional
            Make a mode movie by showing the magnetic oscillation over one period in a loop.
        periods : int, optional
            Number of oscillation periods to animate (default is 1). Will be ignored if ``animated=False``.
        frames_per_period : int, optional
            Number of frames to animate per period (default is 20). Will be ignored if ``animated=False``.
        fps : int, optional
            Frames per second for animation (default is 12).
        """
        is_AFM = self.sample._magnetic_order == "AFM"

        if not self._modes_available:
            print("There are no mode profiles available which could be plotted.")
            return None

        modes_path = f"{self.sample.name}/{self.name}/mode-profiles"

        modes = glob.glob(f"{modes_path}/mode_*.vtk")

        # this is a set, only unique values allowed!
        k_ = []
        for mode in modes:
            k_i = mode.split("mode_k")[1].split("rad")[0]
            k_.append(float(k_i))

        # k_ = set(k_)
        k_ = np.sort(np.unique(np.array(k_)))
        closest_k = k_[np.abs(k_ - k).argmin()]
        if closest_k != k:
            print(f"Clostest k-value found: {closest_k} rad/µm")

        try:
            if not is_AFM:
                m = read_mode_from_vtk(f"{modes_path}/mode_k{closest_k}radperum_{str(N).zfill(3)}.vtk", magnetic_order="FM")
                magnitude = np.sqrt(
                    m.real.x ** 2 + m.real.y ** 2 + m.real.z ** 2 + m.imag.x ** 2 + m.imag.y ** 2 + m.imag.z ** 2)
                colors = k3d.helpers.map_colors(magnitude, k3d.matplotlib_color_maps.Inferno,
                                                color_range=[0, 1.2 * np.max(magnitude)])  # , color_range=[0.5, 0.6])
                colors = [[c, c] for c in colors]
            else:
                m1, m2 = read_mode_from_vtk(f"{modes_path}/mode_k{closest_k}radperum_{str(N).zfill(3)}.vtk", magnetic_order="AFM")
                magnitude1 = np.sqrt(
                    m1.real.x ** 2 + m1.real.y ** 2 + m1.real.z ** 2 + m1.imag.x ** 2 + m1.imag.y ** 2 + m1.imag.z ** 2)
                magnitude2 = np.sqrt(
                    m2.real.x ** 2 + m2.real.y ** 2 + m2.real.z ** 2 + m2.imag.x ** 2 + m2.imag.y ** 2 + m2.imag.z ** 2)

                colors1 = k3d.helpers.map_colors(magnitude1, k3d.matplotlib_color_maps.Blues,
                                                color_range=[0, 1* np.max(magnitude1)])  # , color_range=[0.5, 0.6])
                colors1 = [[c, c] for c in colors1]

                colors2 = k3d.helpers.map_colors(magnitude1, k3d.matplotlib_color_maps.Reds,
                                                 color_range=[0, 1* np.max(magnitude2)])  # , color_range=[0.5, 0.6])
                colors2 = [[c, c] for c in colors2]
        except:
            print(f"Could not find mode for k = {k} rad/µm, N = {N}")
            return None


        plot = k3d.plot(lighting=0)
        plot = _add_sample_mesh(plot, self.sample)

        if not is_AFM:
            field_to_plot = scale_mode * m.real
            field_to_plot = field_to_plot + self.sample.mag if on_equilibrium else field_to_plot
            field = MeshVector(field_to_plot)
            plt_vectors = k3d.vectors(np.float32(self.sample.mesh.points),
                                      np.float32(scale * (np.vstack([field.x, field.y, field.z])).T),
                                      head_size=2.5 * scale,
                                      line_width=0.05 * scale,
                                      colors=colors)
            plot += plt_vectors

        else:
            field_to_plot1 = scale_mode * m1.real
            field_to_plot1 = field_to_plot1 + self.sample.mag1 if on_equilibrium else field_to_plot1
            field = MeshVector(field_to_plot1)
            plt_vectors1 = k3d.vectors(np.float32(self.sample.mesh.points),
                                      np.float32(scale * (np.vstack([field.x, field.y, field.z])).T),
                                      head_size=2.5 * scale,
                                      line_width=0.05 * scale,
                                      colors=colors1)

            field_to_plot2 = scale_mode * m2.real
            field_to_plot2 = field_to_plot2 + self.sample.mag2 if on_equilibrium else field_to_plot2
            field = MeshVector(field_to_plot2)
            plt_vectors2 = k3d.vectors(np.float32(self.sample.mesh.points),
                                       np.float32(scale * (np.vstack([field.x, field.y, field.z])).T),
                                       head_size=2.5 * scale,
                                       line_width=0.05 * scale,
                                       colors=colors2)
            plot += plt_vectors1
            plot += plt_vectors2


        if not animated:
            plot.display()

        if animated:
            # plot the mesh itself

            plot.display()

            def field_at_time(t):
                field_to_plot = scale_mode * (m.real * np.cos(t) - m.imag * np.sin(t))
                field_to_plot = field_to_plot + self.sample.mag if on_equilibrium else field_to_plot
                field = MeshVector(field_to_plot)
                return np.float32(scale * (np.vstack([field.x, field.y, field.z])).T)

            def field_at_time1(t):
                field_to_plot = scale_mode * (m1.real * np.cos(t) - m1.imag * np.sin(t))
                field_to_plot = field_to_plot + self.sample.mag1 if on_equilibrium else field_to_plot
                field = MeshVector(field_to_plot)
                return np.float32(scale * (np.vstack([field.x, field.y, field.z])).T)

            def field_at_time2(t):
                field_to_plot = scale_mode * (m2.real * np.cos(t) - m2.imag * np.sin(t))
                field_to_plot = field_to_plot + self.sample.mag2 if on_equilibrium else field_to_plot
                field = MeshVector(field_to_plot)
                return np.float32(scale * (np.vstack([field.x, field.y, field.z])).T)

            t_ = np.linspace(0, 2 * np.pi * periods, frames_per_period * periods)
            if not is_AFM:
                for t in t_:
                    plt_vectors.vectors = field_at_time(t)
                    time.sleep(1 / fps)
            if is_AFM:
                for t in t_:
                    plt_vectors1.vectors = field_at_time1(t)
                    plt_vectors2.vectors = field_at_time2(t)
                    time.sleep(1 / fps)
            # plot.start_auto_play()


def create_experimental_setup(sample, name: str = "my_experiment") -> ExperimentalSetup:
    """Creates an experimental setup around a given sample.

    Parameters
    ----------
    sample : AbstractSample
        Sample object to create ExperimentalSetup around.

    name : str
        Name of the experimental setup.

    Returns
    -------
    ExperimentalSetup
    """

    return ExperimentalSetup(sample, name)


class _AbstractMicrowaveAntenna:

    def __init__(self):
        self.type = None


class StriplineAntenna(_AbstractMicrowaveAntenna):
    """Stripline antenna. The antenna is assumed to be infinitely thin.

    .. image:: /usage/exp_antenna_stripline.png
       :width: 150

    Attributes
    ----------
    width : float
        Width of the current lines (in propagation direction).
    gap : float
        Gap (not pitch) between the current lines.
    spacing : float
        Distance between coordiante origin and antenna center plane.

    """

    def __init__(self, width, spacing):
        self.type = "stripline"
        self.width = width
        self.spacing = spacing
        self.k_spectrum = h_strip

    def get_mesh(self, span):
        x0 = [-span / 2, self.spacing - 2.5, -self.width / 2]
        with pygmsh.occ.Geometry() as geom:
            geom.add_box(x0, [span, 5, self.width], self.width)
            mesh = geom.generate_mesh()
        return mesh

    def get_microwave_field_function(self, xyz):
        x_ = xyz.T[0]
        antenna_func = lambda k: h_strip(k, 1, self.width, np.abs(x_ - self.spacing))
        antenna_field_func = lambda k: np.array([np.zeros_like(x_), antenna_func(k), antenna_func(k)]).flatten()
        return antenna_field_func


class CPWAntenna(_AbstractMicrowaveAntenna):
    """Coplanar-waveguide antenna. The antenna is assumed to be infinitely thin.

    .. image:: /usage/exp_antenna_cpw.png
       :width: 300

    Attributes
    ----------
    width : float
        Width of the current lines (in propagation direction).
    gap : float
        Gap (not pitch) between the current lines.
    spacing : float
        Distance between coordiante origin and antenna center plane.

    """

    def __init__(self, width, gap, spacing):
        self.type = "stripline"
        self.width = width
        self.gap = gap
        self.spacing = spacing
        self.k_spectrum = h_CPW

    def get_mesh(self, span):
        with pygmsh.occ.Geometry() as geom:
            geom.add_box([-span / 2, self.spacing - 2.5, -self.width * 1.5 - self.gap],
                         [span, 5, self.width],
                         self.width)
            geom.add_box([-span / 2, self.spacing - 2.5, -self.width / 2],
                         [span, 5, self.width],
                         self.width)
            geom.add_box([-span / 2, self.spacing - 2.5, self.width / 2 + self.gap],
                         [span, 5, self.width],
                         self.width)
            mesh = geom.generate_mesh()
        return mesh

    def get_microwave_field_function(self, xyz):
        x_ = xyz.T[0]
        antenna_func = lambda k: h_CPW(k, 1, self.width, self.gap, np.abs(x_ - self.spacing))
        antenna_field_func = lambda k: np.array([np.zeros_like(x_), antenna_func(k), antenna_func(k)]).flatten()
        return antenna_field_func


class HomogeneousAntenna(_AbstractMicrowaveAntenna):
    """Antenna which produces a homogeneous microwave field.

    Attributes
    ----------
    theta : float
        Polar angle of the field polarization (given in degrees).
    phi : float
        Azimuthal angle of the field polarization (given in degrees).

    """

    def __init__(self, theta=0, phi=0):
        self.type = "homogeneous"
        self.theta = theta / 180 * np.pi
        self.phi = phi / 180 * np.pi

    def get_mesh(self):
        with pygmsh.occ.Geometry() as geom:
            # geom.add_box(x0, [span, 5, self.width], self.width)
            mesh = geom.generate_mesh()
        return mesh

    def get_microwave_field_function(self, xyz):
        x_ = xyz.T[0]
        antenna_func = lambda k: h_hom(k, np.ones_like(x_))
        antenna_field_func = lambda k: np.array([np.sin(self.theta) * np.cos(self.phi) * antenna_func(k),
                                                 np.sin(self.theta) * np.sin(self.phi) * antenna_func(k),
                                                 np.cos(self.theta) * antenna_func(k)]).flatten()
        return antenna_field_func


class CurrentLoopAntenna(_AbstractMicrowaveAntenna):
    """Antenna with the shape of a current loop.

    .. image:: /usage/exp_antenna_loop.png
       :width: 180

    The current loop is oriented in the :math:`xy` plane. For calculations, the current loop is taken to be infinitely
    thin in radial direction.

    Attributes
    ----------
    width : float
        Width of the antenna (in propagation direction).
    radius : float
        Radius of the antenna.

    """

    def __init__(self, width, radius):
        self.type = "currentloop"
        self.width = width
        self.radius = radius

    def get_mesh(self, span):
        with pygmsh.occ.Geometry() as geom:
            geom.characteristic_length_min = self.radius * np.pi / 30
            geom.characteristic_length_max = self.radius * np.pi / 20
            cylinder1 = geom.add_cylinder([0, 0, -self.width / 2], [0, 0, self.width], self.radius + 2.5, 2 * np.pi)
            cylinder2 = geom.add_cylinder([0, 0, -self.width / 2], [0, 0, self.width], self.radius - 2.5, 2 * np.pi)
            geom.boolean_difference(cylinder1, cylinder2)
            mesh = geom.generate_mesh()
        return mesh

    def get_microwave_field_function(self, xyz):
        x_ = xyz.T[0]
        y_ = xyz.T[1]
        rho_ = np.sqrt(x_ ** 2 + y_ ** 2)
        phi_ = np.arctan2(y_, x_)
        antenna_func = lambda k: h_strip(k, 1, self.width, np.abs(rho_ - self.radius))

        # antenna_field_func = lambda k: np.array([np.zeros_like(x_), np.zeros_like(x_), antenna_func(k)]).flatten()

        antenna_field_func = lambda k: np.array(
            [antenna_func(k) * np.cos(phi_), antenna_func(k) * np.sin(phi_), antenna_func(k)]).flatten()
        return antenna_field_func
