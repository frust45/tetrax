"""
This core submodule provides several subclasses of `np.ndarray` to define different scalar and vector
fields on meshes. The import convention

    >>> import numpy as np

is used.
"""

import numpy as np

__all__ = ["MeshScalar",
           "MeshVector",
           "FlattenedMeshVector",
           "FlattenedAFMMeshVector",
           "LocalMeshVector",
           "FlattenedLocalMeshVector",
           "FlattenedLocalAFMMeshVector",
           "FlattenedAFMMeshVectorSpherical",
           "FlattenedMeshVectorSpherical",
           "make_flattened_AFM",
           "flattened_spherical_to_cartesian_fm",
           "flattened_spherical_to_cartesian_afm",
           ]


class MeshScalar(np.ndarray):
    """Class for scalar fields defined on a mesh.

    Input `array_like` needs to be one dimensional. The number of nodes `nx` is
    inferred from the length of the input array.

    Attributes
    ----------
    nx : int
        Number of nodes.
    """

    def __new__(cls, input_array):

        obj = np.asarray(input_array).view(cls)
        if len(obj.shape) == 1:
            obj.nx = obj.shape[0]
            return obj
        else:
            raise ValueError(f"Input {input_array} is not 1-dimensional.")


class MeshVector(np.ndarray):
    r"""Class for three-component vector fields of shape `(nx, 3)` defined on a mesh.

    The entries of the vector are ordered according to

    .. math::

      \mathbf{a} = \begin{pmatrix}
                a_{x_1} & a_{y_1} & a_{z_1} \\
                & \vdots & \\
                a_{x_N} & a_{y_N} & a_{z_N} \\
            \end{pmatrix}

    while :math:`N` = `nx`. The input `array_like` needs to be two dimensional with `shape[1] = 3` (array of triplets).
    The number of nodes `nx` is inferred from `shape[0]` of the input array.

    Attributes
    ----------
    nx : int
        Number of nodes.
    x : MeshScalar
        The :math:`x` component of the vector field at each mesh node.
    y : MeshScalar
        The :math:`y` component of the vector field at each mesh node.
    z : MeshScalar
        The :math:`z` component of the vector field at each mesh node.

    Methods
    -------
    to_flattened()
        Returns the vector field as :py:class:`FlattenedMeshVector`.

    See Also
    --------
    FlattenedMeshVector, LocalMeshVector, FlattenedLocalMeshVector
    """

    def __new__(cls, input_array):
        obj = np.asarray(input_array).view(cls)
        if len(obj.shape) == 2:
            if obj.shape[1] != 3:
                raise ValueError(f"Input {input_array} malshaped. Vector dimension is {obj.shape[1]} and should be 3.")
            obj.nx = obj.shape[0]
            return obj
        else:
            raise ValueError(f"Input {input_array} is not 2-dimensional.")

    def __array_finalize__(self, obj):
        if obj is None: return
        self.nx = getattr(obj, 'nx', None)

    def to_flattened(self):
        return FlattenedMeshVector(self.T.flatten())

    @property
    def x(self):
        return MeshScalar(self.T[0])

    @x.setter
    def x(self, value):
        self[:, 0] = value

    @property
    def y(self):
        return MeshScalar(self.T[1])

    @y.setter
    def y(self, value):
        self[:, 1] = value

    @property
    def z(self):
        return MeshScalar(self.T[2])

    @z.setter
    def z(self, value):
        self[:, 2] = value


class FlattenedMeshVector(np.ndarray):
    """Class for three-component vector fields of shape `(3*nx,)` defined on a mesh.

    The entries of the vector are ordered according to

    .. math:: \mathbf{a} = (a_{x_1}, ... , a_{x_N}, a_{y_1}, ... , a_{y_N}, a_{z_1}, ... , a_{z_N})

    while :math:`N` = `nx`. The input `array_like` needs to be one dimensional and cannot be a
    :py:class:`FlattenedLocalMeshVector`. The number of nodes `nx` is inferred from the length of the input array.

    Attributes
    ----------
    nx : int
        Number of nodes.
    x : MeshScalar
        The :math:`x` component of the vector field at each mesh node.
    y : MeshScalar
        The :math:`y` component of the vector field at each mesh node.
    z : MeshScalar
        The :math:`z` component of the vector field at each mesh node.

    Methods
    -------
    to_ùnflattened()
        Returns the vector field as an unflattened :py:class:`MeshVector`.

    to_spherical()
        Converts the vector to spherical coordinates (only angles), returning a :py:class:`FlattenedMeshVectorSpherical`.

    See Also
    --------
    MeshVector, LocalMeshVector, FlattenedMeshVectorSpherical
    """
    __array_priority__ = 15

    def __new__(cls, input_array):
        obj = np.asarray(input_array).view(cls)
        if len(obj.shape) == 1 and not isinstance(input_array, FlattenedLocalMeshVector):
            obj.nx = obj.shape[0] // 3
            return obj

        elif len(obj.shape) == 1 and isinstance(input_array, FlattenedLocalMeshVector):
            raise ValueError(f"Input {input_array} is a flattened local mesh vector. "
                             "These can only be converted to flattened mesh vectors "
                             "using the inverse of a projection operator.")
        else:
            raise ValueError(f"Input {input_array} is not 1-dimensional.")

    def __array_finalize__(self, obj):
        if obj is None: return
        self.nx = getattr(obj, 'nx', None)

    def to_unflattened(self):
        return MeshVector(self.reshape(3, self.nx).T)

    def to_spherical(self):
        theta = np.arccos(self.z)
        phi = np.arctan2(self.y, self.x)
        return FlattenedMeshVectorSpherical(np.concatenate((theta, phi)))

    @property
    def x(self):
        return MeshScalar(self[:self.nx])

    @x.setter
    def x(self, value):
        self[:self.nx] = value

    @property
    def y(self):
        return MeshScalar(self[self.nx:-self.nx])

    @y.setter
    def y(self, value):
        self[self.nx:-self.nx] = value

    @property
    def z(self):
        return MeshScalar(self[-self.nx:])

    @z.setter
    def z(self, value):
        self[-self.nx:] = value


class FlattenedAFMMeshVector(np.ndarray):
    r"""Class of shape for antiferromagnets `(6*nx,)` to hold the flattened vector fields of each sublattice
    defined on the same mesh.

    The entries of the vector are ordered according to

    .. math:: \mathbf{a} = (a_{x_1}, ... , a_{x_N}, a_{y_1}, ... , a_{y_N}, a_{z_1}, ... , a_{z_N}, b_{x_1}, ... , b_{x_N}, b_{y_1}, ... , b_{y_N}, b_{z_1}, ... , b_{z_N})


    while :math:`N` = `nx` and :math:`\mathbf{a}`, :math:`\mathbf{b}` are the vector fields of the respective sublattices.
    The input `array_like` needs to be one dimensional. The number of nodes `nx` is inferred from the input array.

    Attributes
    ----------
    nx : int
        Number of nodes.
    x1 : MeshScalar
        The :math:`x` component of the first sublattice vector field at each mesh node.
    y1 : MeshScalar
        The :math:`y` component of the first sublattice vector field at each mesh node.
    z1 : MeshScalar
        The :math:`z` component of the first sublattice vector field at each mesh node.
    x2 : MeshScalar
        The :math:`x` component of the second sublattice vector field at each mesh node.
    y2 : MeshScalar
        The :math:`y` component of the second sublattice vector field at each mesh node.
    z2 : MeshScalar
        The :math:`z` component of the second sublattice vector field at each mesh node.

    Methods
    -------
    to_two_unflattened()
        Returns the indivudal vector fields of the sublattices as two separate :py:class:`MeshVector`s.

    to_spherical()
        Converts the vectors on each sublattice to spherical coordinates (only angles), returning
        a :py:class:`FlattenedAFMMeshVectorSpherical`.

    See Also
    --------
    make_flattened_AFM, FlattenedAFMMeshVectorSpherical
    """
    __array_priority__ = 15

    def __new__(cls, input_array):
        obj = np.asarray(input_array).view(cls)
        if len(obj.shape) == 1 and not isinstance(input_array, FlattenedLocalMeshVector):
            obj.nx = obj.shape[0] // 6
            return obj

        elif len(obj.shape) == 1 and isinstance(input_array, FlattenedLocalMeshVector):
            raise ValueError(f"Input {input_array} is a flattened local mesh vector. "
                             "These can only be converted to flattened mesh vectors "
                             "using the inverse of a projection operator.")
        else:
            raise ValueError(f"Input {input_array} is not 1-dimensional.")

    def __array_finalize__(self, obj):
        if obj is None: return
        self.nx = getattr(obj, 'nx', None)

    def to_two_unflattened(self):
        mag1 = FlattenedMeshVector(self[:3 * self.nx]).to_unflattened()
        mag2 = FlattenedMeshVector(self[3 * self.nx:]).to_unflattened()
        return mag1, mag2

    def to_spherical(self):
        theta1 = np.arccos(self.z1)
        phi1 = np.arctan2(self.y1, self.x1)
        theta2 = np.arccos(self.z2)
        phi2 = np.arctan2(self.y2, self.x2)
        return FlattenedAFMMeshVectorSpherical(np.concatenate((theta1, phi1, theta2, phi2)))

    @property
    def x1(self):
        return MeshScalar(self[:self.nx])

    @x1.setter
    def x1(self, value):
        self[:self.nx] = value

    @property
    def y1(self):
        return MeshScalar(self[self.nx:2 * self.nx])

    @y1.setter
    def y1(self, value):
        self[self.nx:2 * self.nx] = value

    @property
    def z1(self):
        return MeshScalar(self[2 * self.nx:3 * self.nx])

    @z1.setter
    def z1(self, value):
        self[2 * self.nx:3 * self.nx] = value

    @property
    def x2(self):
        return MeshScalar(self[3 * self.nx:4 * self.nx])

    @x2.setter
    def x2(self, value):
        self[3 * self.nx:4 * self.nx] = value

    @property
    def y2(self):
        return MeshScalar(self[4 * self.nx:5 * self.nx])

    @y2.setter
    def y2(self, value):
        self[4 * self.nx:5 * self.nx] = value

    @property
    def z2(self):
        return MeshScalar(self[5 * self.nx:6 * self.nx])

    @z2.setter
    def z2(self, value):
        self[5 * self.nx:6 * self.nx] = value


class FlattenedLocalAFMMeshVector(np.ndarray):
    r"""Class for antiferromagnets  of shape `(4*nx,)` to hold the flattened vector fields of each sublattice
    defined on the same mesh.

    This class is used to describe vector fields locally ortoghonal and expressed in the :math:`(u,v,w)` basis attached to the
    equilibrium magnetization of each sublattice, hence the name. Because of that, the third (:math:`w`) compontent of the vector field
    of each sublattice is always zero and therefore omitted.
    The entries of the vector are ordered according to

    .. math:: \mathbf{v} = (u_{1,1}, ... , u_{1,N}, v_{1,1}, ... , v_{1,N}, u_{2,1}, ... , u_{2,N}, v_{2,1}, ... , v_{2,N})


    while :math:`N` = `nx` and :math:`u_{i,j}` and :math:`v_{i,j}` being locally orthogonal components
     of the :math:`i` th sublattice at the :math:`j` th node.
    The input `array_like` needs to be one dimensional. The number of nodes `nx` is inferred from the input array.

    Attributes
    ----------
    nx : int
        Number of nodes.
    u1 : MeshScalar
        The :math:`u` component of the first sublattice vector field at each mesh node.
    v1 : MeshScalar
        The :math:`v` component of the first sublattice vector field at each mesh node.
    u1 : MeshScalar
        The :math:`u` component of the first sublattice vector field at each mesh node.
    v2 : MeshScalar
        The :math:`v` component of the second sublattice vector field at each mesh node.

    See Also
    --------
    make_flattened_AFM, FlattenedAFMMeshVectorSpherical
    """
    __array_priority__ = 15

    def __new__(cls, input_array):
        obj = np.asarray(input_array).view(cls)
        if len(obj.shape) == 1 and not isinstance(input_array, FlattenedLocalMeshVector):
            obj.nx = obj.shape[0] // 4
            return obj

        elif len(obj.shape) == 1 and isinstance(input_array, FlattenedLocalMeshVector):
            raise ValueError(f"Input {input_array} is a flattened local mesh vector. "
                             "These can only be converted to flattened mesh vectors "
                             "using the inverse of a projection operator.")
        else:
            raise ValueError(f"Input {input_array} is not 1-dimensional.")

    def __array_finalize__(self, obj):
        if obj is None: return
        self.nx = getattr(obj, 'nx', None)

    @property
    def u1(self):
        return MeshScalar(self[:self.nx])

    @u1.setter
    def u1(self, value):
        self[:self.nx] = value

    @property
    def v1(self):
        return MeshScalar(self[self.nx:2 * self.nx])

    @v1.setter
    def v1(self, value):
        self[self.nx:2 * self.nx] = value

    @property
    def u2(self):
        return MeshScalar(self[2 * self.nx:3 * self.nx])

    @u2.setter
    def u2(self, value):
        self[2 * self.nx:3 * self.nx] = value

    @property
    def v2(self):
        return MeshScalar(self[3 * self.nx:4 * self.nx])

    @v2.setter
    def v2(self, value):
        self[3 * self.nx:4 * self.nx] = value


class LocalMeshVector(np.ndarray):
    r"""Class for two-component vector fields of shape `(nx, 2)` defined on a mesh.

    This class is used to describe vector fields locally ortoghonal and expressed in the :math:`(u,v,w)` basis attached to the
    equilibrium magnetization, hence the name. Because of that, the third (:math:`w`) compontent of the vector field
    is always zero and therefore omitted.
    The entries of the vector field are ordered according to

    .. math::

      \mathbf{a} = \begin{pmatrix}
                a_{u_1} & a_{v_1} \\
                & \vdots & \\
                a_{u_N} & a_{v_N} \\
            \end{pmatrix}

    while :math:`N` = `nx`. The input `array_like` needs to be one dimensional and cannot be a
    :py:class:`FlattenedMeshVector`. The number of nodes `nx` is inferred from the length of the input array.

    Attributes
    ----------
    nx : int
        Number of nodes.
    u : MeshScalar
        The first (:math:`u`) component of the vector field at each mesh node.
    v : MeshScalar
        The second (:math:`v`) component of the vector field at each mesh node.

    Methods
    -------
    to_flattened()
        Returns the vector field as a :py:class:`FlattenedLocalMeshVector`.

    See Also
    --------
    MeshVector, FlattenedMeshVector, LocalMeshVector
    """

    def __new__(cls, input_array):
        obj = np.asarray(input_array).view(cls)
        if len(obj.shape) == 2:
            if obj.shape[1] != 2:
                raise ValueError(f"Input {input_array} malshaped. Vector dimension is {obj.shape[1]} and should be 2.")
            obj.nx = obj.shape[0]
            return obj
        else:
            raise ValueError(f"Input {input_array} is not 2-dimensional.")

    def to_flattened(self):
        return FlattenedLocalMeshVector(self.T.flatten())

    @property
    def u(self):
        return MeshScalar(self.T[0])

    @u.setter
    def u(self, value):
        self[:, 0] = value

    @property
    def v(self):
        return MeshScalar(self.T[1])

    @v.setter
    def v(self, value):
        self[:, 1] = value


class FlattenedLocalMeshVector(np.ndarray):
    r"""Class for two-component vector fields of shape `(2*nx,)` defined on a mesh.

    This class is used to describe vector fields locally ortoghonal and expressed in the :math:`(u,v,w)` basis attached to the
    equilibrium magnetization, hence the name. Because of that, the third (:math:`w`) compontent of the vector field
    is always zero and therefore omitted.
    The entries of the vector field are ordered according to

    .. math:: \mathbf{a} = (a_{u_1}, ... , a_{u_N}, a_{v_1}, ... , a_{v_N})

    while :math:`N` = `nx`. The input `array_like` needs to be one dimensional.
    The number of nodes `nx` is inferred from `shape[0]` of the input array.

    Attributes
    ----------
    nx : int
        Number of nodes.
    u : MeshScalar
        The first (:math:`u`) component of the vector field at each mesh node.
    v : MeshScalar
        The second (:math:`v`) component of the vector field at each mesh node.

    Methods
    -------
    to_unflattened()
        Returns the vector field as an unflattened :py:class:`LocalMeshVector`.

    See Also
    --------
    MeshVector, FlattenedMeshVector, FlattenedLocalMeshVector
    """

    def __new__(cls, input_array):
        obj = np.asarray(input_array).view(cls)
        if len(obj.shape) == 1 and not isinstance(input_array, FlattenedMeshVector):
            obj.nx = obj.shape[0] // 2
            return obj

        elif len(obj.shape) == 1 and isinstance(input_array, FlattenedMeshVector):
            raise ValueError(f"Input {input_array} is a flattened mesh vector. "
                             "These can only be converted to flattened local mesh vectors "
                             "using a projection operator.")
        else:
            raise ValueError(f"Input {input_array} is not 1-dimensional.")

    def to_unflattened(self):
        return LocalMeshVector(self.reshape(2, self.nx).T)

    @property
    def u(self):
        return MeshScalar(self[:self.nx])

    @u.setter
    def u(self, value):
        self[:self.nx] = value

    @property
    def v(self):
        return MeshScalar(self[self.nx:])

    @v.setter
    def v(self, value):
        self[self.nx:] = value


class FlattenedMeshVectorSpherical(np.ndarray):
    r"""Class for two-component unit-vector fields of shape `(2*nx,)` defined on a mesh using spherical angles.

    The entries of the vector field are ordered according to

    .. math:: \mathbf{v} = (\theta_1, ... , \theta_N, \varphi_1, ... , \varphi_N)

    while :math:`N` = `nx` and :math:`\theta_i` and :math:`\varphi_i` being polar and azimuthal angle of the unit-vector
    field. The input `array_like` needs to be one dimensional.
    The number of nodes `nx` is inferred from `shape[0]` of the input array.

    Attributes
    ----------
    nx : int
        Number of nodes.
    theta : MeshScalar
        The polar angle (:math:`\theta`) of the unit-vector field at each mesh node.
    phi : MeshScalar
        The azimuthal angle (:math:`\varphi`) of the unit-vector field at each mesh node.

    Methods
    -------
    to_cartesian()
        Returns the vector field as a flattened :py:class:`FlattenedMeshVector` in cartesian coordinates.

    See Also
    --------
    MeshVector, FlattenedMeshVector
    """

    def __new__(cls, input_array):
        obj = np.asarray(input_array).view(cls)
        if len(obj.shape) == 1 and not isinstance(input_array, MeshVector):
            obj.nx = obj.shape[0] // 2
            return obj

        else:
            raise ValueError(f"Input {input_array} is not 1-dimensional.")

    def to_cartesian(self):

        vec_x = np.sin(self.theta) * np.cos(self.phi)
        vec_y = np.sin(self.theta) * np.sin(self.phi)
        vec_z = np.cos(self.theta)

        return FlattenedMeshVector(np.concatenate((vec_x, vec_y, vec_z)).flatten())

    @property
    def theta(self):
        return MeshScalar(self[:self.nx])

    @theta.setter
    def theta(self, value):
        self[:self.nx] = value

    @property
    def phi(self):
        return MeshScalar(self[self.nx:])

    @phi.setter
    def phi(self, value):
        self[self.nx:] = value


class FlattenedAFMMeshVectorSpherical(np.ndarray):
    r"""Class for antiferromagnets of shape `(4*nx,)` holding two two-component unit-vector fields of shape `(2*nx,)`
    each defined on a mesh using spherical angles.

    The entries of the vector field are ordered according to

    .. math:: \mathbf{v} = (\theta_{1,1}, ... , \theta_{1,N}, \varphi_{1,1}, ... , \varphi_{1,N},\theta_{2,1}, ... , \theta_{2,N}, \varphi_{2,1}, ... , \varphi_{2,N})

    while :math:`N` = `nx` and :math:`\theta_{i,j}` and :math:`\varphi_{i,j}` being polar and azimuthal angle of the unit-vector
    field of the :math:`i` th sublattice at the :math:`j` th node. The input `array_like` needs to be one dimensional.
    The number of nodes `nx` is inferred from `shape[0]` of the input array.

    Attributes
    ----------
    nx : int
        Number of nodes.
    theta1 : MeshScalar
        The polar angle (:math:`\theta_1`) of the unit-vector field of the first sublattice at each mesh node.
    phi1 : MeshScalar
        The azimuthal angle (:math:`\varphi_1`) of the unit-vector of the first sublattice field at each mesh node.
    theta2 : MeshScalar
        The polar angle (:math:`\theta_2`) of the unit-vector field of the first sublattice at each mesh node.
    phi2 : MeshScalar
        The azimuthal angle (:math:`\varphi_2`) of the unit-vector of the first sublattice field at each mesh node.

    Methods
    -------
    to_cartesian()
        Returns the vector fields for both sublattices as a flattened :py:class:`FlattenedAFMMeshVector` in cartesian coordinates.

    See Also
    --------
    MeshVector, FlattenedAFMMeshVector
    """

    def __new__(cls, input_array):
        obj = np.asarray(input_array).view(cls)
        if len(obj.shape) == 1 and not isinstance(input_array, MeshVector):
            obj.nx = obj.shape[0] // 4
            return obj

        else:
            raise ValueError(f"Input {input_array} is not 1-dimensional.")

    def to_cartesian(self):

        vec_x1 = np.sin(self.theta1) * np.cos(self.phi1)
        vec_y1 = np.sin(self.theta1) * np.sin(self.phi1)
        vec_z1 = np.cos(self.theta1)

        vec_x2 = np.sin(self.theta2) * np.cos(self.phi2)
        vec_y2 = np.sin(self.theta2) * np.sin(self.phi2)
        vec_z2 = np.cos(self.theta2)

        return FlattenedAFMMeshVector(np.concatenate((vec_x1, vec_y1, vec_z1, vec_x2, vec_y2, vec_z2)).flatten())

    @property
    def theta1(self):
        return MeshScalar(self[:self.nx])

    @theta1.setter
    def theta1(self, value):
        self[:self.nx] = value

    @property
    def phi1(self):
        return MeshScalar(self[self.nx:2 * self.nx])

    @phi1.setter
    def phi1(self, value):
        self[self.nx:2 * self.nx] = value

    @property
    def theta2(self):
        return MeshScalar(self[2 * self.nx:3 * self.nx])

    @theta2.setter
    def theta2(self, value):
        self[2 * self.nx:3 * self.nx] = value

    @property
    def phi2(self):
        return MeshScalar(self[3 * self.nx:4 * self.nx])

    @phi2.setter
    def phi2(self, value):
        self[3 * self.nx:4 * self.nx] = value


def make_flattened_AFM(a, b):
    """Creates a :py:class:`FlattenedAFMMeshVector` from two individual :py:class:`MeshVector`s.

    Parameters
    ----------
    a : MeshVector
        Vector field of the first sublattice.
    b : MeshVector
        Vector field of the second sublattice.

    Returns
    -------
    FlattenedAFMMeshVector
        Flattened vector field of the AFM lattice.

    See Also
    --------
    FlattenedAFMMeshVector
    """
    a = MeshVector(a)
    b = MeshVector(b)
    afm_mag = FlattenedAFMMeshVector(
        np.concatenate((a.to_flattened(), b.to_flattened()))
    )
    return afm_mag


def flattened_spherical_to_cartesian_fm(vec,nx):
    """Convert a flattened mesh vector from spherical angles to cartesian coordinates.

    This method exists solely for cases where mesh-vector data types are overwritten by :py:func:`numpy.asarray`.
    This is the case for example in :py:func:`scipy.optimize.minimize`.

    Parameters
    ----------
    vec : array_like
        Array assumed to be of length `2*nx` (for ferromagnets) containing the spherical angles.
        Checks are not done to save time.
    nx : int
        Number of nodes.

    Returns
    -------
    numpy.array
        Array of length 3*nx containing the cartesian coordinates.

    See Also
    --------
    flattened_spherical_to_cartesian_afm
    """
    theta = vec[:nx]
    phi = vec[-nx:]

    # convert to carthesian coordinates
    vec_x = np.sin(theta) * np.cos(phi)
    vec_y = np.sin(theta) * np.sin(phi)
    vec_z = np.cos(theta)
    return np.concatenate((vec_x, vec_y, vec_z))


def flattened_spherical_to_cartesian_afm(vec,nx):
    """Convert a flattened mesh vector from spherical angles to cartesian coordinates.

    This method exists solely for cases where mesh-vector data types are overwritten by :py:func:`numpy.asarray`.
    This is the case for example in :py:func:`scipy.optimize.minimize`.

    Parameters
    ----------
    vec : array_like
        Array assumed to be of length `4*nx` (for antiferromagnets) containing the spherical angles of both sublattices
        Checks are not done to save time.
    nx : int
        Number of nodes.

    Returns
    -------
    numpy.array
        Array of length 6*nx containing the cartesian coordinates.

    See Also
    --------
    flattened_spherical_to_cartesian_fm
    """
    theta1 = vec[:nx]
    phi1 = vec[nx:2*nx]

    theta2 = vec[2*nx: 3*nx]
    phi2 = vec[3*nx:4*nx]

    # convert to carthesian coordinates
    vec_x1 = np.sin(theta1) * np.cos(phi1)
    vec_y1 = np.sin(theta1) * np.sin(phi1)
    vec_z1 = np.cos(theta1)

    vec_x2 = np.sin(theta2) * np.cos(phi2)
    vec_y2 = np.sin(theta2) * np.sin(phi2)
    vec_z2 = np.cos(theta2)
    return np.concatenate((vec_x1, vec_y1, vec_z1, vec_x2, vec_y2, vec_z2))