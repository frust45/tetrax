"""
This submodule provides some helper functions for input and output of files,
as well as some mesh checkers.
"""
import os

import bibtexparser
import meshio
import numpy as np

from ..core.mesh import MeshVector, make_flattened_AFM

# from meshio
topological_dimension = {
    "line": 1,
    "polygon": 2,
    "triangle": 2,
    "quad": 2,
    "tetra": 3,
    "hexahedron": 3,
    "wedge": 3,
    "pyramid": 3,
    "line3": 1,
    "triangle6": 2,
    "quad9": 2,
    "tetra10": 3,
    "hexahedron27": 3,
    "wedge18": 3,
    "pyramid14": 3,
    "vertex": 0,
    "quad8": 2,
    "hexahedron20": 3,
    "triangle10": 2,
    "triangle15": 2,
    "triangle21": 2,
    "line4": 1,
    "line5": 1,
    "line6": 1,
    "tetra20": 3,
    "tetra35": 3,
    "tetra56": 3,
    "quad16": 2,
    "quad25": 2,
    "quad36": 2,
    "triangle28": 2,
    "triangle36": 2,
    "triangle45": 2,
    "triangle55": 2,
    "triangle66": 2,
    "quad49": 2,
    "quad64": 2,
    "quad81": 2,
    "quad100": 2,
    "quad121": 2,
    "line7": 1,
    "line8": 1,
    "line9": 1,
    "line10": 1,
    "line11": 1,
    "tetra84": 3,
    "tetra120": 3,
    "tetra165": 3,
    "tetra220": 3,
    "tetra286": 3,
    "wedge40": 3,
    "wedge75": 3,
    "hexahedron64": 3,
    "hexahedron125": 3,
    "hexahedron216": 3,
    "hexahedron343": 3,
    "hexahedron512": 3,
    "hexahedron729": 3,
    "hexahedron1000": 3,
    "wedge126": 3,
    "wedge196": 3,
    "wedge288": 3,
    "wedge405": 3,
    "wedge550": 3,
    "VTK_LAGRANGE_CURVE": 1,
    "VTK_LAGRANGE_TRIANGLE": 2,
    "VTK_LAGRANGE_QUADRILATERAL": 2,
    "VTK_LAGRANGE_TETRAHEDRON": 3,
    "VTK_LAGRANGE_HEXAHEDRON": 3,
    "VTK_LAGRANGE_WEDGE": 3,
    "VTK_LAGRANGE_PYRAMID": 3,
}

reference_tetrax = {
    'doi': '10.14278/rodare.1418', 'month': 'jan', 'year': '2022',
    'author': 'K{\\"o}rber, Lukas and Quasebarth, Gwendolyn and Hempel, Alexander and Zahn, Friedrich and Andreas, Otto and Westphal, Elmar and Hertel, Riccardo and K{\\\'a}kay, Attila',
    'title': '{{TetraX}}: {{Finite-Element Micromagnetic-Modeling Package}}', 'ENTRYTYPE': 'misc',
    'ID': 'tetrax'}

reference_dynmat2d = {'publisher': 'AIP Publishing LLC', 'year': '2021', 'pages': '095006', 'number': '9',
                      'volume': '11', 'journal': 'AIP Advances',
                      'author': 'K{\\"o}rber, Lukas and Quasebarth, Gwendolyn and Otto, Andreas and K{\\\'a}kay, Attila',
                      'title': 'Finite-element dynamic-matrix approach for spin-wave dispersions in magnonic waveguides with arbitrary cross section',
                      'ENTRYTYPE': 'article', 'ID': 'fem_dynmat_SW',
                      'comment': 'For eigenmodes(). FEM dynamic matrix approach for propagating spin waves in waveguides with arbitrary cross section.'}

reference_dynmat1d = {'copyright': 'arXiv.org perpetual, non-exclusive license', 'year': '2022', 'publisher': 'arXiv',
                      'title': 'Finite-element dynamic-matrix approach for propagating spin waves: Extension to mono- and multilayers of arbitrary spacing and thickness',
                      'keywords': 'Mesoscale and Nanoscale Physics (cond-mat.mes-hall), Materials Science (cond-mat.mtrl-sci), FOS: Physical sciences, FOS: Physical sciences',
                      'author': 'Körber, Lukas and Hempel, Alexander and Otto, Andreas and Gallardo, Rodolfo and Henry, Yves and Lindner, Jürgen and Kákay, Attila',
                      'url': 'https://arxiv.org/abs/2207.01519', 'doi': '10.48550/ARXIV.2207.01519',
                      'ENTRYTYPE': 'misc', 'ID': 'fem_dynmat_SW_layers',
                      'comment': 'For eigenmodes(). FEM dynamic matrix approach for propagating spin waves in infinitely extended multilayers.'}

reference_hexa = {'publisher': 'APS', 'year': '2021', 'pages': '184429', 'number': '18', 'volume': '104',
                  'journal': 'Physical Review B',
                  'author': 'K{\\"o}rber, Lukas and Zimmermann, Michael and Wintz, Sebastian and Finizio, Simone and Kronseder, Matthias and Bougeard, Dominique and Dirnberger, Florian and Weigand, Markus and Raabe, J{\\"o}rg and Ot{\\\'a}lora, Jorge A and others',
                  'title': 'Symmetry and curvature effects on spin waves in vortex-state hexagonal nanotubes',
                  'ENTRYTYPE': 'article', 'ID': 'korber2021symmetry',
                  'comment': 'For absorption(). Description how to calculate line widhts and absorption from mode profiles of propagating spin waves.'}

reference_roman = {'keywords': 'damping,gyrotropic,linear,theory,vortex',
                   'doi': '10.1103/PhysRevB.98.104408', 'pages': '104408', 'number': '10', 'volume': '98',
                   'journal': 'Physical Review B', 'month': 'apr', 'year': '2018',
                   'author': 'Verba, Roman and Tiberkevich, Vasil and Slavin, Andrei',
                   'title': 'Damping of Linear Spin-Wave Modes in Magnetic Nanostructures: {{Local}}, Nonlocal, and Coordinate-Dependent Damping',
                   'ENTRYTYPE': 'article', 'ID': 'verbaDampingLinearSpinwave2018',
                   'comment': 'For absorption(). Description how to calculate line widhts and absorption from general mode profiles.'}

reverse_engineering_reference = {
    'publisher': 'APS', 'year': '2021', 'pages': '174414', 'number': '17',
    'volume': '104', 'journal': 'Physical Review B',
    'author': 'K{\\"o}rber, Lukas and K{\\\'a}kay, Attila',
    'title': 'Numerical reverse engineering of general spin-wave dispersions: Bridge between numerics and analytics using a dynamic-matrix approach',
    'ENTRYTYPE': 'article', 'ID': 'korber2021numerical',
    'comment': 'Description how to numerically calculate perturbed SW dispersion and individual stiffness fields.'}

references = {"tetrax": [reference_tetrax],
              "eigenmodes2d": [reference_dynmat2d],
              "eigenmodes3d": [reference_dynmat2d],
              "absorption1d": [reference_hexa, reference_roman],
              "absorption2d": [reference_hexa, reference_roman],
              "eigenmodes1d": [reference_dynmat1d],
              "perturbation1d": [reference_dynmat1d, reverse_engineering_reference],
              "perturbation2d": [reference_dynmat2d, reverse_engineering_reference],
              "linewidth": [reference_roman]
              }


def read_mode_from_vtk(fname, as_flattened=False, magnetic_order="FM"):
    """Reads a complex-valued spatial mode profile from a ´vtk` file.

    Parameters
    ----------
    fname : str
        Filename of vtk file.
    as_flattened : bool, optional
        Return mode profile as flattened mesh vector (default is `False`).

    Returns
    -------
    MeshVector or FlattenedMeshVector
        Mode profile as mesh vector of shape `(N, 3)` if `as_flattened=False` or
        as flattened mesh vector of shape `(3*N,)` if `as_flattened=True`.

    Notes
    -----
    Requires that the vtk file contains the data fields "Re(m_x)", "Im(m_x)" and so forth.
    """
    vtk_file = meshio.read(fname)
    #mx = mode.point_data["Re(m_x)"] + 1j * mode.point_data["Im(m_x)"]
    #my = mode.point_data["Re(m_y)"] + 1j * mode.point_data["Im(m_y)"]
    #mz = mode.point_data["Re(m_z)"] + 1j * mode.point_data["Im(m_z)"]
    if magnetic_order == "FM":
        mode = MeshVector(vtk_file.point_data["Re(m)"] + 1j*vtk_file.point_data["Im(m)"])
        return mode.to_flattened() if as_flattened else mode

    if magnetic_order == "AFM":
        mode1 = MeshVector(vtk_file.point_data["Re(m1)"] + 1j*vtk_file.point_data["Im(m1)"])
        mode2 = MeshVector(vtk_file.point_data["Re(m2)"] + 1j*vtk_file.point_data["Im(m2)"])
        return make_flattened_AFM(mode1, mode2) if as_flattened else (mode1, mode2)

    #return np.array([mx, my, mz]).flatten() if as_flattened else np.array([mx, my, mz]).T


def write_field_to_file(field, sample, fname="field.vtk", qname=None):
    """
    Writes a scalar or vector field (MeshScalar or MeshVector) or a list of fields to a file using meshio.

    Parameters
    ----------
    field : MeshScalar, MeshVector array_like, or list
        Input scalar- or vector field(s) to be saved to the file. If input is not MeshScalar or MeshVector,
        a shape check will be performed to infer the data type.
    sample : AbstractSample
        The sample on which the scalar- or vector field is defined. Necessary to save mesh with vtk.
    fname : str
        Name of the file, has to contain a file ending from which the format can be
        inferred (Default is `"field.vtk"`)
    qname : str or list(str)
        Name(s) of the scalar or vector quantity (quantities) to appear in the file (default is `None`). If `qname` is `None`, then
        the name(s) will be set to `scalar` or `vector`, depending on each input field.
    """
    # make to list if not already list
    fields = [field] if not isinstance(field, list) else field
    qnames = [qname] if not isinstance(qname, list) else qname

    # dictionary to save to vtk
    save_dict = {}
    for qname, field in zip(qnames, fields):
        if field.shape == (sample.nx,):
            # scalar field case
            qname = "scalar" if qname is None else qname

        elif field.shape == (sample.nx, 3):
            # vector field case

            if not isinstance(field, MeshVector):
                field = MeshVector(field)

            qname = "vector" if qname is None else qname


        else:
            print("Malshaped input field. Only fields with shape of appropriate MeshScalar or MeshVector can be saved.")

        save_dict[qname] = field

    meshio.write_points_cells(filename=fname,
                              points=sample.xyz,
                              cells=sample.mesh.cells,
                              point_data=save_dict,
                              )

def get_mesh_dimension(mesh: meshio.Mesh) -> int:
    """Obtain the dimension of a mesh.

    The dimension of the mesh is obtained as the highest topoligical dimension
    of the different cell types in the mesh.

    Parameters
    ----------
    mesh : meshio.Mesh
        Mesh object.

    Returns
    -------
    int
        Dimension of the mesh (0, 1, 2 or 3).
    """

    mesh_dim = max([topological_dimension[cellblock.type] for cellblock in mesh.cells])

    return mesh_dim


def check_mesh_shape(mesh: meshio.Mesh, dim: int) -> bool:
    """Check if mesh is properly shaped.

    - ``dim == 3``: always `True`
    - ``dim == 2``: `True` if mesh is in `xy` plane.
    - ``dim == 1``: `True` if mesh is on `y` axis.

    Parameters
    ----------
    mesh : meshio.Mesh
        Mesh object.
    dim : int
        Dimension of mesh.

    Returns
    -------
    bool
    """

    if dim == 3:
        properly_shaped = True

    elif dim == 2:
        # check if all z coordinates are 0.
        z = mesh.points.T[2]
        properly_shaped = np.any(z) == 0

    elif dim == 1:
        # check if all z and x coordinates are 0.
        x = mesh.points.T[0]
        z = mesh.points.T[2]
        properly_shaped = np.any(z) == 0 and np.any(x) == 0

    else:
        raise ValueError(
            'Illegal value for "dim", should be 1, 2, or 3.'
        )

    return properly_shaped


def add_to_bib(sample, entries_for):
    """Adds the references relevant for a certain numerical experiment (depending on the sample geometry) to the bibtex
    file ``references.bib`` of the sample. If this file does not exist, it will be created.

    Parameters
    ----------
    sample : AbstractSample
        Sample object at hand.
    entries_for : {'eigenmodes1d', 'eigenmodes2d', 'absorption', 'tetrax', 'linewidth'}
        Can be in

    """

    bibfile_name = sample.name + "/references.bib"

    # if sample directory does not exist yet, create it
    if not os.path.exists(sample.name):
        os.makedirs(sample.name)

    # if the bib file does not exist, create it
    if not os.path.exists(bibfile_name):
        open(bibfile_name, 'a').close()

    # now open the file
    with open(bibfile_name) as bibtex_file:
        bib_database = bibtexparser.load(bibtex_file)

    ids = get_ids(bib_database)

    for entry in references[entries_for]:
        if not entry["ID"] in ids:
            bib_database.entries.append(entry)

    # write back to file
    with open(bibfile_name, 'w') as bibtex_file:
        bibtexparser.dump(bib_database, bibtex_file)


def get_ids(db):
    """Get the list of the bibtex key in a bibtex database."""

    ids = []
    for entry in db.entries:
        # print(entry)
        this_id = entry["ID"]
        # print(this_id)
        ids.append(this_id)
    return ids


def get_num_modes_from_dataframe(dataframe)-> int:
    """Returns the number of modes from dispersion dataframe."""

    columns = dataframe.columns
    matching = [s for s in columns if "f" in s]

    # have to subtract possible columns of frequencies of perturbed modes
    matching_pert = [s for s in columns if "f_" in s]
    num_modes = len(matching)-len(matching_pert)
    return num_modes
