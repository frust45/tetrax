"""
This submodule contains a number of mathematical functions. The import convention

    >>> import numpy as np

is used.
"""
import numpy as np
from scipy.sparse import dia_matrix, bmat
from scipy.interpolate import griddata

from ..core.mesh import *


def normalize_single_3d_vec(vec):
    """Normalizes a vector field.

    Parameters
    ----------
    vec : MeshVector
        Mesh vector of shape `(N,3)`.

    Returns
    -------
    vec_normalized : MeshVector
        Normalized mesh vector of shape `(N,3)`.
    """
    norm = np.sqrt(vec.T[0] ** 2 + vec.T[1] ** 2 + vec.T[2] ** 2)
    norm = np.repeat(norm, 3)
    norm = np.reshape(norm, (len(norm) // 3, 3))
    return np.array(vec) / norm


def flattened_mesh_vec_abs(vec):
    """Calculates the node-wise magnitude of a :class:`FlattenedMeshVector` :math:`\mathbf{v}`.

    Assumes that the mesh vector is of shape `(3*N,)` (with `N` being the number of nodes in the mesh) and is ordered according to

    .. math:: \mathbf{v} = (v_{x_1}, ... , v_{x_N}, v_{y_1}, ... , v_{y_N}, v_{z_1}, ... , v_{z_N})

    At each node :math:`i`, the magnitude is calculated as

    .. math:: \| \mathbf{v}_i \| = \sqrt{v_{x_i}^2 + v_{y_i}^2+ v_{z_i}^2}


    Parameters
    ----------
    vec : FlattenedMeshVector
        Flattened mesh vector of shape `(3*N,)`.

    Returns
    -------
    MeshScalar
        Mesh scalar of shape `(N,)`.
    """
    nx = vec.shape[0] // 3
    return np.sqrt(vec[:nx] ** 2 + vec[nx:2 * nx] ** 2 + vec[2 * nx:] ** 2)


def flattened_mesh_vec_scalar_product(vec1, vec2):
    r"""Calculates the node-wise inner product of two flattened mesh vectors :math:`\mathbf{v}` and :math:`\mathbf{w}`.

    Assumes that the mesh vectors are of shape `(3*N,)` (with `N` being the number of nodes in the mesh) and are ordered according to

    .. math:: \mathbf{v} = (v_{x_1}, ... , v_{x_N}, v_{y_1}, ... , v_{y_N}, v_{z_1}, ... , v_{z_N})

    and

    .. math:: \mathbf{w} = (w_{x_1}, ... , w_{x_N}, w_{y_1}, ... , w_{y_N}, w_{z_1}, ... , w_{z_N})

    At each node :math:`i`, the inner product is defined as

    .. math:: \mathbf{v}_i \cdot \mathbf{w}_i = \sum\limits_{j=x,y,z} v_{j_i}w_{j_i}

    Parameters
    ----------
    vec1 : FlattenedMeshVector
        Flattened mesh vector of shape `(3*N,)`.

    vec2 : FlattenedMeshVector
        Flattened mesh vector of shape `(3*N,)`.

    Returns
    -------
    MeshScalar
        Flattened mesh scalar of shape `(N,)`.
    """

    nx = vec1.shape[0] // 3
    assert vec1.shape[0] == 3 * nx and vec2.shape[0] == 3 * nx
    return vec1[:nx] * vec2[:nx] + vec1[nx:2 * nx] * vec2[nx:2 * nx] \
           + vec1[2 * nx:] * vec2[2 * nx:]


def flattened_AFMmesh_vec_scalar_product_separate(vec1, vec2):
    r"""Calculates the node-wise inner product of two flattened mesh vectors :math:`\mathbf{v}` and :math:`\mathbf{w}`.

    Assumes that the mesh vectors are of shape `(3*N,)` (with `N` being the number of nodes in the mesh) and are ordered according to

    .. math:: \mathbf{v} = (v_{x_1}, ... , v_{x_N}, v_{y_1}, ... , v_{y_N}, v_{z_1}, ... , v_{z_N})

    and

    .. math:: \mathbf{w} = (w_{x_1}, ... , w_{x_N}, w_{y_1}, ... , w_{y_N}, w_{z_1}, ... , w_{z_N})

    At each node :math:`i`, the inner product is defined as

    .. math:: \mathbf{v}_i \cdot \mathbf{w}_i = \sum\limits_{j=x,y,z} v_{j_i}w_{j_i}

    Parameters
    ----------
    vec1 : FlattenedMeshVector
        Flattened mesh vector of shape `(3*N,)`.

    vec2 : FlattenedMeshVector
        Flattened mesh vector of shape `(3*N,)`.

    Returns
    -------
    MeshScalar
        Flattened mesh scalar of shape `(N,)`.
    """

    nx = vec1.shape[0] // 6
    assert vec1.shape[0] == 6 * nx and vec2.shape[0] == 6 * nx
    return np.concatenate(
        (vec1[0:nx] * vec2[0:nx] + vec1[nx:2 * nx] * vec2[nx:2 * nx] + vec1[2 * nx:3 * nx] * vec2[2 * nx:3 * nx],
         vec1[3 * nx:4 * nx] * vec2[3 * nx:4 * nx] + vec1[4 * nx:5 * nx] * vec2[4 * nx:5 * nx] +
         vec1[5 * nx:6 * nx] * vec2[5 * nx:6 * nx]))


def flattened_AFMmesh_vec_scalar_product(vec1, vec2):
    r"""tba
    """

    nx = vec1.shape[0] // 6
    assert vec1.shape[0] == 6 * nx and vec2.shape[0] == 6 * nx
    return (vec1[0:nx] * vec2[0:nx] + vec1[nx:2 * nx] * vec2[nx:2 * nx] + vec1[2 * nx:3 * nx] * vec2[2 * nx:3 * nx] +
            vec1[3 * nx:4 * nx] * vec2[3 * nx:4 * nx] + vec1[4 * nx:5 * nx] * vec2[4 * nx:5 * nx] +
            vec1[5 * nx:6 * nx] * vec2[5 * nx:6 * nx])


def flattened_mesh_vec_tensor_product(vec1, vec2):
    r"""Calculates the node-wise tensor product of two flattened mesh vectors :math:`\mathbf{v}` and :math:`\mathbf{w}`.

    Assumes that the mesh vectors are of shape `(3*N,)` (with `N` being the number of nodes in the mesh) and are ordered according to

    .. math:: \mathbf{v} = (v_{x_1}, ... , v_{x_N}, v_{y_1}, ... , v_{y_N}, v_{z_1}, ... , v_{z_N})

    and

    .. math:: \mathbf{w} = (w_{x_1}, ... , w_{x_N}, w_{y_1}, ... , w_{y_N}, w_{z_1}, ... , w_{z_N})

    At each node :math:`i`, the tensor product is defined as

    .. math::

      \mathbf{v}_i \otimes \mathbf{w}_i = \begin{pmatrix}
                v_{x_i} w_{x_i} & v_{x_i} w_{y_i} & v_{x_i} w_{z_i} \\
                v_{y_i} w_{x_i} & v_{y_i} w_{y_i} & v_{y_i} w_{z_i} \\
                v_{z_i} w_{x_i} & v_{z_i} w_{y_i} & v_{z_i} w_{z_i}
            \end{pmatrix}

    The result is returned as a sparse block matrix of shape `(3*N, 3*N)`, where the entries are ordered as

    .. math::

      \hat{\mathbf{U}} = \begin{pmatrix}
                \hat{\mathbf{U}}_{xx} & \hat{\mathbf{U}}_{xy} & \hat{\mathbf{U}}_{xz} \\
                \hat{\mathbf{U}}_{yx} & \hat{\mathbf{U}}_{yy} & \hat{\mathbf{U}}_{yz} \\
                \hat{\mathbf{U}}_{zx} & \hat{\mathbf{U}}_{zy} & \hat{\mathbf{U}}_{zz}
            \end{pmatrix}

    in the diagonal blocks :math:`\hat{\mathbf{U}}_{xx} = \mathrm{diag}(v_{x_1} w_{x_1}, ..., v_{x_N} w_{x_N})` and so forth.

    Parameters
    ----------
    vec1 : FlattenedMeshVector
        Flattened mesh vector of shape `(3*N,)`.

    vec2 : FlattenedMeshVector
        Flattened mesh vector of shape `(3*N,)`.

    Returns
    -------
    scipy.sparse.csr_matrix
        Sparse matrix of shape ``(3*N, 3*N)`` in ``csr`` format.
    """

    nx = vec1.shape[0] // 3
    assert vec1.shape[0] == 3 * nx and vec2.shape[0] == 3 * nx

    tmv1x = vec1[:nx]
    tmv1y = vec1[nx:2 * nx]
    tmv1z = vec1[2 * nx:]

    tmv2x = vec2[:nx]
    tmv2y = vec2[nx:2 * nx]
    tmv2z = vec2[2 * nx:]

    # only calculate necessary elements (only 6 distinct) and make diagonal blocks from them
    dxx = dia_matrix((tmv1x * tmv2x, 0), shape=(nx, nx))
    dxy = dia_matrix((tmv1x * tmv2y, 0), shape=(nx, nx))
    dxz = dia_matrix((tmv1x * tmv2z, 0), shape=(nx, nx))

    dyx = dia_matrix((tmv1y * tmv2x, 0), shape=(nx, nx))
    dyy = dia_matrix((tmv1y * tmv2y, 0), shape=(nx, nx))
    dyz = dia_matrix((tmv1y * tmv2z, 0), shape=(nx, nx))

    dzx = dia_matrix((tmv1z * tmv2x, 0), shape=(nx, nx))
    dzy = dia_matrix((tmv1z * tmv2y, 0), shape=(nx, nx))
    dzz = dia_matrix((tmv1z * tmv2z, 0), shape=(nx, nx))

    # stitch blocks together

    sparse_mat = bmat([[dxx, dxy, dxz], [dyx, dyy, dyz], [dzx, dzy, dzz]], format="csr")

    return sparse_mat


def flattened_mesh_vec_scalar_product2d(vec1, vec2):
    r"""Calculates the node-wise inner product of two flattened mesh vectors :math:`\mathbf{v}` and :math:`\mathbf{w}` which are define in a 2D vector field.

    Assumes that the mesh vectors are of shape `(2*N,)` (with `N` being the number of nodes in the mesh) and are ordered according to

    .. math:: \mathbf{v} = (v_{x_1}, ... , v_{x_N}, v_{y_1}, ... , v_{y_N})

    and

    .. math:: \mathbf{w} = (w_{x_1}, ... , w_{x_N}, w_{y_1}, ... , w_{y_N})

    At each node :math:`i`, the inner product is defined as

    .. math:: \mathbf{v}_i \cdot \mathbf{w}_i = \sum\limits_{j=x,y} v_{j_i}w_{j_i}

    Parameters
    ----------
    vec1 : FlattenedLocalMeshVector
        Flattened local mesh vector of shape `(2*N,)`.

    vec2 : FlattenedLocalMeshVector
        Flattened local mesh vector of shape `(2*N,)`.

    Returns
    -------
    MeshScalar
        Flattened mesh scalar of shape `(N,)`.
    """

    nx = vec1.shape[0] // 2
    assert vec1.shape[0] == 2 * nx and vec2.shape[0] == 2 * nx
    return vec1[:nx] * vec2[:nx] + vec1[nx:2 * nx] * vec2[nx:2 * nx]


def sample_average(field, sample):
    """Calculates the sample average of a scalar or vector field.

    Parameters
    ----------
    field : MeshScalar or MeshVector
    sample : AbstractSample
        Sample on which the field is define. Required to obtain the (generalized) volume elments.

    Returns
    -------
    averaged_field : float or numpy.array
        Returns averaged scalar fields as a ``float`` and averaged vector fields as a triplet of ``float``s.
    """

    total_volume = np.sum(sample.dA)

    if field.shape == (sample.nx,):
        # scalar field case
        averaged_field = np.sum(field * sample.dA) / total_volume
        return averaged_field

    elif field.shape == (sample.nx, 3):
        # vector field case

        if not isinstance(field, MeshVector):
            field = MeshVector(field)

        averaged_field_x = np.sum(field.x * sample.dA) / total_volume
        averaged_field_y = np.sum(field.y * sample.dA) / total_volume
        averaged_field_z = np.sum(field.z * sample.dA) / total_volume

        return np.array([averaged_field_x, averaged_field_y, averaged_field_z])


def interpolate_along_curve(xyz, field, curve, num_points=100, return_curve=False, dim=2):
    """
    Interpolate unstructured data along a specified line or curve.

    Parameters:
    -----------
    xyz : MeshVector, shape (n, 3)
        The coordinates on the mesh.
    field : MeshScalar or MeshVector
        The data values  associated with each data point.
    curve : ndarray, shape (nx, 3) or tuple/list of shape (2, 3)
        The points along the interpolation curve or the start and end points of the curve
        ``((x1, y1, z1),(x2, y2, z2))``.
    num_points : int
        The number of points to interpolate along the curve. Will only be considered if start
        and end points are given (default is 100).
    return_curve : bool
        Next to the interpolated values, also return the interpolation curve.
    dim : int
        Dimension of the mesh (needs to be specified for interpolation to work properly).

    Returns:
    --------
    interp_values : ndarray, shape (num_points, m)
        The interpolated values along the curve with ``m`` being the size of the second dimension of ``field``.
    """

    curve = np.array(curve)

    # start and end point was supplied
    if curve.shape == (2, 3):

        start_point = curve[0]
        end_point = curve[1]
        x = np.linspace(start_point[0], end_point[0], num_points)
        y = np.linspace(start_point[1], end_point[1], num_points)
        z = np.linspace(start_point[2], end_point[2], num_points)
        curve_to_fit = np.vstack((x, y, z)).T

    # A set of points was supplied for the curve
    elif curve.shape[0] > 2 and curve.shape[1] == 3:
        curve_to_fit = curve

    # Nothing proper was supplied.
    else:
        print("Malshaped 'curve' argument. Should be either an array_like of shape "
              "(nx, 3) or a array_like of shape (2, 3) containing the start and end point as ((x1, y1, z1),"
              "(x2, y2, z2)).")
        return None

    # throwing away unecessary values depending on the dimension (need for griddata)
    xyz_to_interpolate = xyz if dim == 3 else (xyz[:, :2] if dim == 2 else xyz[:, 1])
    curve_to_interpolate = curve_to_fit if dim == 3 else (curve_to_fit[:, :2] if dim == 2 else curve_to_fit[:, 1])

    scan = griddata(xyz[:, :2], field, curve_to_interpolate[:, :2])
    return (scan, curve_to_fit) if return_curve else scan

def spherical_angles_to_mesh_vector(theta, phi, as_flattened=False):
    """Node-wise, convert spherical angles to a unit vector.

    Assumes that `theta` and `phi` are of shape `(N,)` with `N` being the number of mesh nodes.

    Parameters
    ----------
    theta : MeshScalar
        Polar angle at each node point.
    phi : MeshScalar
        Azimuthal angle at each node point.
    as_flattened : bool, optional
        Return resulting unit vector as flattened mesh vector (default is `False`).

    Returns
    -------
    MeshVector or FlattenedMeshVector
       Unit mesh vector of shape `(N, 3)` if `as_flattened=False` or as flattened mesh vector of shape `(3*N,)` if `as_flattened=True`.
    """

    nx = len(theta)
    vec_x = np.sin(theta) * np.cos(phi)
    vec_y = np.sin(theta) * np.sin(phi)
    vec_z = np.cos(theta)

    vec_flattened = FlattenedMeshVector(np.concatenate((vec_x, vec_y, vec_z)).flatten())
    return vec_flattened if as_flattened else vec_flattened.to_unflattened()


def diag_matrix_from_vec(v):
    """
    Given a vector v (length n), construct a sparse matrix with dimension n x
    n. The diagonal of the matrix is v.
    """
    return dia_matrix(([v], [0]), shape=(len(v), len(v)))


def h_strip(k, A, L, s):
    r"""Fourier transform in `z` direction of one component of the microwave field distribution of a stripline antenna.

    Parameters
    ----------
    k : float
        Wave vector along :math:`z` direction.
    A : float
        Amplitude of the field.
    L : float
        Width of antenna (in :math:`z` direction).
    s : float
        Distance from the center plane of the antenna.

    Returns
    -------
    float

    See Also
    --------
    h_U, h_CPW, h_hom

    Notes
    -----
    Assuming an infinitely thin antenna [1]_, the wave-vector dependent magnetitude is calculated according to

    .. math::
        h_\mathrm{strip}(k)=A\frac{\sin(kL/2)}{kL/2} e^{-\vert k\vert s}.

    References
    ----------
    .. [1] M. Sushruth, *et al.*, "Electrical spectroscopy of forward volume spin
           waves in perpendicularly magnetized materials", `Physical
           Review Research 2, 043203 (2020) <https://journals.aps.org/prresearch/abstract/10.1103/PhysRevResearch.2.043203>`_.
    """

    # sinc is evaluated using np.divide in order to catch divide-by-zero exception
    sinc = np.divide(np.sin(k * L / 2), (k * L / 2), out=np.ones_like(k), where=(k != 0))

    return A * sinc * np.exp(-np.abs(k * s))


def h_CPW(k, A, L, g, s):
    r"""Fourier transform in `z` direction of one component of the microwave field distribution of a CPW antenna.

    Parameters
    ----------
    k : float
        Wave vector along :math:`z` direction.
    A : float
        Amplitude of the field.
    L : float
        Width of individual current lines (in :math:`z` direction).
    g : float
        Gap between individual current lines (in :math:`z` direction). Do not confuse with pitch.
    s : float
        Distance from the center plane of the antenna.

    Returns
    -------
    float

    See Also
    --------
    h_strip : This function is used in `h_CPW`.
    h_U, h_hom

    Notes
    -----
    Assuming an infinitely thin antenna [1]_, the wave-vector dependent magnetitude is calculated according to

    .. math::
        h_\mathrm{CPW}(k)=2A\sin^2\big(k(g+L)/2\big)\,h_\mathrm{strip}(k).

    References
    ----------
        .. [1] M. Sushruth, *et al.*, "Electrical spectroscopy of forward volume spin
           waves in perpendicularly magnetized materials", `Physical
           Review Research 2, 043203 (2020) <https://journals.aps.org/prresearch/abstract/10.1103/PhysRevResearch.2.043203>`_.
    """

    return 2 * np.sin(k * (g + L) / 2) ** 2 * h_strip(k, A, L, s)


def h_U(k, A, L, g, s):
    r"""Fourier transform in `z` direction of one component of the microwave-field distribution of a U-shaped antenna.

    Parameters
    ----------
    k : float
        Wave vector along :math:`z` direction.
    A : float
        Amplitude of the field.
    L : float
        Width of individual current lines (in :math:`z` direction).
    g : float
        Gap between individual current lines (in :math:`z` direction). Do not confuse with pitch.
    s : float
        Distance from the center plane of the antenna.

    Returns
    -------
    float

    See Also
    --------
    h_strip : This function is used in `h_CPW`.
    h_U, h_hom

    Notes
    -----
    Assuming an infinitely thin antenna [1]_, the wave-vector dependent magnetitude is calculated according to

    .. math::
        h_\mathrm{CPW}(k)=2iA\sin\big(k(g+L)/2\big)\,h_\mathrm{strip}(k).

    References
    ----------
    .. [1] M. Sushruth, *et al.*, "Electrical spectroscopy of forward volume spin
           waves in perpendicularly magnetized materials", `Physical
           Review Research 2, 043203 (2020) <https://journals.aps.org/prresearch/abstract/10.1103/PhysRevResearch.2.043203>`_.
    """

    return 2 * 1j * np.sin(k * (g + L) / 2) * h_strip(k, A, L, s)


def h_hom(k, A):
    """Fourier transform in `z` direction of one component of the of one component of the microwave-field distribution of a CPW antenna.

    Parameters
    ----------
    k : float
        Wave vector along :math:`z` direction.
    A : float
        Amplitude of the field.

    Returns
    -------
    float

    See Also
    --------
    h_strip, h_CPW, h_U

    Notes
    -----
    The wave-vector dependent magnetitude is calculated according to

    .. math::
        h_\mathrm{hom}(k)=A\delta(k).

    with :math:`\delta` being the Dirac distribution.
    """
    return A * np.piecewise(k, [k == 0, k != 0], [1, 0])


def matrix_elem(i, j, A, dV):
    """
    Calculates the element of an operator `A` in the basis with respect to the basis vectors `i` and `j` as

    .. math::
        A_{i,j} = \int\mathrm{d}V \ \mathbf{i}^*\cdot \hat{\mathbf{A}} \cdot \mathbf{j}.

    Parameters
    ----------
    i : FlattenedMeshVector
        Flattened mesh vector of shape `(3*N,)`.
    j : FlattenedMeshVector
        Flattened mesh vector of shape `(3*N,)`.
    A : scipy.sparse.linalg.LinearOperator
        LinearOperator of shape `(3*N, 3*N)`.
    dV : MeshScalar
        Volume elements of the mesh.

    Returns
    -------
    float
    """
    rhs = A.dot(j)

    temp = flattened_mesh_vec_scalar_product2d(np.conjugate(i), rhs)

    elem = (np.sum(temp * dV))
    return elem
