import numpy as np
import pandas as pd
from scipy.constants import mu_0

from ..core.fem_core.cythoncore import rotation_matrix_py
from ..core.operators import cross_operator
from ..helpers.io import read_mode_from_vtk, get_num_modes_from_dataframe
from ..helpers.math import flattened_mesh_vec_scalar_product2d, flattened_mesh_vec_scalar_product


def get_ellipticy_factor(mode_loc, Lambda, dA, return_norm=False) -> float:
    """
    Obtains the ellipticity factor of  given mode profile. This factor is important to calculate modal linewidths.


    Parameters
    ----------
    mode_loc : MeshVector
        Mesh vector of shape `(N,2)`. Mode profile in local frame of reference.
    Lambda : csr_matrix
        Sparse matrix of size `(2*N,2*N)` containing the cross product with the equilibrium magnetization in the local
        frame of reference.
    dA : MeshScalar
        Volume/area elements of mesh.
    return_norm : bool
        Apart from ellipticity coefficient, also return normalization factor of mode (multiplied with volume)

    Returns
    -------
    ellipticity_factor : float
        Ellipticity factor of respective mode.
    norm_times_volume : float
        Normalization factor of the multiplied with the volume of the sample (is only return if ``return_norm=True``).
    """

    avrg_mag_times_V = (flattened_mesh_vec_scalar_product2d(mode_loc.conjugate(), mode_loc) * dA).sum()
    norm_times_volume = 1j * (
            flattened_mesh_vec_scalar_product2d(mode_loc.conjugate(), Lambda.dot(mode_loc)) * dA).sum()
    ellipticity_factor = (avrg_mag_times_V / norm_times_volume).real
    return (ellipticity_factor, norm_times_volume) if return_norm else ellipticity_factor


def calculate_linewidhts(sample, exp_name, dispersion, k_calc):
    """Internal function that calculates linewidhts from existing mode profiles."""

    mag = sample.mag.to_flattened()
    rotation = rotation_matrix_py(mag)
    rotation = rotation[:2 * sample.nx, :]
    Lambda = cross_operator(sample.nx)[:2 * sample.nx, :2 * sample.nx]

    modes_path = "./{}/{}/mode-profiles".format(sample.name, exp_name)

    num_modes = get_num_modes_from_dataframe(dispersion)

    if k_calc is not None:
        dispersion = dispersion.loc[dispersion["k (rad/m)"] == k_calc]

    for index, row in dispersion.iterrows():
        k = row["k (rad/m)"]

        kdtet = k * sample.scale

        for i in range(num_modes):
            freq = row["f{} (GHz)".format(i)]

            if not isinstance(freq, float):
                continue

            fname = "{}/mode_k{}radperum_{:03d}.vtk".format(modes_path, k * 1e-6, i)
            # print(freq)

            # print(omega_nu)
            try:
                mode_lab = read_mode_from_vtk(fname, as_flattened=True)
                mode_loc = rotation.dot(mode_lab)

                # calculate ellipticity used for linewidth
                epsilon_nu = get_ellipticy_factor(mode_loc, Lambda, sample.dA)

                # calculate damping of mode
                Gamma_nu_over_two_pi = sample.alpha * epsilon_nu * freq

            except:
                print("Mode {} at k = {} rad/µm could not be found.".format(i, k * 1e-6))
                Gamma_nu_over_two_pi = np.nan
            dispersion.at[index, f"Gamma{i}/2pi (GHz)"] = Gamma_nu_over_two_pi

    return dispersion





def calculate_absorption(sample, exp_name, dispersion, antenna, fmin, fmax, Nf, k_calc):
    mag = sample.mag.T.flatten()
    rotation = rotation_matrix_py(mag)
    rotation = rotation[:2 * sample.nx, :]
    Lambda = cross_operator(sample.nx)[:2 * sample.nx, :2 * sample.nx]
    f_ = np.linspace(fmin, fmax, Nf)
    df = f_[1] - f_[0]
    omega_ = 2 * np.pi * f_
    chi_ = []

    h_RF = antenna.get_microwave_field_function(sample.xyz)
    modes_path = "./{}/{}/mode-profiles".format(sample.name, exp_name)

    num_modes = get_num_modes_from_dataframe(dispersion)

    if k_calc is not None:
        dispersion = dispersion.loc[dispersion["k (rad/m)"] == k_calc]

    for index, row in dispersion.iterrows():
        k = row["k (rad/m)"]

        kdtet = k * sample.scale

        chi = np.zeros_like(omega_) + 0j
        h_RF_k = h_RF(kdtet)

        for i in range(num_modes):
            freq = row["f{} (GHz)".format(i)]

            if not isinstance(freq, float):
                continue

            if np.isnan(freq):
                continue

            fname = "{}/mode_k{}radperum_{:03d}.vtk".format(modes_path, k * 1e-6, i)
            # print(freq)

            omega_nu = 2 * np.pi * freq
            # print(omega_nu)
            try:
                mode_lab = read_mode_from_vtk(fname, as_flattened=True)
            except:
                print("Mode {} at k = {} rad/µm could not be found.".format(i, k * 1e-6))
                continue

            mode_loc = rotation.dot(mode_lab)

            # calculate ellipticity used for linewidth
            epsilon_nu, A_nu_times_V = get_ellipticy_factor(mode_loc, Lambda, sample.dA, return_norm=True)

            chi_nu = np.abs(
                (flattened_mesh_vec_scalar_product(mode_lab.conjugate(), h_RF_k) * sample.dA).sum() / A_nu_times_V) ** 2

            chi += chi_nu * sample.gamma * mu_0 * sample.Msat_avrg / (
                    (omega_nu - omega_) - 1j * sample.alpha * omega_nu * epsilon_nu)

        chi_.append(chi)

    chi_ = np.array(chi_)

    if k_calc is None:
        FF, KK = np.meshgrid(f_, dispersion["k (rad/m)"].values)
        return chi_.imag, KK, FF
    if k_calc is not None:
        chi_ = chi_.flatten()
        chi_dataframe = pd.DataFrame({"RF frequency (GHz)": f_, "Re(chi)": chi_.real, "Im(chi)": chi_.imag,
                                      "Abs(chi)": np.sqrt(chi_.real ** 2 + chi_.imag ** 2), "Arg(chi)": np.angle(chi_)})

        return chi_dataframe
