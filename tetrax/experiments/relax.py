import time

import numpy as np
from scipy.constants import mu_0
from scipy.optimize import minimize

from ..core.mesh import FlattenedAFMMeshVectorSpherical, FlattenedMeshVectorSpherical, make_flattened_AFM, \
    flattened_spherical_to_cartesian_afm, flattened_spherical_to_cartesian_fm
from ..helpers.math import flattened_mesh_vec_scalar_product, flattened_AFMmesh_vec_scalar_product


def not_implemented_minimizer(sample, Bext, tol_cg, return_last=False, continue_with_least_squares=False):
    print("This energy minimizer will be implemented in a future release.")


def energy_per_length_spherical(mag_spherical, nx, dA, h_ext, N, N_cub, convert_to_cartesian, scalar_product):
    mag = convert_to_cartesian(mag_spherical, nx)

    # print(mag.shape)
    # print((h_ext - 0.5 * N.dot(mag)).shape)
    # energy_per_volume = -tetramagvec_scalar_product(mag, h_ext + 0.5*N_cub.nonlinear_field(mag) - 0.5 * N.dot(mag)).real
    energy_per_volume = -scalar_product(mag, h_ext - 0.5 * N.dot(mag) + 0.5 * N_cub.nonlinear_field(
        mag)).real

    return (energy_per_volume * dA).sum()


def jac_spherical_fm(mag_spherical, nx, dA, h_ext, N, N_cub, convert_to_cartesian, scalar_product):
    # TODO include faster scalar product!
    # e_per_V = -tetramagvec_scalar_product(mag, h_ext)

    # unpack
    theta = mag_spherical[:nx]
    phi = mag_spherical[-nx:]

    # convert to carthesian coordinates
    st = np.sin(theta)
    sp = np.sin(phi)
    ct = np.cos(theta)
    cp = np.cos(phi)

    mag = convert_to_cartesian(mag_spherical, nx)

    h_eff = h_ext - N.dot(mag).real + N_cub.nonlinear_field(mag)
    # TODO include again!

    h_x = h_eff[:nx]
    h_y = h_eff[nx:-nx]
    h_z = h_eff[-nx:]

    dm00 = cp * ct
    dm01 = -sp * st
    dm10 = sp * ct
    dm11 = cp * st
    dm20 = -st
    dm21 = 0.0

    dw_theta = -1 * (h_x * dm00 + h_y * dm10 + h_z * dm20)
    dw_phi = -1 * (h_x * dm01 + h_y * dm11 + h_z * dm21)

    return np.concatenate((dw_theta, dw_phi))


def jac_spherical_afm(mag_spherical, nx, dA, h_ext, N, N_cub, convert_to_cartesian, scalar_product):
    # TODO include faster scalar product!
    # e_per_V = -tetramagvec_scalar_product(mag, h_ext)

    # unpack
    theta1 = mag_spherical[:nx]
    phi1 = mag_spherical[nx:2 * nx]

    theta2 = mag_spherical[2 * nx:3 * nx]
    phi2 = mag_spherical[3 * nx:4 * nx]

    st1 = np.sin(theta1)
    sp1 = np.sin(phi1)
    ct1 = np.cos(theta1)
    cp1 = np.cos(phi1)

    st2 = np.sin(theta2)
    sp2 = np.sin(phi2)
    ct2 = np.cos(theta2)
    cp2 = np.cos(phi2)

    mag = convert_to_cartesian(mag_spherical, nx)

    h_eff = h_ext - N.dot(mag).real + N_cub.nonlinear_field(mag)
    # TODO include again!

    h_x1 = h_eff[:nx]
    h_y1 = h_eff[nx:2 * nx]
    h_z1 = h_eff[2 * nx:3 * nx]
    h_x2 = h_eff[3 * nx:4 * nx]
    h_y2 = h_eff[4 * nx:5 * nx]
    h_z2 = h_eff[5 * nx:6 * nx]

    dm001 = cp1 * ct1
    dm011 = -sp1 * st1
    dm101 = sp1 * ct1
    dm111 = cp1 * st1
    dm201 = -st1
    dm211 = 0.0

    dm002 = cp2 * ct2
    dm012 = -sp2 * st2
    dm102 = sp2 * ct2
    dm112 = cp2 * st2
    dm202 = -st2
    dm212 = 0.0

    dw_theta1 = -1 * (h_x1 * dm001 + h_y1 * dm101 + h_z1 * dm201)
    dw_phi1 = -1 * (h_x1 * dm011 + h_y1 * dm111 + h_z1 * dm211)

    dw_theta2 = -1 * (h_x2 * dm002 + h_y2 * dm102 + h_z2 * dm202)
    dw_phi2 = -1 * (h_x2 * dm012 + h_y2 * dm112 + h_z2 * dm212)

    return np.concatenate((dw_theta1, dw_phi1, dw_theta2, dw_phi2))


def minimize_gibbs_free_energy(sample, Bext, tol_cg, return_last=False, continue_with_least_squares=False,
                               verbose=True):
    # specify which flattened-mesh-vector type to use for spherical coordinates (FM or AFM)

    is_AFM = sample._magnetic_order == "AFM"
    FlattendVectorFMorAFM = FlattenedAFMMeshVectorSpherical if is_AFM else FlattenedMeshVectorSpherical
    convert_to_cartesian = flattened_spherical_to_cartesian_afm if is_AFM else flattened_spherical_to_cartesian_fm

    scalar_product = flattened_AFMmesh_vec_scalar_product if is_AFM else flattened_mesh_vec_scalar_product
    jac_spherical = jac_spherical_afm if is_AFM else jac_spherical_fm

    # read_tmv_from_project("area.bin", conf)
    nx = sample.nx
    # area associated with each node
    dA = sample.dA
    noise = 1e-10

    if sample._magnetic_order == "FM":
        m_ini = sample._mag.to_flattened()
        h_ext = Bext.T.flatten() / (np.tile(sample.Msat,3) * mu_0)

    elif sample._magnetic_order == "AFM":
        m_ini = make_flattened_AFM(sample._mag1, sample._mag2)
        h_ext = make_flattened_AFM(Bext, Bext) / (sample.Msat * mu_0)

    m_ini_spherical = m_ini.to_spherical()
    m_ini_spherical += noise * np.random.randn(m_ini_spherical.shape[0])

    sample.N_dip.set_k(0)
    sample.N_exc.set_k(0)
    sample.N_DMI.set_k(0)
    sample.N_iDMI.set_k(0)

    N_tot = sample.N_dip + sample.N_exc + sample.N_uni + sample.N_DMI + sample.N_iDMI + sample.N_uDMI
#    N_tot = sample.N_exc + sample.N_uni + sample.N_DMI + sample.N_iDMI + sample.N_uDMI
    N_cub = sample.N_cub

#    fac = (sample.Msat_avrg * sample.scale) ** 2 * mu_0
    fac = mu_0 * sample.scale ** 2
    # tol_cg = 1e-12

    print_current_spherical = (lambda x: print(
        "Current energy length density: {:.15e} J/m  mx = {:.2f}  my = {:.2f}  mz = {:.2f}\r".format(
            (energy_per_length_spherical(x, nx, dA, h_ext, N_tot, N_cub, convert_to_cartesian, scalar_product) * fac),
            (np.sin(x[:nx]) * np.cos(x[-nx:])).mean(),
            (np.sin(x[:nx]) * np.sin(x[-nx:])).mean(),
            (np.cos(x[:nx])).mean()), end="")) if not is_AFM else (lambda x: print(
        "Current energy length density: {:.15e} J/m mx1 = {:.2f}  my1 = {:.2f}  mz1 = {:.2f}  mx2 = {:.2f}  my2 = {:.2f}  mz2 = {:.2f}\r".format(
            (energy_per_length_spherical(x, nx, dA, h_ext, N_tot, N_cub, convert_to_cartesian, scalar_product) * fac),
            (np.sin(x[:nx]) * np.cos(x[nx:2 * nx])).mean(),
            (np.sin(x[:nx]) * np.sin(x[nx:2 * nx])).mean(),
            (np.cos(x[:nx])).mean(),
            (np.sin(x[2 * nx:3 * nx]) * np.cos(x[3 * nx:4 * nx])).mean(),
            (np.sin(x[2 * nx:3 * nx]) * np.sin(x[3 * nx:4 * nx])).mean(),
            (np.cos(x[2 * nx:3 * nx])).mean()
        ), end=""))

    starting_method = "L-BFGS-B"
    t_start = time.time()
    if verbose:
        print(f"Minimizing in using '{starting_method}' (tolerance {tol_cg}) ...")

        result = minimize(energy_per_length_spherical, m_ini_spherical,
                          (nx, dA, h_ext, N_tot, N_cub, convert_to_cartesian, scalar_product),
                          jac=jac_spherical,
                          callback=print_current_spherical, method=starting_method,
                          options={"ftol": tol_cg, "gtol": tol_cg})
    else:
        result = minimize(energy_per_length_spherical, m_ini_spherical,
                          (nx, dA, h_ext, N_tot, N_cub, convert_to_cartesian, scalar_product),
                          jac=jac_spherical, method=starting_method,
                          options={"ftol": tol_cg, "gtol": tol_cg})
    t_end = time.time()
    if result.success:

        # convert to cartesian coordinates
        m_final = FlattendVectorFMorAFM(result.x).to_cartesian()

        if verbose:
            print("\nSuccess!\n")
        return m_final, True
    else:
        if not continue_with_least_squares:
            print(f"\nRelaxation with {starting_method} method was not succesful.\n")

            if not return_last:
                return m_ini, False
            else:
                # convert to cartesian coordinates
                m_final = FlattendVectorFMorAFM(result.x).to_cartesian()
                print("Returning last iteration step.")
                return m_final, False

        if continue_with_least_squares:
            if verbose:
                print(f"\nRelaxation with {starting_method} method was not succesful.\n")
                print(f"Minimizing in using SLSQP method (tolerance {tol_cg}) ...")
                result = minimize(energy_per_length_spherical, result.x,
                                  (nx, dA, h_ext, N_tot, N_cub, convert_to_cartesian, scalar_product),
                                  jac=jac_spherical,
                                  callback=print_current_spherical, method='SLSQP', options={"ftol": tol_cg})
            else:
                result = minimize(energy_per_length_spherical, result.x,
                                  (nx, dA, h_ext, N_tot, N_cub, convert_to_cartesian, scalar_product),
                                  jac=jac_spherical, method='SLSQP', options={"ftol": tol_cg})
            if result.success:
                # convert to cartesian coordinates
                m_final = FlattendVectorFMorAFM(result.x).to_cartesian()

                if verbose:
                    print("\nSuccess!\n")
                return m_final, True

            else:
                print(f"\nRelaxation with SLSQP method method was not successful.\n")

                if return_last:
                    m_final = FlattendVectorFMorAFM(result.x).to_cartesian()
                    print("\nReturning last iteration step.\n")
                    return m_final, False
                else:
                    return m_ini, False
