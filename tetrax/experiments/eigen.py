import multiprocessing as mp
import os
from functools import partial

import meshio
import numpy as np
import pandas as pd
import tqdm
from scipy.constants import mu_0
from scipy.sparse import csc_matrix, csr_matrix, bmat
from scipy.sparse.linalg import spilu, eigs

from ..core.fem_core.cythoncore import rotation_matrix_py
from ..core.mesh import make_flattened_AFM, FlattenedAFMMeshVector, FlattenedMeshVector
from ..core.operators import cross_operator, TotalDynMat, SuperLUInv, InvTotalDynMat
from ..helpers.math import flattened_mesh_vec_scalar_product, diag_matrix_from_vec, flattened_mesh_vec_scalar_product2d, \
    matrix_elem, flattened_AFMmesh_vec_scalar_product, flattened_AFMmesh_vec_scalar_product_separate
from ..experiments.calculate_absorption import get_ellipticy_factor

from sys import platform

def not_implemented_eigensolver(sample, Bext, exp_name, kmin=-40e6, kmax=40e6, Nk=81, num_modes=20, k=None,
                                no_dip=False, num_cpus=1, save_modes=False, save_local=False, save_magphase=False):
    print("This eigensolver will be implemented in a future release.")


def test_rotation(mag, rotation, nx):
    """
    Test of rotation matrix and equilibrium vector match, i.e. rotation rotates mag into the z direction.
    """
    test_ez = rotation.dot(mag)
    e_z = np.concatenate([np.zeros(2 * nx), np.ones(nx)])
    if not np.allclose(test_ez, e_z):
        raise ValueError("Rotation matrix and equilibrium files don't belong together. "
                         "Please create a new rotation matrix.")


def calculate_normal_modes(sample, Bext, exp_name, kmin=-40e6, kmax=40e6, Nk=81, num_modes=20, k=None, no_dip=False,
                           h0_dip=False,
                           num_cpus=1, save_modes=False, save_local=False, save_magphase=False,
                           perturbed_dispersion=False, save_stiffness_fields=False, verbose=True, linewidhts=False):
    """
        2D version
    """

    modes_path = "./{}/{}/mode-profiles".format(sample.name, exp_name)
    if not os.path.exists(modes_path):
        os.makedirs(modes_path)

    dim = sample._dim
    is_AFM = sample._magnetic_order == "AFM"
    lattice_wise_projection = flattened_AFMmesh_vec_scalar_product_separate if is_AFM else flattened_mesh_vec_scalar_product

    # exchange operator
    N_exc = sample.N_exc
    N_dip = sample.N_dip
    N_uni = sample.N_uni
    N_DMI = sample.N_DMI
    N_iDMI = sample.N_iDMI
    N_uDMI = sample.N_uDMI
    N_cub = sample.N_cub
    N_iec = sample.N_iec

    N_iDMI.set_k(0)
    N_DMI.set_k(0)
    N_exc.set_k(0)
    N_dip.set_k(0)
    N_cub.make_sparse_mat_with_current_m0()

    mu0_cross = cross_operator(sample.nx)

    mag = make_flattened_AFM(sample.mag1, sample.mag2) if is_AFM else sample.mag.to_flattened()

    nx = sample.nx
    hext = make_flattened_AFM(Bext,Bext)/(mu_0 * sample.Msat) if is_AFM else Bext.to_flattened()/(mu_0 * np.tile(sample.Msat,3))
    hexc = -N_exc.dot(mag)
    hdip = -N_dip.dot(mag)
    hDMI = -N_DMI.dot(mag)
    hiDMI = -N_iDMI.dot(mag)
    hani = -N_uni.dot(mag)
    hcub = N_cub.nonlinear_field(mag)
    hiec = -N_iec.dot(mag)
    hudmi = -N_uDMI.dot(mag)

    # overwrite dipolar option in case of AFMs
    no_dip = True if is_AFM else no_dip
    if no_dip:
        heff = (hexc + hext + hani + hDMI + hiDMI + hcub + hiec + hudmi)  * np.tile(sample.Msat ** 2,3) if not is_AFM else \
            (hexc + hext + hani + hDMI + hiDMI + hcub + hiec + hudmi)
        if h0_dip and not is_AFM:
            heff += hdip * sample.Msat_avrg ** 2
    else:
        heff = (hexc + hext + hani + hDMI + hiDMI + hcub + hiec + hudmi) * np.tile(sample.Msat ** 2,3) \
               + hdip * sample.Msat_avrg ** 2

    h0 = lattice_wise_projection(heff, mag)

    h0_avrg = np.sum(h0 * sample.dA) / np.sum(sample.dA) if not is_AFM else np.nan

    mu0_cross = cross_operator(sample.nx)[:2 * nx,  :2 * nx]

    if is_AFM:
        Lambda = bmat([[mu0_cross, None], [None, mu0_cross]])

        rotation1 = rotation_matrix_py(sample.mag1.to_flattened())
        test_rotation(sample.mag1.to_flattened(), rotation1, sample.nx)
        rotation1 = rotation1[:2 * nx, :]

        rotation2 = rotation_matrix_py(sample.mag2.to_flattened())
        test_rotation(sample.mag2.to_flattened(), rotation2, sample.nx)
        rotation2 = rotation2[:2 * nx, :]

        rotation = bmat([[rotation1, None], [None, rotation2]])
        rotation_T = rotation.transpose()

    else:
        Lambda = mu0_cross

        rotation = rotation_matrix_py(mag)
        test_rotation(mag, rotation, sample.nx)
        rotation = rotation[:2 * nx, :]
        rotation_T = rotation.transpose()


    # create k and convert to mesh units
    if isinstance(k, float) or isinstance(k, int):
        k_ = [k * sample.scale]
    elif np.all(k) == None:
        k_ = np.linspace(kmin, kmax, Nk) * sample.scale
    else:
        k_ = k * sample.scale

    # create k and convert to mesh units
    #    if k == None:
    #        k_ = np.linspace(kmin, kmax, Nk) * sample.scale
    #    else:
    #        k_ = [k * sample.scale]

    length_v0 = 4*nx if is_AFM else 2*nx
    v0 = np.full(length_v0, 1 + 1j, dtype=complex)

    D_tot = None if is_AFM or dim == 3 else TotalDynMat(N_exc.sparse_mat, N_dip, 1j * mu0_cross[:2 * nx, :2 * nx].dot(rotation), rotation_T)
#    D_tot = None if is_AFM else TotalDynMat(N_exc.sparse_mat, N_dip, 1j * mu0_cross[:2 * nx, :2 * nx].dot(rotation), rotation_T)

    bndint_order = 6

    modes_per_k_partial = partial(modes_per_k, N_exc, N_uni, N_cub, N_DMI, N_iDMI, N_uDMI, N_iec, h0, h0_avrg, Lambda, rotation,
                                  rotation_T,
                                  bndint_order, D_tot, v0, no_dip, sample.scale, sample.Msat_avrg, sample.gamma, sample,
                                  num_modes, modes_path, save_modes, save_local, save_magphase, perturbed_dispersion,
                                  save_stiffness_fields, is_AFM, linewidhts)

    if num_cpus == 0 or num_cpus < -1:
        num_cpus = 1
    if num_cpus == -1:
        num_cpus = mp.cpu_count()
    if num_cpus > mp.cpu_count():
        num_cpus = mp.cpu_count()

    if num_cpus == 1:
        if mp.cpu_count() > 1:
            pass
    else:
        pass

    if platform == "win32":
        with mp.Pool(processes=num_cpus) as p:
            res_list = []
            with tqdm.tqdm(total=len(k_), disable=(not verbose)) as pbar:
                for i, res in enumerate(p.imap_unordered(modes_per_k_partial, k_)):
                    res_list.append(res)
                    pbar.update()
    else:
        with mp.get_context('fork').Pool(processes=num_cpus) as p:
            res_list = []
            with tqdm.tqdm(total=len(k_), disable=(not verbose)) as pbar:
                for i, res in enumerate(p.imap_unordered(modes_per_k_partial, k_)):
                    res_list.append(res)
                    pbar.update()
    df_freqs = pd.concat(res_list, sort=False).sort_values("k (rad/m)").reset_index(drop=True)  # .fillna("nan")

    return df_freqs


def modes_per_k(N_exc, N_uni, N_cub, N_DMI, N_iDMI, N_uDMI, N_iec, h0, h0_avrg, Lambda, rotation, rotation_T,
                bndint_order, D_tot, v0, no_dip, scale, Msat, gamma, sample,
                num_modes, modes_path, save_modes, save_local, save_magphase, perturbed_dispersion,
                save_stiffness_fields, is_AFM, linewidhts, k):

    nx = sample.nx
    N_exc.set_k(k)
    N_DMI.set_k(k)
    N_iDMI.set_k(k)

    N_nodip_k_mat = N_exc.sparse_mat + N_uni.sparse_mat + N_DMI.sparse_mat + N_iDMI.sparse_mat + N_cub.sparse_mat \
                    + N_iec.sparse_mat + N_uDMI.sparse_mat
    h0_operator = diag_matrix_from_vec(np.concatenate((np.tile(h0[:nx], 3), np.tile(h0[nx:], 3)))) if is_AFM else diag_matrix_from_vec(np.tile(h0, 3))
    N_nodip_k_mat += h0_operator # * diag_matrix_from_vec(np.tile(sample.Msat ** 2, 3))
    dyn_mat_k = 1j * Lambda.dot(rotation).dot(N_nodip_k_mat).dot(rotation_T)

    dyn_mat_k_csc = csc_matrix(dyn_mat_k) / Msat ** 2 if not is_AFM else csc_matrix(dyn_mat_k)

    modes_calculated = False

    is_oneD = True if (sample._dim == 1) else False
    # just to be sure
    no_dip = True if is_AFM else no_dip
#    sample._no_dip = True if is_AFM else no_dip

    if no_dip:
        freq, eigenvectors = eigs(dyn_mat_k_csc, v0=v0, which="LM", k=num_modes * 2, tol=1e-3, sigma=0)
        ef = freq.real * mu_0 * Msat * gamma / 2 / np.pi * 1e-9 #if not is_AFM else freq.real * mu_0 * Msat * gamma / 2 / np.pi * 1e-9
        modes_calculated = True

    else:
        D_tot.set_k(k, N_nodip_k_mat, no_dip=False)
        try:
            preconditioner = SuperLUInv(spilu(csc_matrix(dyn_mat_k)))
        except:
            print("It was not possible to calculate the preconditioner at k = {} rad/m".format(k_m=k / sample.scale))
            quit()

        D_tot_inv = InvTotalDynMat(D_tot, k, preconditioner=preconditioner)
        try:
            tol = 1e-7 if no_dip else 1e-3
            freq, eigenvectors = eigs(D_tot, v0=v0, which="LM", k=num_modes * 2, tol=tol, sigma=0, OPinv=D_tot_inv)
    #            ef = freq * mu_0 * Msat * gamma / 2 / np.pi * 1e-9
    #            ef = freq.real * mu_0 * Msat * gamma / 2 / np.pi * 1e-9
            ef = freq.real * mu_0 * Msat * gamma / 2 / np.pi * 1e-9
            modes_calculated = True
        except:
            k_m = k / sample.scale
                # error_k = this_error.k
                # error_iter = this_error.after_iter
                # error_success = this_error.already_sucess
            print(f"Could not calculate modes at k = {k_m} rad/m. Maybe your equilibrium state is metastable. "
                      f"Inserting NaN as frequencies.")
            ef = np.empty(num_modes) * np.nan
            eigenvectors = np.zeros((2 * D_tot.nx, num_modes))

    ef_org = ef.copy()
    if modes_calculated:
        # sort out all negative frequency modes
        gtzero = ef.real >= 0
        eigenvectors = eigenvectors[:, gtzero]
        ef = ef[gtzero]

        # now sort modes by ascending frequency
        sortindices = np.argsort(ef.real)
        ef = ef[sortindices]
        eigenvectors = eigenvectors[:, sortindices]

    k_m = k / scale

    cells = sample.mesh.cells
    xyz = sample.xyz

    if save_modes and modes_calculated:

        for i in range(eigenvectors.shape[1]):
            ev = eigenvectors[:, i]

            # rotate eigenvector back to lab system
            eta = FlattenedAFMMeshVector(rotation_T.dot(ev)) if is_AFM else FlattenedMeshVector(rotation_T.dot(ev))

            if is_AFM:
                eta1, eta2 = eta.to_two_unflattened()

                save_dict = {"Re(m1)": eta1.real,
                             "Re(m2)": eta2.real,
                             "Im(m1)": eta1.imag,
                             "Im(m2)": eta2.imag,
                             }
            else:
                eta = eta.to_unflattened()
                save_dict = {"Re(m)": eta.real,
                             "Im(m)": eta.imag,
                             }


            if save_local:
                if is_AFM:
                    eta1_u = ev[:nx]
                    eta1_v = ev[nx:2*nx]
                    eta2_u = ev[2*nx:3*nx]
                    eta2_v = ev[3*nx:4*nx]


                    save_dict["Re(m1_u)"] = eta1_u.real
                    save_dict["Im(m1_u)"] = eta1_u.imag
                    save_dict["Re(m1_v)"] = eta1_v.real
                    save_dict["Im(m1_v)"] = eta1_v.imag

                    save_dict["Re(m2_u)"] = eta2_u.real
                    save_dict["Im(m2_u)"] = eta2_u.imag
                    save_dict["Re(m2_v)"] = eta2_v.real
                    save_dict["Im(m2_v)"] = eta2_v.imag

                    if save_magphase:
                        save_dict["Abs(m1_u)"] = np.abs(eta1_u)
                        save_dict["Abs(m1_v)"] = np.abs(eta1_v)
                        save_dict["Arg(m1_u)"] = np.angle(eta1_u)
                        save_dict["Arg(m1_v)"] = np.angle(eta1_v)

                        save_dict["Abs(m2_u)"] = np.abs(eta2_u)
                        save_dict["Abs(m2_v)"] = np.abs(eta2_v)
                        save_dict["Arg(m2_u)"] = np.angle(eta2_u)
                        save_dict["Arg(m2_v)"] = np.angle(eta2_v)


                else:
                    eta_u = ev[:nx]
                    eta_v = ev[nx:]

                    save_dict["Re(m_u)"] = eta_u.real
                    save_dict["Im(m_u)"] = eta_u.imag
                    save_dict["Re(m_v)"] = eta_v.real
                    save_dict["Im(m_v)"] = eta_v.imag

                    if save_magphase:
                        save_dict["Abs(m_u)"] = np.abs(eta_u)
                        save_dict["Abs(m_v)"] = np.abs(eta_v)
                        save_dict["Arg(m_u)"] = np.angle(eta_u)
                        save_dict["Arg(m_v)"] = np.angle(eta_v)

            if save_magphase:
                if is_AFM:
                    save_dict["Abs(m1)"] = np.abs(eta1)
                    save_dict["Arg(m1)"] = np.angle(eta1)
                    save_dict["Abs(m2)"] = np.abs(eta2)
                    save_dict["Arg(m2)"] = np.angle(eta2)

                else:
                    save_dict["Abs(m)"] = np.abs(eta)
                    save_dict["Arg(m)"] = np.angle(eta)

            meshio.write_points_cells(filename="{}/mode_k{}radperum_{:03d}.vtk".format(modes_path, k_m * 1e-6, i),
                                      points=xyz,
                                      cells=cells,
                                      point_data=save_dict
                                      )

    else:
        pass

    df_k = pd.DataFrame(columns=["k (rad/m)"] + ["f" + str(j) + " (GHz)" for j in range(len(ef))])
    df_k.loc[0] = [k_m] + ef.tolist()

#    df_k.loc[0] = [k_m] + np.real(ef).tolist()

    if perturbed_dispersion:
        # for details of calcultions here, see Phys. Rev. B 104, 174414
        dA = sample.dA
        no_dip = False
        D_tot.set_k(k, csr_matrix((3 * nx, 3 * nx)),no_dip)

        operator_dict = {
            "exc": 1j * Lambda.dot(rotation).dot(N_exc.sparse_mat).dot(rotation_T),
            "uni": 1j * Lambda.dot(rotation).dot(N_uni.sparse_mat).dot(rotation_T),
            "cub": 1j * Lambda.dot(rotation).dot(N_cub.sparse_mat).dot(rotation_T),
            "bDMI": 1j * Lambda.dot(rotation).dot(N_DMI.sparse_mat).dot(rotation_T),
            "iDMI": 1j * Lambda.dot(rotation).dot(N_iDMI.sparse_mat).dot(rotation_T),
            "dip": D_tot
        }

        for i in range(eigenvectors.shape[1]):
            Im_N21_ = []
            Re_N21_ = []
            N11_ = []
            N22_ = []
            mode_local = eigenvectors[:, i]

            # calculate orthonormal basis from mode profiles
            s1 = np.concatenate([mode_local[:nx], np.zeros(nx)])
            s2 = np.concatenate([np.zeros(nx), mode_local[nx:]])
            s1 /= np.sqrt(np.sum(flattened_mesh_vec_scalar_product2d(np.conjugate(s1), s1) * dA))
            s2 /= np.sqrt(np.sum(flattened_mesh_vec_scalar_product2d(np.conjugate(s2), s2) * dA))

            for name in operator_dict:

                # in Phys. Rev. B 104, 174414, this is D
                partial_dynmat = operator_dict[name]

                # Calculate matrix elements of diagonal part of partial dynamic matrices
                C11 = matrix_elem(s1, s1, partial_dynmat, dA)
                C22 = matrix_elem(s2, s2, partial_dynmat, dA)
                C12 = matrix_elem(s1, s2, partial_dynmat, dA)
                C21 = matrix_elem(s2, s1, partial_dynmat, dA)

                # convert to stiffness fields
                Im_N21 = 0.5 * (C11 + C22).real
                Re_N21 = (1j * 0.5 * (C11 - C22)).real
                N11 = (C21).real
                N22 = (C12).real

                if name == "dip":
                    N11 *= sample.Msat_avrg ** 2
                    N22 *= sample.Msat_avrg ** 2
                    Im_N21 *= sample.Msat_avrg ** 2
                    Re_N21 *= sample.Msat_avrg ** 2

                Im_N21_.append(Im_N21)
                Re_N21_.append(Re_N21)
                N11_.append(N11)
                N22_.append(N22)

                if save_stiffness_fields:
                    # quit()
                    df_k = pd.concat([df_k, pd.DataFrame({
                        f"Im(N21_{name})_{i}": [Im_N21],
                        f"Re(N21_{name})_{i}": [Re_N21],
                        f"N11_{name}_{i}": [N11],
                        f"N22_{name}_{i}": [N22],
                    })], axis=1)

            # print(Re_N12)
            omega_i = np.sum(Im_N21_) + np.sqrt(
                (np.sum(N11_) + h0_avrg) * (np.sum(N22_) + h0_avrg) - np.sum(Re_N21_) ** 2)

            f_k = omega_i.real * mu_0 * sample.Msat_avrg * sample.gamma / 2 / np.pi * 1e-9 / sample.Msat_avrg ** 2
            df_k[f"f_pert_{i} (GHz)"] = f_k

    if linewidhts and not is_AFM:
        for i in range(eigenvectors.shape[1]):
            mode_local = eigenvectors[:, i]
            epsilon_nu = get_ellipticy_factor(mode_local, Lambda, sample.dA)
            df_k[f"Gamma{i}/2pi (GHz)"] = sample.alpha * epsilon_nu * df_k[f"f{i} (GHz)"]


    return df_k

