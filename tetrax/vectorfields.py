import numpy as np


def helical(xyz, Theta, chi):
    """Helical vector field with given angle :math:`\Theta` and circularity :math:`\chi`.

    The field is calculated according to

    .. math:: \mathbf{v} = \chi\sin(\Theta) \mathbf{e}_\phi + \cos(\Theta)\mathbf{e}_z

    which, in cartesian basis, is

    .. math:: \mathbf{v} = -\chi\sin(\Theta)\sin(\phi) \mathbf{e}_x + \chi\sin(\Theta)\cos(\phi) \mathbf{e}_y + \cos(\Theta)\mathbf{e}_z

    with :math:`\phi` being the azimuthal angle in a cylindrical coordinate system.

    Parameters
    ----------
    xyz : MeshVector
        Coordinates on a mesh.
    Theta : float
        Angle :math:`\Theta` of the helical field with the :math:`z` axis (given in `degree`).
    chi : int
        Circularity :math:`\chi` or sense of rotation around the :math:`z` axis (either ``1`` or ``-1``).

    Returns
    -------
    MeshVector
        Vector field :math:`\mathbf{v}` defined at each node of the mesh.

    See Also
    --------
    radial, circular_vortex, radial_vortex
    """
    Theta = Theta / 180 * np.pi
    x = xyz.T[0]
    y = xyz.T[1]
    phi = np.arctan2(y, x)
    mx = chi * np.sin(Theta) * (-np.sin(phi))
    my = chi * np.sin(Theta) * (np.cos(phi))
    mz = np.cos(Theta) * np.ones_like(x)
    return np.array([mx, my, mz]).T


def bloch_wall(xyz, thickness=10, x0=0, domain_axis="y", domain_polarity=1, wall_polarity=1):
    r"""Domain wall of Bloch type perpendicular to the :math:`x` direction.

    The field is calculated according to

    .. math:: \mathbf{v} = \begin{cases} p_\mathrm{D} \cos(\theta)\mathbf{e}_y + p_\mathrm{DW} \sin(\theta)\mathbf{e}_z, & \text{domains polarized along } y\\
                                   p_\mathrm{DW} \sin(\theta) \mathbf{e}_y + p_\mathrm{D} \cos(\theta)\mathbf{e}_z, & \text{domains polarized along } z
                            \end{cases}

    with :math:`p_\mathrm{D}` and :math:`p_\mathrm{DW}` being the ``domain_polarity`` and the ``wall_polarity``,
    respectively. The domain-wall angle :math:`\theta = \theta(x)` is given by the Bloch wall solution

    .. math:: \theta(x) = 2 \arctan\left(e^{(x-x_0)/\Delta}\right)

    with :math:`\Delta` being the ``thickness`` and :math:`x_0 =` ``x0`` being the position of the wall.

    Parameters
    ----------
    xyz : MeshVector
        Coordinates on a mesh.
    thickness : float
        Thickness of the domain wall (default is 10).
    x0 : float
        Position of the domain wall along the :math:`x` direction (default is 0).
    domain_axis : {"y", "z"}
        Axis, along which the domains adjecent to the Bloch wall are oriented (default is "y").
    domain_polarity : {1, -1}
        Polarity of the `first` domain with respect to the ``domain_axis``.
    wall_polarity : {1, -1}
        Polarity of the domain wall.

    Returns
    -------
    MeshVector
        Vector field :math:`\mathbf{v}` defined at each node of the mesh.

    See Also
    --------
    neel_wall
    """
    x = xyz.x
    y = xyz.y
    theta = 2 * np.arctan(np.exp((x - x0) / thickness))
    mx = np.zeros_like(x)
    if domain_axis == "y":
        my = domain_polarity * np.cos(theta)
        mz = wall_polarity * np.sin(theta)
    elif domain_axis == "z":
        mz = domain_polarity * np.cos(theta)
        my = wall_polarity * np.sin(theta)
    else:
        raise ValueError("No valid domain axis has been supplied.")

    return np.array([mx, my, mz]).T


def neel_wall(xyz, thickness=10, x0=0, domain_axis="z", domain_polarity=1, wall_polarity=1):
    r"""Domain wall of Neel type perpendicular to the :math:`x` direction.

     This profile is simply being approximated as the profile of a Bloch wall (see :func:`bloch_wall`), however,
     with rotation of the vecto field perpendicular to the wall plane.

     Parameters
     ----------
     xyz : MeshVector
         Coordinates on a mesh.
     thickness : float
         Thickness of the domain wall (default is 10).
     x0 : float
         Position of the domain wall along the :math:`x` direction (default is 0).
     domain_axis : {"y", "z"}
         Axis, along which the domains adjecent to the Bloch wall are oriented (default is "z").
     domain_polarity : {1, -1}
         Polarity of the `first` domain with respect to the ``domain_axis``.
     wall_polarity : {1, -1}
         Polarity of the domain wall.

     Returns
     -------
     MeshVector
         Vector field :math:`\mathbf{v}` defined at each node of the mesh.

     See Also
     --------
     bloch_wall
     """
    x = xyz.x
    y = xyz.y
    theta = 2 * np.arctan(np.exp((x - x0) / thickness))
    my = np.zeros_like(x)

    if domain_axis == "y":
        mz = wall_polarity * np.sin(theta)
        mx = domain_polarity * np.cos(theta)
    elif domain_axis == "z":
        mx = wall_polarity * np.sin(theta)
        mz = domain_polarity * np.cos(theta)
    else:
        raise ValueError("No valid domain axis has been supplied.")

    return np.array([mx, my, mz]).T


def radial(xyz, p):
    r"""Radial vector field with given polarization :math:`p`.

        The field is calculated according to

        .. math:: \mathbf{v} = p \mathbf{e}_\rho.


        Parameters
        ----------
        xyz : MeshVector
            Coordinates on a mesh.
        p : int
            Polarization :math:`p` of the radial field (either ``1`` for outward or ``-1`` for inward).

        Returns
        -------
        MeshVector
            Vector field :math:`\mathbf{v}` defined at each node of the mesh.

        See Also
        --------
        helical, circular_vortex, radial_vortex
        """
    x = xyz.x
    y = xyz.y
    mx = p * x / np.sqrt(x ** 2 + y ** 2)
    my = p * y / np.sqrt(x ** 2 + y ** 2)
    mz = np.zeros_like(y)
    return np.array([mx, my, mz]).T


def radial_vortex(xyz, p):
    """Not implemented yet."""
    x = xyz.T[0]
    y = xyz.T[1]
    mx = p * x / np.sqrt(x ** 2 + y ** 2)
    my = p * y / np.sqrt(x ** 2 + y ** 2)
    mz = np.zeros_like(y)
    return np.array([mx, my, mz]).T


def homogeneous(xyz, theta, phi):
    """Homogeneous magnetization state with direction given by spherical angles :math:`\\theta` and :math:`\phi`.

    Parameters
    ----------
    xyz : MeshVector
        Coordinates on a mesh.
    theta : float
        Polar angle :math:`\\theta` given in `degree`.
    phi : float
        Azimuthal angle :math:`\phi` given in `degree`.

    Returns
    -------
    MeshVector
        Vector field :math:`\mathbf{v}` defined at each node of the mesh.

    Notes
    -----
    Setting a homogeneous magnetization on sample ``sample`` or in an experimental setup ``exp``, the same can also be achieved
    by giving triplets.

    >>> sample.mag = [0, 0, 1]

    or

    >>> exp.Bext = [0.1, 0, 0]
    """
    theta = theta / 180.0 * np.pi
    phi = phi / 180.0 * np.pi
    mx = np.sin(theta) * np.cos(phi)
    my = np.sin(theta) * np.sin(phi)
    mz = np.cos(theta)
    return np.full_like(xyz, [mx, my, mz])


def circular_vortex(xyz, chi, p, rho_c=10):
    r"""Vortex state with given circularity :math:`\chi` and core polarity :math:`p`.

    The field is calculated according to

    .. math:: \mathbf{v} = \chi\sin[\theta(\rho)] \mathbf{e}_\phi +\cos[\theta(\rho)]\mathbf{e}_z

    which, in cartesian basis, is

    .. math:: \mathbf{v} = -\chi\sin[\theta(\rho)]\sin(\phi) \mathbf{e}_x + \chi\sin[\theta(\rho)]\cos(\phi) \mathbf{e}_y + \cos[\theta(\rho)]\mathbf{e}_z

    with :math:`\phi` being the azimuthal angle and :math:`\rho` being the radius in a cylindrical coordinate system.
    Here, :math:`\theta(\rho)` is the angle of the vector field with the :math:`z` axis and is approximated by
    the Usov-Pechany ansatz

    .. math:: \theta(\rho) = \begin{cases} 2\arctan(\rho/\rho_c), & \rho \leq \rho_c \\
                                \pi/2, & \rho > \rho_c\end{cases}

    Parameters
    ----------
    xyz : MeshVector
        Coordinates on a mesh.
    chi : int
        Circularity :math:`\chi` or sense of rotation around the :math:`z` axis (either ``1`` or ``-1``).
    p : int
        Polarity :math:`p` of the vortex core along the :math:`z` axis (either ``1`` or ``-1``).

    Returns
    -------
    MeshVector
        Vector field :math:`\mathbf{v}` defined at each node of the mesh.

    See Also
    --------
    helical, radial_vortex

    References
    ----------
    .. [1] N.A. Usov and S.E. Peschany, "Magnetization curling in a fine cylindrical particle", `J. Magn. Magn. Mater. 118, L290 (1993) <https://doi.org/10.1016/0304-8853(93)90428-5>`_.


    """
    x = xyz.x
    y = xyz.y

    rho = np.sqrt(x ** 2 + y ** 2)
    phi = np.arctan2(y, x)

    # Usov Pechany ansatz
    theta = np.piecewise(rho, [rho <= rho_c, rho > rho_c],
                         [lambda rho: 2 * np.arctan(rho / rho_c), lambda rho: np.pi / 2])

    mx = chi * np.sin(theta) * (-np.sin(phi))
    my = chi * np.sin(theta) * (np.cos(phi))
    mz = p * np.cos(theta) * np.ones_like(x)
    return np.array([mx, my, mz]).T

# def onion_tube_axial(xyz, pol_y=1, pol_n=1, pol_s=1, delta=5):
#     """
#     Onion state with domain wall along y at x = 0. The domain walls rotating on a cylinder (not in radial direction).
#
#     pol_y : polarization of state in y direction (1 or -1)
#     delta : thickness of domain walls (default is 5)
#     pol_n : polarity (z) of domain wall in y > 0 region
#     pol_s : polarity (z) of domain wall in y < 0 region
#     """
#
#     x = xyz.T[0]
#     y = xyz.T[1]
#
#     phi = np.arctan2(y, x)
#     mx = np.zeros_like(x)
#     my = np.zeros_like(x)
#     # theta = np.ones_like(x)*np.pi/2
#
#     theta = 2 * np.arctan(np.exp(x / delta)) - np.pi / 2
#
#     mx[x > 0] = pol_y * (-np.sin(phi[x > 0])) * np.sin(theta[x > 0])
#     mx[x <= 0] = pol_y * (-np.sin(phi[x <= 0])) * np.sin(theta[x <= 0])
#     my[x > 0] = pol_y * (np.cos(phi[x > 0])) * np.sin(theta[x > 0])
#     my[x <= 0] = pol_y * (np.cos(phi[x <= 0])) * np.sin(theta[x <= 0])
#
#     mz = np.cos(theta)
#     mz[y > 0] *= pol_n
#     mz[y <= 0] *= pol_s
#     return np.array([mx, my, mz]).T
#
#
# def neel_tube(xyz, chi):
#     x = xyz.T[0]
#     y = xyz.T[1]
#     phi = np.arctan2(y, x)
#     mx = np.zeros_like(x)
#     my = np.zeros_like(x)
#     delta = 5
#     # theta = np.ones_like(x)*np.pi/2
#     theta = 2 * np.arctan(np.exp(x / delta))
#
#     mx[x > 0] = chi * (-np.sin(phi[x > 0])) * np.sin(theta[x > 0])
#     mx[x <= 0] = -chi * (-np.sin(phi[x <= 0])) * np.sin(theta[x <= 0])
#     my[x > 0] = chi * (np.cos(phi[x > 0])) * np.sin(theta[x > 0])
#     my[x <= 0] = -chi * (np.cos(phi[x <= 0])) * np.sin(theta[x <= 0])
#
#     mz = np.cos(theta)
#     return np.array([mx, my, mz]).T
