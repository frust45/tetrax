import numpy as np
import pygmsh

__all__ = ["tube_cross_section",
           "tube_segment_cross_section",
           "round_wire_cross_section",
           "round_wire_cross_section_refined",
           "polygonal_cross_section",
           "rectangle_cross_section",
           "round_wire_cross_section_refined",
           ]


def tube_cross_section(r, R, lc=5, x0=0, y0=0):
    """Creates a mesh with the cross section of a tube.

    .. image:: /usage/tube_cross_section.png
       :width: 350

    Parameters
    ----------
    r : float
        Inner radius of the tube.
    R : float
        Outer radius of the tube.
    lc : float
        Characteristic length of mesh (default is 5).
    x0 : float
        x coordinate of the origin (Default is 0).
    y0 : float
        y coordinate of the origin (Default is 0).

    Returns
    -------
    mesh : meshio.Mesh

    See Also
    --------
    tube_segment_cross_section, round_wire_cross_section, polygonal_cross_section
    """
    with pygmsh.occ.Geometry() as geom:
        geom.characteristic_length_min = lc
        geom.characteristic_length_max = lc
        disk1 = geom.add_disk([x0, y0], R)
        disk2 = geom.add_disk([x0, y0], r)
        geom.boolean_difference(disk1, disk2)
        mesh = geom.generate_mesh()
    return mesh


def tube_segment_cross_section(arc_length, thickness, angle, lc=5, shifted_to_origin=True):
    """Creates a mesh with the cross section of a tube segment.

    .. image:: /usage/tube_segment_cross_section.png
       :width: 350

    Parameters
    ----------
    arc_length : float
        Average arc length of the tube segment (at the center surface).
    thickness : float
        Thickness of the tube segment.
    angle : float
        Opening angle of the tube segment (degrees).
    lc : float
        Characteristic length of mesh (default is 5).
    shifted_to_origin : bool
        If True, shift the tube segment to the coordinate origin (default is True).
        Otherwise, the origin will be the symmetry axis of the tube.

    Returns
    -------
    mesh : meshio.Mesh

    See Also
    --------
    tube_cross_section, round_wire_cross_section, polygonal_cross_section
    """
    angle = angle * np.pi / 180.0
    if angle == 2 * np.pi:
        r_avrg = arc_length / angle
        r = r_avrg - thickness / 2
        R = r_avrg + thickness / 2
        return tube_cross_section(r, R, lc, y0=-r_avrg)
    elif angle == 0:
        return rectangle_cross_section(arc_length, thickness, lc, lc)
    else:
        r_avrg = arc_length / angle
        shift = r_avrg if shifted_to_origin else 0
        r = r_avrg - thickness / 2
        R = r_avrg + thickness / 2
        with pygmsh.occ.Geometry() as geom:

            p0 = geom.add_point([-r * np.sin(-angle / 2), r * np.cos(-angle / 2) - shift], lc)
            p1 = geom.add_point([-R * np.sin(-angle / 2), R * np.cos(-angle / 2) - shift], lc)
            poly = geom.add_line(
                p0,
                p1,

            )
            geom.revolve(poly, [0.0, 0.0, 1.0], [0.0, -shift, 0.0], angle)
            mesh = geom.generate_mesh()
        return mesh


def round_wire_cross_section(R, lc=5, x0=0, y0=0):
    """Creates a mesh with the cross section of a round wire.

    .. image:: /usage/round_wire_cross_section.png
       :width: 350

    Parameters
    ----------
    R : float
        Radius of the round wire.
    lc : float
        Characteristic length of mesh (default is 5).
    x0 : float
        x coordinate of the origin (Default is 0).
    y0 : float
        y coordinate of the origin (Default is 0).

    Returns
    -------
    mesh : meshio.Mesh

    See Also
    --------
    polygonal_cross_section, rectangle_cross_section
    """
    with pygmsh.occ.Geometry() as geom:
        geom.characteristic_length_min = lc
        geom.characteristic_length_max = lc
        geom.add_disk([x0, y0], R)
        mesh = geom.generate_mesh()
    return mesh


def round_wire_cross_section_refined(R, func, x0=0, y0=0):
    """Creates a refined mesh with the cross section of a round wire.

    Instead of a fixed characteristic mesh length ``lc``, a scalar field ``func(x,y,z)`` is supplied to define ``lc``
    at each position of the mesh. This mesh is preferable, for example, for round wires housing a vortex string.

    .. image:: /usage/round_wire_cross_section.png
       :width: 350

    Parameters
    ----------
    R : float
        Radius of the round wire.
    func : func
        Scalar field of signture ``func(x,y,z)`` which returns the characteristic mesh length at each position.
    x0 : float
        x coordinate of the origin (Default is 0).
    y0 : float
        y coordinate of the origin (Default is 0).

    Returns
    -------
    mesh : meshio.Mesh

    See Also
    --------
    polygonal_cross_section, rectangle_cross_section
    """
    with pygmsh.occ.Geometry() as geom:
        geom.add_disk([x0, y0], R)
        geom.set_mesh_size_callback(
            lambda dim, tag, x, y, z, lc: func(x, y, z)
        )
        mesh = geom.generate_mesh()
    return mesh


def polygonal_cross_section(points, lc=5):
    """Creates a mesh with the cross section of a polygonal wire.

    .. image:: /usage/polygonal_cross_section.png
       :width: 350

    Parameters
    ----------
    points : list (Point)
        List of points defining the corners of the polygon.
    lc : float
        Characteristic length of mesh (default is 5).

    Returns
    -------
    mesh : meshio.Mesh

    See Also
    --------
    round_wire_cross_section, rectangle_cross_section
    """
    with pygmsh.geo.Geometry() as geom:
        geom.add_polygon(points, mesh_size=lc)
        mesh = geom.generate_mesh()
    return mesh


def rectangle_cross_section(a, b, lca=5, lcb=5):
    """Creates a regular mesh with rectangular cross section.

    .. image:: /usage/rectangle_cross_section.png
       :width: 350


    Parameters
    ----------
    a : float
        Width of the rectangle.
    b : float
        Thickness of the rectangle.
    lca : float
        characteristic mesh length along the width (Default is 5).
        The number of FEM layers along the width will be `a/lca + 1`.
    lcb : float
        characteristic mesh length along the thickness (Default is 5).
        The number of FEM layers along the thickness will be `b/lcb + 1`.

    Returns
    -------
    mesh : meshio.Mesh

    See Also
    --------
    round_wire_cross_section, polygonal_cross_section
    """
    with pygmsh.geo.Geometry() as geom:
        nx = int(a / lca)
        ny = int(b / lcb)
        p = geom.add_point([-a / 2, -b / 2, 0.0], mesh_size=lca)
        _, l, _ = geom.extrude(p, translation_axis=[a, 0, 0], num_layers=nx)
        geom.extrude(l, translation_axis=[0, b, 0], num_layers=max(2,ny))
        mesh = geom.generate_mesh()
    return mesh
