import numpy as np
import pygmsh

__all__ = ["disk", "tube", "prism"]


def disk(R, t, lc=5, nl=2, x0=0, y0=0):
    """Creates a mesh for a round disk.

    Parameters
    ----------
    R : float
        Radius of the round wire.
    t : float
        thickness of the disk.
    lc : float
        Characteristic length of mesh in the (xy) plane (default is 5).
    nl : integer
        The number of layers along the thickness (default is 3).
    x0 : float
        x coordinate of the origin (Default is 0).
    y0 : float
        y coordinate of the origin (Default is 0).

    Returns
    -------
    mesh : meshio.Mesh

    See Also
    --------

    """
    with pygmsh.occ.Geometry() as geom:
        geom.characteristic_length_min = lc
        geom.characteristic_length_max = lc
        disk = geom.add_disk([x0,y0,-t/2],R)
        geom.extrude(disk, translation_axis=[0, 0, t], num_layers=nl)
        mesh = geom.generate_mesh()
    return mesh

def tube(r, R, t, lc=5, nl=2, x0=0, y0=0):
    """Creates a mesh with the cross section of a tube.

    .. image:: /usage/tube_cross_section.png
       :width: 350

    Parameters
    ----------
    r : float
        Inner radius of the tube.
    R : float
        Outer radius of the tube.
    lc : float
        Characteristic length of mesh (default is 5).
    x0 : float
        x coordinate of the origin (Default is 0).
    y0 : float
        y coordinate of the origin (Default is 0).

    Returns
    -------
    mesh : meshio.Mesh

    See Also
    --------
    tube_segment_cross_section, round_wire_cross_section, polygonal_cross_section
    """
    with pygmsh.occ.Geometry() as geom:
        geom.characteristic_length_min = lc
        geom.characteristic_length_max = lc
        disk1 = geom.add_disk([x0, y0], R)
        disk2 = geom.add_disk([x0, y0], r)
        ring = geom.boolean_difference(disk1, disk2)
        geom.extrude(ring, translation_axis=[0, 0, t], num_layers=max(2,nl))
        mesh = geom.generate_mesh()
    return mesh



def prism(a, b, c, lca=5, lcb=5, lcc=5):
    """Creates a regular mesh of a prism.

    .. image:: /usage/prism.png
       :width: 350


    Parameters
    ----------
    a : float
        Length of the prism.
    b : float
        Width of the prism.
    c : float
        Thickness of the prism.
    lca : float
        characteristic mesh length along the length (Default is 5).
        The number of FEM layers along the length will be `a/lca + 1`.
    lcb : float
        characteristic mesh length along the width (Default is 5).
        The number of FEM layers along the width will be `b/lcb + 1`.
    lcc : float
        characteristic mesh length along the thickness (Default is 5).
        The number of FEM layers along the thickness will be `c/lcc + 1`.
    Returns
    -------
    mesh : meshio.Mesh

    See Also
    --------
    round_wire_cross_section, polygonal_cross_section
    """
    with pygmsh.geo.Geometry() as geom:
        nx = int(a / lca)
        ny = int(b / lcb)
        nz = int(c / lcc)
        p = geom.add_point([-a / 2, -b / 2, -c / 2], mesh_size=lca)
        _, l, _ = geom.extrude(p, translation_axis=[a, 0, 0], num_layers=nx)
        _, s, _ = geom.extrude(l, translation_axis=[0, b, 0], num_layers=ny)
        geom.extrude(s, translation_axis=[0, 0, c], num_layers=max(2,nz))
        mesh = geom.generate_mesh()
    return mesh
