import numpy as np
import pygmsh

__all__ = ["monolayer_line_trace", "bilayer_line_trace", "multilayer_line_trace"]

def monolayer_line_trace(thickness, lc=2, y0=0):
    """Creates a mesh for the line trace along the normal direction of a magnetic monolayer.

    .. image:: /usage/monolayer_line_trace.png
       :width: 350

    Parameters
    ----------
    thickness : float
        Thickness :math:`d` of the monolayer.
    lc : float
        Characteristic mesh length.
    y0 : float
        y coordinate of the center (Default is 0).
    """
    return multilayer_line_trace([thickness], [], lc, y0)


def bilayer_line_trace(thickness1, thickness2, spacing, lcs, y0=0):
    """Creates a mesh for the line trace along the normal direction of a magnetic bilayer.

    .. image:: /usage/bilayer_line_trace.png
       :width: 350

    Parameters
    ----------
    thickness1 : float
        Thickness :math:`d_1` of the bottom magnetic layer.
    thickness2 : float
        Thickness :math:`d_2` of the top magnetic layer.
    spacing : float
        Thickness :math:`s` of the interlayer.
    lcs : float or list(float)
        Characteristic lengths for the different magnetic layers. If list, has to contain two elements. If
        not a list it will be same for both layers.
    y0 : float
        y coordinate of the center (Default is 0).

    See also
    --------
    monolayer_line_trace, multilayer_line_trace
    """
    return multilayer_line_trace([thickness1, thickness2], [spacing], lcs, y0)


def multilayer_line_trace(thicknesses, spacings, lcs, y0=0):
    r"""Creates a mesh for the line trace along the normal direction of a magnetic multi-layer.

    .. image:: /usage/multilayer_line_trace.png
       :width: 350

    Parameters
    ----------
    thicknesses : list(float)
        Thicknesses :math:`[d_1, d_2, d_3, ...]` of the magnetic layers (ordered with increasing :math:`y` coordinate). List must be one element
        longer than ``spacings``.
    spacings : list(float)
        Spacings :math:`[s_1, s_2, s_3, ...]` between the magnetic layers, which is are the thicknesses of the non-magnetic interlayers
        (ordered with increasing :math:`y` coordinate). List must be one element shorter than ``thicknesses``.
    lcs : list(float) or float
        Characteristic lengths for the different magnetic layers. If list, has to be same length as ``thicknesses``. If
        not a list it will be same for all layers.
    y0 : float
        y coordinate of the center (Default is 0).

    Returns
    -------
    mesh : meshio.Mesh
    """

    lcs = [lcs for d in thicknesses] if not isinstance(lcs, list) else lcs

    if len(spacings) != len(thicknesses) - 1:
        print(f"Number of layer thicknesses ({len(thicknesses)}) and number of spacings ({len(spacings)}) do "
              f"not match (has to be off by one).")

    if len(lcs) != len(thicknesses):
        print(f"Number of layer thicknesses ({len(thicknesses)}) and number of characteristic lengths ({len(lcs)}) do "
              f"not match (has to be the same).")


    d_tot = sum(spacings) + sum(thicknesses)

    spacings.append(0)

    with pygmsh.geo.Geometry() as geom:
        y1_i = -d_tot / 2 + y0
        for d, s, lc in zip(thicknesses, spacings, lcs):
            ny_i = int(d / lc)
            p1 = geom.add_point([0.0, y1_i, 0.0], mesh_size=lc)
            geom.extrude(p1, translation_axis=[0.0, d, 0.0], num_layers=ny_i)
            y1_i += d + s
        mesh = geom.generate_mesh()
    return mesh
