from pathlib import Path

from setuptools import Extension, setup, find_packages, dist

dist.Distribution().fetch_build_eggs(['Cython>=0.15.1', 'numpy>=1.10'])
import numpy as np
from Cython.Build import cythonize

fem_core_path = Path('tetrax/core/fem_core').resolve()

with open("README.md", "r") as fh:
    long_description = fh.read()

c_sourcefiles = list(map(lambda p: str(fem_core_path / p), [
    'dense2D_onthefly.c',
    'bessel_K1.c',
    'akmag.c',
    'rotation_mat.c',
    'matutils.c',
    'sorting.c',
    'utils.c',
    'bnd2d.c',
    'opmatspm.c',
    'galerkin.c',
    'fempreproc.c',
    'hotriang.c',
    'dense3D.c'
]))

extensions = [
    Extension('tetrax.core.fem_core.cythoncore',
              c_sourcefiles + ['tetrax/core/fem_core/cythoncore.pyx'],
              libraries=[],
              library_dirs=[],
              include_dirs=[str(fem_core_path), 'main', np.get_include()])
]

setup(
    name='TetraX',
    version='1.3.3',
    entry_points={'console_scripts': ['TetraX=fem_core:main']},
    packages=find_packages(),
    ext_modules=cythonize(extensions, language_level=3),
    py_modules=["tetrax"],
    author="Lukas Körber, Attila Kákay",
    author_email="l.koerber@hzdr.de, a.kakay@hzdr.de",
    url="https://codebase.helmholtz.cloud/micromagnetic-modeling/tetrax",
    description="Finite-element micromagnetic modeling package.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    license="GNU GPLv3",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Programming Language :: Cython",
        "Programming Language :: C",
        "Programming Language :: Python :: 3 :: Only",
        "Topic :: Scientific/Engineering :: Physics",
        "Topic :: Scientific/Engineering :: Mathematics",
    ],
    install_requires=[
        "Cython",
        "k3d",
        "meshio == 4.4.6",
        "numpy>=1.20.0",
        "pandas",
        "pygmsh == 7.1.14",
        "scipy",
        "setuptools",
        "tqdm",
        "bibtexparser",
    ],
    extras_require={
        "dev": [
            "pydata_sphinx_theme",
            "nbsphinx",
            "numpydoc",
            "sphinx_copybutton",
        ]
    }
)
