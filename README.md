# TetraX

![](logo_large.png)

TetraX is a package for finite-element-method (FEM) micromagnetic modeling of magnetization statics and dynamics with
the aim to provide user friendly and flexible workflows. Apart from energy minimizers and an LLG solver, it provides
implementations of several FEM dynamic-matrix approaches to numerically calculate the normal modes and associated
frequencies for magnetic specimen of different geometries such as confined samples, infinitely long waveguides, or
infinitely extended multilayers. Next to support for ferromagnets, TetraX will also provide the first full micromagnetic
package for antiferromagnets.

TetraX is maintained by the [Micromagnetic Modeling Group](https://www.hzdr.de/db/Cms?pOid=55944&pNid=107) of Dr. Attila
Kákay at the [Helmholtz-Zentrum Dresden - Rossendorf](https://www.hzdr.de).

For questions or feedback, please contact [Lukas Körber](mailto:l.koerber@hzdr.de).

# Installation

Install this package using

```bash
   pip install git+https://gitlab.hzdr.de/micromagnetic-modeling/tetrax.git
```

To allow for 3D visualization in Jupyter notebooks, you additionally need to activate the k3d extension in your shell
using

```bash
   jupyter nbextension install --py --sys-prefix k3d
   jupyter nbextension enable --py --sys-prefix k3d
```

# Getting started

To get started, take a look into the package documentation on [Read the Docs](https://tetrax.readthedocs.io/en/latest/).
