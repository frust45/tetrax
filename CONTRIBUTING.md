# How to contribute to the TetraX code base

## Coding style

Please adopt the [PEP 8](https://www.python.org/dev/peps/pep-0008/) style guide when programing Python code. We recommend using the PyCharm IDE together with the "Save Actions" plug-in which automatically formats code and optimizes package imports upon saving each file. As our documentation is auto-generated using sphinx, it is important to use the [numpydoc](https://numpydoc.readthedocs.io/en/latest/format.html) format when documeting code. When using PyCharm, you can set it up to automatocally generate template doc strings in this format.


## Git 
### Commit messages

TetraX is developed using git. When contributing to the code base, please use [meaningful commit messages](https://chiamakaikeanyi.dev/how-to-write-good-git-commit-messages/). Here are a bad and a good example:

> bug was fixed
>
> -- <cite>a bad commit message</cite>

> Add documentation for 'AbstractSample' class.
>
> -- <cite>a good commit message</cite>


### Branch strategy

At the moment, TetraX is developed in a small team. Therefore, we employ a simple branching strategy, which is close to the GitHub workflow:

#### Branch structure

- The `main` branch is protected and always deployable. Merges with this branch are either considered as feature releases or as hotfixes.
- The `develop` branch is where all the developement happens. 

Both `main` and `develop` are protected branches. Nobody can push to and only maintainers can merge.

#### Implementing a new feature 

If you want to implement a new feature (maybe in response to an issue), do the following:
1. Create a branch called `feature/feature_name` from `develop`
2. Do your development and commits against this branch.
3. When finished coding, request to merge with `develop`. The merge request message should describe the implemented feature.
4. Test if everything is still working on `develop`, especially if other features have been implemented in meantime.
5. If testing fails, go back to `feature/feature_name`, do your fixes and repeat from step 3.
6. If tests go well, delete the feature branch.
7. On `develop` a new release is finalized by user testing. 
Once enough features have been implemented to justify a new version number, `develop` is merged to `main` by a maintainer and the minor version number is increased, for example from 1.1.x to 1.2. A tag (release) with this version number is created for this merge.

#### Hotfixes

Sometimes it is necessary to patch the software and fix some bugs inbetween feature releases. In such a case the strategy is different:
1. Create a branch called `hotfix/fix_name` from `main`. You might also include multiple fixes in this branch.
2. Perform fix(es) and test.
3. Request a merge to `main`.

A maintainer will review and accept the hotfixes and then create a new tag (release) where the patch version number increases, for example from 1.1 to 1.1.1.


