tetrax.core.fem\_core package
=============================

Submodules
----------

tetrax.core.fem\_core.cythoncore module
---------------------------------------

.. automodule:: tetrax.core.fem_core.cythoncore
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: tetrax.core.fem_core
   :members:
   :undoc-members:
   :show-inheritance:
