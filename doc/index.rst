.. TetraX documentation master file, created by sphinx-quickstart on Sat Jan 15 20:14:29 2022. You can adapt this file completely to your liking, but it should at least contain the root `toctree` directive.

.. image:: ../logo_large.png
   :width: 600

|
|

TetraX Documentation
====================

.. toctree::
   :maxdepth: 4
   :caption: Contents:
   :hidden:

   Getting started <quickstart>
   Examples <examples>
   Publications <publications>
   User Guide <usage>
   API Reference <modules>
   Release Notes <release_notes>


.. automodule:: tetrax

**Version**: 1.3.3

TetraX is a package for finite-element-method (FEM) micromagnetic modeling with the aim to provide user friendly and versatile micromagnetic workflows. Apart from energy minimizers and an LLG solver, it aims to provide implementations of several FEM dynamic-matrix approaches to numerically calculate the normal modes and associated frequencies for magnetic specimen of different geometries such as confined samples, infinitely long waveguides, or infinitely extended multilayers. Apart from ferromagnets, the package also supports antiferromagnets as an experimental feature.

.. list-table::

    * - .. figure:: images/sample_showhelical.png

					 Magnetic equilibria
      - .. figure:: images/dispersion_tube.png

           Spin-wave dispersions
      - .. figure:: examples/modemovie.gif

           Mode profiles

and more.

Getting start
-------------

For a quick introduction, how to start your own FEM micromagnetic simulations, visit our :doc:`quickstart` page and take a look at the provided :doc:`examples`.


.. _howtocite:

Cite us
-------

If you use TetraX for your research, please cite

.. [1] L. Körber, G. Quasebarth, A. Hempel, F. Zahn, A. Otto, E. Westphal, R. Hertel and A. Kákay (2022).
       "TetraX: Finite-Element Micromagnetic-Modeling Package",
       Rodare. DOI: `10.14278/rodare.1418 <https://doi.org/10.14278/rodare.1418>`_
.. [2] L. Körber, G. Quasebarth, A. Otto and A. Kákay, "Finite-element dynamic-matrix approach for
       spin-wave dispersions in magnonic waveguides with arbitrary
       cross section", `AIP Advances 11, 095006 (2021) <https://doi.org/10.1063/5.0054169>`_


.. code-block:: TeX

   @misc{TetraX,
     author = {Körber, Lukas and
               Quasebarth, Gwendolyn and
               Hempel, Alexander and
               Zahn, Friedrich and
               Otto, Andreas and
               Westphal, Elmar and
               Hertel, Riccardo and
               Kakay, Attila},
        title = {{TetraX: Finite-Element Micromagnetic-Modeling
                  Package}},
        month = jan,
        year = 2022,
        doi = {10.14278/rodare.1418},
        url = {https://doi.org/10.14278/rodare.1418}
    }

   @article{korberFiniteelementDynamicmatrixApproach2021a,
		title = {Finite-element dynamic-matrix approach for spin-wave dispersions
		         in magnonic waveguides with arbitrary cross section},
		volume = {11},
		doi = {10.1063/5.0054169},
		language = {en},
		journal = {AIP Advances},
		author = {Körber, L and Quasebarth, G and Otto, A and Kákay, A},
		year = {2021},
		pages = {095006},
	}


The numerical experiments implemented in `TetraX` are often based on seminal papers.
In order to give credit to these works, when conducting a numerical experiment, `TetraX` saves references
important for this experiment to a bibtex file called ``references.bib``,
found in the sample directory. In this file, each entry contains a ``comment``
field describing how the reference was important for the computation.
When publishing results calculated with `TetraX` in your research, please
also give credit to the works which are important for the numerical experiments
you conducted.

Source & license
----------------

The `source code <https://gitlab.hzdr.de/micromagnetic-modeling/tetrax>`_ of TetraX is licensed under the GNU GPL v3.0 Open-Source license.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
