tetrax.core package
===================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   tetrax.core.fem_core

Submodules
----------

tetrax.core.experimental\_setup module
--------------------------------------

.. automodule:: tetrax.core.experimental_setup
   :members:
   :undoc-members:
   :show-inheritance:

tetrax.core.mesh module
-----------------------

.. automodule:: tetrax.core.mesh
   :members:
   :undoc-members:
   :show-inheritance:

tetrax.core.operators module
----------------------------

.. automodule:: tetrax.core.operators
   :members:
   :undoc-members:
   :show-inheritance:

tetrax.core.sample module
-------------------------

.. automodule:: tetrax.core.sample
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: tetrax.core
   :members:
   :undoc-members:
   :show-inheritance:
