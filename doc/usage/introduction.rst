Introduction
============

.. contents:: Table of Contents
   :depth: 2
   :local:
   :backlinks: none

Basic usage
-----------

`TetraX` is a package for finite-element-method (FEM) micromagnetic modeling with the aim to provide user friendly and versatile micromagnetic workflows. Usually, for FEM simulations one requires a software to create the finite element mesh which is used for doing the actual simulations and a visualization program to analyse the results. With the possibility to use `TetraX` from Jupter notebooks we tried to combine the mesh creation, simulation and visualization into one package. Furthermore, by providing pre-defined functions to create finite element meshes of different geometries scientist do not need to become experts of finite element mesh creation but can right away perform simulations and concentrate on the science. By running the simulations in Jupyter notebooks, one can use all the benefits of Python, such as running multiple simulations over large parameter spaces, easy visualization of dataframes or vectorfields defined on the finite element meshes. Moreover, the workflows of simulations can be shared between colleagues or published as supplementary materials of manscuripts. To get the taste, please check the :doc:`/quickstart`.

Those who prefer to run simulations remotely or have access to high-performance clusters, do not need to use Jupyter notebooks. However, we strongly recommend to create a Juptyer notebook and export it as a python code that can be run on clusters using a job scheduling system as the `SLURM <https://www.schedmd.com/index.php>`_. To see how to set-up your numerical experiments in a python script file, see :any:`here <howtoscript>`.



Using TetraX in your research
-----------------------------

If you use `TetraX` for your research, we kindly ask you please cite

.. [1] L. Körber, G. Quasebarth, A. Hempel, F. Zahn, A. Otto, E. Westphal, R. Hertel and A. Kákay (2022).
       "TetraX: Finite-Element Micromagnetic-Modeling Package",
       Rodare. DOI: `10.14278/rodare.1418 <https://doi.org/10.14278/rodare.1418>`_
.. [2] L. Körber, G. Quasebarth, A. Otto and A. Kákay, "Finite-element dynamic-matrix approach for
       spin-wave dispersions in magnonic waveguides with arbitrary
       cross section", `AIP Advances 11, 095006 (2021) <https://doi.org/10.1063/5.0054169>`_


.. code-block:: TeX

   @misc{TetraX,
     author = {Körber, Lukas and
               Quasebarth, Gwendolyn and
               Hempel, Alexander and
               Zahn, Friedrich and
               Otto, Andreas and
               Westphal, Elmar and
               Hertel, Riccardo and
               Kakay, Attila},
        title = {{TetraX: Finite-Element Micromagnetic-Modeling
                  Package}},
        month = jan,
        year = 2022,
        doi = {10.14278/rodare.1418},
        url = {https://doi.org/10.14278/rodare.1418}
    }

   @article{korberFiniteelementDynamicmatrixApproach2021a,
		title = {Finite-element dynamic-matrix approach for spin-wave dispersions
		         in magnonic waveguides with arbitrary cross section},
		volume = {11},
		doi = {10.1063/5.0054169},
		language = {en},
		journal = {AIP Advances},
		author = {Körber, L and Quasebarth, G and Otto, A and Kákay, A},
		year = {2021},
		pages = {095006},
	}


The numerical experiments implemented in `TetraX` are often based on seminal papers.
In order to give credit to these works, when conducting a numerical experiment, `TetraX` saves references
important for this experiment to a bibtex file called ``references.bib``,
found in the sample directory. In this file, each entry contains a ``comment``
field describing how the reference was important for the computation.
When publishing results calculated with `TetraX` in your research, please
also give credit to the works which are important for the numerical experiments
you conducted.

Problems or feature requests
----------------------------

`TetraX` is maintained by the `Micromagnetic Modeling Group <https://www.hzdr.de/db/Cms?pOid=55944&pNid=107>`_ of Dr. Attila Kákay at the `Helmholtz-Zentrum Dresden - Rossendorf <https://www.hzdr.de>`_. In case you experience any problem, being related with the `TetraX` package or installation issues, please directly write to our `support email <mailto:gitlab-incoming+micromagnetic-modeling-tetrax-4372-issue-@hzdr.de>`_ or contact either `Lukas Körber <mailto:l.koerber@hzdr.de>`_ or `Attila Kákay <mailto:a.kakay@ghzdr.de>`_. We are happy to help you with implementing new features for your studies. Please approach us to discuss the details and find the optimal way.

Acknowledgements
----------------

The authors are very thankful to Steffen Boerm, Burkhard Clauß, Pedro Landeros, Jorge A. Otálora, Jürgen Lindner and Jürgen Fassbender for fruitful discussions. We are grateful to Henrik Schulz and Jens Lasch for their continuous support of our computational infrastructure. Financial support from the Deutsche Forschungsgemeinschaft within the programs under Grant Nos. KA 5069/1-1 and KA 5069/3-1 is gratefully acknowledged.
