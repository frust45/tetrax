Numerical Experiments
=====================

.. contents:: Table of Contents
   :depth: 2
   :local:
   :backlinks: none

The ``ExperimentalSetup`` object
--------------------------------

Creating a setup and conducting experiments
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The central object to conduct numerical experiments is the :mod:`ExperimentalSetup <tetrax.core.experimental_setup.ExperimentalSetup>` class which is always associated to a sample. It can be created using

.. code-block:: python

   exp = tetrax.create_experimental_setup(sample, name="my_experiment")

As with the names of sample objects, the ``name`` attribute of the setup is only for saving purposes and is used to create directories for data calculated in numerical experiments. Within the setup, experimental conditions can be specified:

- ``Bext``, a static external magnetic field given in units of Tesla, which, like the magnetization of the sample, is a :class:`MeshVector <tetrax.core.mesh.MeshVector>` object (an array of shape ``(nx, 3)``) for which template vectorfields are found in the :mod:`tetrax.vectorfields` module. Homogenous external fields can also be set for examples as

    .. code-block:: python

       exp.Bext = [0, 0, 5e-3]

- ``antenna`` is the microwave antenna in the experimental setup for which the microwave absorption of the spin-wave modes in the sample can be calculated, or which (in ``confined`` samples) can be used to apply time-dependent external fields. An antenna can be set using

    .. code-block:: python

       exp.antenna = tetrax.core.experimental_setup.CPWAntenna(150, 30, 54)


The full experimental setup, including external field and microwave antenna, can be visualized using

.. code-block:: python

   exp.show()

Within the setup, a number of numerical experiments can be conducted (which are documented below) as

.. code-block:: python

   exp.relax()
   exp.eigenmodes()
   exp.absorption()
   exp.evolve() # only for confined samples

Microwave antennae
^^^^^^^^^^^^^^^^^^
The available microwave antennae, called from the :mod:`tetrax.core.experimental_setup` module, are

.. autoclass:: tetrax.core.experimental_setup.CPWAntenna
    :undoc-members:

.. autoclass:: tetrax.core.experimental_setup.CurrentLoopAntenna
    :undoc-members:

.. autoclass:: tetrax.core.experimental_setup.HomogeneousAntenna
    :undoc-members:

.. autoclass:: tetrax.core.experimental_setup.StriplineAntenna
    :undoc-members:

Energy minimization
-------------------

For many purposes it is important to calculate the magnetic equilibrium state of a sample under given external conditions. There are many different ways to do this, with the simplest one being minimizing the total magnetic energy (the energy length density for waveguide samples and the energy area density for layer samples).

.. warning::
   The relaxation in waveguide samples is not always stable and should be conducted with care.

After initializing the magnetization of the sample (``mag``) can be done using the :func:`relax() <tetrax.core.experimental_setup.ExperimentalSetup.relax>` method.

.. autofunction:: tetrax.core.experimental_setup.ExperimentalSetup.relax

Linear magnetization dynamics
-----------------------------

Spin-wave normal-mode analysis
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Mode frequencies (dispersion) and spatial profiles
""""""""""""""""""""""""""""""""""""""""""""""""""

To obtain the frequencies and spatial mode profiles of the spin-wave normal modes in a given sample, the linearized
lossless Landau-Lifshitz-Gilbert equation

.. math::
   :label: ev_problem

    \frac{\omega_\nu}{\omega_M} \mathbf{m}_\nu = i [\mathbf{m}_0 \times \hat{\mathbf{\Omega}}]\mathbf{m}_\nu  \quad \text{with} \quad \mathbf{m}_0 \perp \mathbf{m}_\nu

can be solved numerically using a dynamic-matrix approach. Here, :math:`\mathbf{m}_\nu` are the complex-valued spatial profiles of the normal modes, :math:`\mathbf{m}_0` is the normalized equilibrium magnetization, :math:`\omega_M = \mu_0\gamma M_\mathrm{s}` is the characteristic frequency of the material and the operator :math:`\hat{\mathbf{\Omega}}` is given as

.. math::

    \hat{\mathbf{\Omega}} = h_0 \hat{\mathbf{\Omega}} + \hat{\mathbf{N}}

with the projection of the (unitless) equilibrium effective field onto the equilibrium magnetization

.. math::

    h_0 = \mathbf{m}_0 \cdot (\mathbf{h}_\mathrm{ext} + \hat{\mathbf{N}}\mathbf{m}_0 ).

The operator :math:`\hat{\mathbf{N}}` is a certain Hermetian operator, which contains the mangetic self interactions

.. math::

    \hat{\mathbf{N}} = \hat{\mathbf{N}}^\mathrm{(exc)} + \hat{\mathbf{N}}^\mathrm{(dip)} + \hat{\mathbf{N}}^\mathrm{(uni)} + \hat{\mathbf{N}}^\mathrm{(cub)} + \hat{\mathbf{N}}^\mathrm{(iDMI)} + \hat{\mathbf{N}}^\mathrm{(bDMI)}

and which we refer to here as the magnetic tensor. Its application to the magnetization yields the effective field. For the cubic anisotropy, a linearized  operator is used, except for the equilibrium field :math:`h_0` for which the full nonlinear field is used. For details, about the individual terms and their implementation, refer to the :doc:`Micromagnetic model and implementation </usage/appendix>` section in the appendix.

To implement the constraint :math:`\mathbf{m}_0 \perp \mathbf{m}_\nu`, the eigenvalue problem :eq:`ev_problem` is projected into the subspace locally orthogonal to the equilibrium direction :math:`\mathbf{m}_0`.

For propagating spin waves in waveguide, or layer samples, the eigenvalue problem is projected into a single cross section of the sample by making the replacements

.. math::

   \mathbf{m}_\nu \rightarrow \mathbf{m}_{\nu k} = \mathbf{m}_{\nu} e^{-ikz} \\
   \hat{\mathbf{N}} \rightarrow \hat{\mathbf{N}}_k = e^{-ikz}\hat{\mathbf{N}} e^{ikz}

with the wave vector in :math:`z` direction :math:`k`, the lateral mode profiles :math:`\mathbf{m}_{\nu k}` and the propagating-wave tensors :math:`\hat{\mathbf{N}}_k`. The eigenvalue problem is then solved for the lateral profiles and the dispersion :math:`\omega_\nu(k)`. For of the propagating-wave dynamic-matrix approach used here, see Ref [1]_.

.. note::
   During the course of this User Guide, volumentric :math:`\mathbf{m}_\nu` and lateral profiles :math:`\mathbf{m}_{\nu k}` are often interchangeably denoted simply as "mode profiles", unless stated otherwise.


In `TetraX`, the eigensystem of a given ``sample`` in an experimental setup ``exp`` can be calculated by

.. code-block:: python

   >>> dispersion = exp.eigenmodes(...)

or simply

.. code-block:: python

   >>> exp.eigenmodes(...)

By default, the oscillation frequencies and the mode profiles are saved into the directory of the experimental setup.

.. autofunction:: tetrax.core.experimental_setup.ExperimentalSetup.eigenmodes

Linear mode damping
"""""""""""""""""""

The linewidths :math:`\Gamma_\nu` of the calculated normal modes can be calculated from the mode profiles and frequencies according to

.. math::

    \Gamma_\nu = \alpha_\mathrm{G} \epsilon_\nu \omega_\nu

with :math:`\alpha_\mathrm{G}` being the Gilbert-damping parameter and :math:`\epsilon_\nu` being the ellipticity coefficient of the respective mode which is calculated from the mode profile :math:`\mathbf{m}_\nu` according to

.. math::

    \epsilon_\nu = -i\frac{\langle\vert\mathbf{m}_\nu\vert^2\rangle_\mathrm{S}}{\langle \mathbf{m}_\nu^* \cdot \mathbf{m}_0\times\mathbf{m}_\nu\rangle_\mathrm{S}}

with the equilibrium-magnetization direction :math:`\mathbf{m}_0` and the sample average :math:`\langle ... \rangle_\mathrm{S}` (see Ref [2]_ for spin waves in general samples and Ref [3]_ for propagating waves in waveguide samples). The linewidths can be automatically calculated together with the dispersion by setting ``linewidths=True`` when calling the :func:`eigenmodes() <tetrax.core.experimental_setup.ExperimentalSetup.eigenmodes>` method, or, alternatively, using the :func:`linewidths() <tetrax.core.experimental_setup.ExperimentalSetup.linewidths>` method.

.. autofunction:: tetrax.core.experimental_setup.ExperimentalSetup.linewidths

Microwave absorption and dynamic susceptibility
"""""""""""""""""""""""""""""""""""""""""""""""

If the experimental setup has an ``antenna``, the high-frequency power absorption (microwave absorption) of the spin-wave normal modes can be calculated with respect to this antenna, as a function of frequency and wave vector
(only for propagating waves).

The wave-vector- and frequency-dependent microwave absorption :math:`P(k,\omega)` is calculated according to

.. math::

    P(k,\omega) \propto \sum\limits_\nu \frac{\vert h_\nu(k)\vert^2}{[\omega_\nu(k) - \omega]^2-\Gamma_\nu^2(k)}

where the index :math:`\nu` runs over all calculated modes (per wave vector), :math:`\omega_\nu(k)` are the mode
angular frequencies and :math:`\Gamma_\nu(k) = \alpha_\mathrm{G}\epsilon_\nu(k)\omega_\nu(k)` are the linewiths,
determined from the Gilbert damping parameter :math:`\alpha_\mathrm{G}` and the mode ellipticites (see previous section). The factor :math:`h_\nu(k)` is given by the overlap of the microwave
field distribution (possible wave-vector dependent)
with the respective mode profile

.. math::
   h_\nu(k) = -i\frac{\langle\mathbf{m}_\nu^* \cdot \mathbf{h}(k)\rangle_\mathrm{S}}{\langle \mathbf{m}_\nu^* \cdot \mathbf{m}_0\times\mathbf{m}_\nu\rangle_\mathrm{S}}


with the equilibrium-magnetization direction :math:`\mathbf{m}_0` and the sample average :math:`\langle ... \rangle_\mathrm{S}` (see, for example, Appendix A of Ref [3]_ for details).

.. note::
   Naturally, for confined samples, the :math:`k` dependence of an antenna is exchanged by a :math:`z` dependence.

In `TetraX`, the microwave absorption of a given ``sample`` in an experimental setup ``exp`` can be calculated by

.. code-block:: python

   >>> (absorbed_power, wave_vectors, frequencies) = exp.absorption(...)

or, for a single wave vector

.. code-block:: python

   >>> subsceptibility = exp.absorption(k=0, ...)

.. autofunction:: tetrax.core.experimental_setup.ExperimentalSetup.absorption

Perturbation analysis and reverse-engineering of analytical spin-wave dispersions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Within the :func:`eigenmodes() <tetrax.core.experimental_setup.ExperimentalSetup.eigenmodes>`, `TetraX` also provides the possibility to perform disentangle dipole-dipole hybridized modes and to numerically reverse-engineer the stiffness fields within a spin-wave dispersion (see Ref. [4]_).

For example, the zeroth-order dipolar perturbation of exchange modes can be calculated using

.. code-block:: python

   >>> exp.absorption(..., no_dip=True, perturbed_dispersion=True)

This will first calculate the normal modes without dipolar interaction and then calculate their perturbed frequency including dynamic dipolar fields and neglecting hybridization. As stated in [4]_ only works well, if the spatial mode profiles are not changed considerably by the dipolar interaction. If desired, dipolar fields can still be included in the equilibrium field by supplying ``h0_dip=True``.

During the calculation, the stiffness fields :math:`N_\nu^{(ij)}` (:math:`i,j=1,2`) within the general spin-wave dispersion

.. math::
    \frac{\omega_\nu(k)}{\omega_M} = \mathrm{Im}\, N_\nu^{(21)} + \sqrt{\Big(N_\nu^{(11)} + \langle h_0 \rangle_\mathrm{S} \Big)\Big(N_\nu^{(22)} + \langle h_0 \rangle_\mathrm{S} \Big) - \Big(\mathrm{Re}\, N_\nu^{(21)} \Big)^2}

are calculated for each mode and magnetic interaction [4]_.

They can be saved into the dispersion dataframe by supplying ``save_stiffness_fields=True``. This method can of course also be used for the true dipole-exchange normal modes (``no_dip=False``) and allow, for example, to disentangle the contributions of different magnetic interactions to the magnetochiral stiffness field responsible for any dispersion asymmetry,

.. math::

   \mathrm{Im}\, N_\nu^{(21)} \equiv \mathcal{A}_\nu = \mathcal{A}_\nu^\mathrm{(dip)} + \mathcal{A}_\nu^\mathrm{(DMI)} + ...

For details of the calculations, we refer to Ref [4]_.

Nonlinear LLG dynamics
----------------------

This feature will be implemented in a future release.


References
----------

.. [1]  Körber, *et al.*, "Finite-element dynamic-matrix approach for spin-wave dispersions in magnonic waveguides with arbitrary cross section", `AIP Advances 11, 095006 (2021) <https://doi.org/10.1063/5.0054169>`_

.. [2] Verba, *et al.*, "Damping of linear spin-wave modes in magnetic nanostructures: Local, nonlocal, and coordinate-dependent damping", `Phys. Rev. B 98, 104408 (2018) <https://doi.org/10.1103/PhysRevB.98.104408>`_

.. [3] Körber, *et al.*, "Symmetry and curvature effects on spin waves in vortex-state hexagonal nanotubes", `Phys. Rev. B 104, 184429 (2021) <https://doi.org/10.1103/PhysRevB.104.184429>`_

.. [4] Körber and Kákay, "Numerical reverse engineering of general spin-wave dispersions: Bridge between numerics and analytics using a dynamic-matrix approach", `Phys. Rev. B 104, 174414 (2021) <https://doi.org/10.1103/PhysRevB.104.174414>`_
