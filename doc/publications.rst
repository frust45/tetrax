Publications
============

If you use `TetraX` for your research, please see :any:`here <howtocite>` on how to cite us.

Publications relevant for the this package:
-------------------------------------------

The following publications were essential in the development of `TetraX`.

.. [T1] L. Körber, G. Quasebarth, A. Hempel, F. Zahn, A. Otto, E. Westphal, R. Hertel and A. Kákay (2022).
       "TetraX: Finite-Element Micromagnetic-Modeling Package",
       Rodare. DOI: `10.14278/rodare.1418 <https://doi.org/10.14278/rodare.1418>`_
.. [T2] Körber, *et al.*, "Finite-element dynamic-matrix approach for
       spin-wave dispersions in magnonic waveguides with arbitrary
       cross section", `AIP Advances 11, 095006 (2021) <https://doi.org/10.1063/5.0054169>`_
.. [T3] Körber and Kákay, "Numerical reverse engineering of general spin-wave dispersions: Bridge between numerics and analytics using a dynamic-matrix approach", `Phys. Rev. B 104, 174414 (2021) <https://doi.org/10.1103/PhysRevB.104.174414>`_
.. [T4] Körber, *et al.*, "Finite-element dynamic-matrix approach for propagating spin waves: Extension to mono- and multilayers of arbitrary spacing and thickness", `AIP Advances 12, 115206 (2022) <https://doi.org/10.1063/5.0107457>`_


Research using TetraX:
----------------------

The following publications used `TetraX` or a previous version of it (ordered chronologically).

.. [1] Körber, *et al.*, "Symmetry and curvature effects on spin waves in vortex-state hexagonal nanotubes", `Phys. Rev. B 104, 184429 (2021) <https://doi.org/10.1103/PhysRevB.104.184429>`_
.. [2] Körber, *et al.*, "Mode splitting of spin waves in magnetic nanotubes with discrete symmetries", `Phys. Rev. B 105, 184435 (2022) <https://doi.org/10.1103/PhysRevB.105.184435>`_
.. [3] Körber, *et al.*, "Curvilinear spin-wave dynamics beyond the thin-shell approximation: Magnetic nanotubes as a case study", `Phys. Rev. B 106, 014405 (2022) <https://doi.org/10.1103/PhysRevB.106.014405>`_
.. [4] Riedel , *et al.* "Hybridization‐Induced Spin‐Wave Transmission Stop Band within a 1D Diffraction Grating", `Advanced Physics Research (2023) <https://doi.org/10.1002/apxr.202200104>`_
.. [5] Iurchuk , *et al.* "Tailoring crosstalk between localized 1D spin-wave nanochannels using focused ion beams", `Scientific Reports volume 13, Article number: 764 (2023) <https://doi.org/10.1038/s41598-022-27249-w>`_
.. [6] Hache , *et al.* "Control of 4-magnon-scattering in a magnonic waveguide by pure spin current", `arXiv preprint (2023) <https://doi.org/10.48550/arXiv.2304.02708>`_
