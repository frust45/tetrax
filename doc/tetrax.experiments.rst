tetrax.experiments package
==========================

Submodules
----------

tetrax.experiments.calculate\_absorption module
-----------------------------------------------

.. automodule:: tetrax.experiments.calculate_absorption
   :members:
   :undoc-members:
   :show-inheritance:

tetrax.experiments.eigen module
-------------------------------

.. automodule:: tetrax.experiments.eigen
   :members:
   :undoc-members:
   :show-inheritance:

tetrax.experiments.relax module
-------------------------------

.. automodule:: tetrax.experiments.relax
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: tetrax.experiments
   :members:
   :undoc-members:
   :show-inheritance:
