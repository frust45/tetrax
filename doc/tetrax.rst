tetrax package
==============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   tetrax.core
   tetrax.experiments
   tetrax.geometries
   tetrax.helpers

Submodules
----------

tetrax.vectorfields module
--------------------------

.. automodule:: tetrax.vectorfields
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: tetrax
   :members:
   :undoc-members:
   :show-inheritance:
